//
//  Constants.h
//  BabyMe
//

#ifndef BabyMe_Constants_h
#define BabyMe_Constants_h

// App update plist
#define APP_ENTERPRISE_PLIST @"https://mobile.siliconstraits.vn/apps/apps/babyme/BabyMe.plist"

// API Path
#define kBaseURL @"http://www.babyme.vn/api/v1/"
//#define kBaseURL @"http://192.168.3.100:3000/api/v1/"
// Screensize and iOS version
#define IS_IPHONE4     ([UIScreen mainScreen].bounds.size.height < 568.0)
#define IS_IPHONE6     ([UIScreen mainScreen].bounds.size.height > 660.0)
#define SYSTEM_VERSION_LESS_THAN(v)   ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)


// Color
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define NAVBAR_COLOR [UIColor colorWithRed:20/255.0f green:185/255.0f blue:82/255.0f alpha:1.0f]

// Define baby information
#define    BABY_ID          (NSString *)[[NSUserDefaults standardUserDefaults] secretObjectForKey:kBabyID]
#define    USER_ID          (NSString *)[[NSUserDefaults standardUserDefaults] secretObjectForKey:kUserID   ]
#define    BABY_NAME        (NSString *)[[NSUserDefaults standardUserDefaults] secretObjectForKey:kBabyName]
#define    BABY_GENDER      (NSString *)[[NSUserDefaults standardUserDefaults] secretObjectForKey:kBabyGender]
#define    BABY_BIRTHDAY    (NSString *)[[NSUserDefaults standardUserDefaults] secretObjectForKey:kBabyBirthday]
#define    kSentBabyInfo    @"2IjVmciz3Wq4g3U5T1Zn"

// Is logged in?
#define     kLoggedIn       @"yUdL8ylKJvi1JmNPUIGk"
#define     LOGGED_IN       ([[NSUserDefaults standardUserDefaults] boolForKey:kLoggedIn] == YES)

#define     kLoggedInAsGuest     @"GZ9Wr4owHKzLUz3z3hWr"
#define     LOGGED_IN_GUEST      [[NSUserDefaults standardUserDefaults] boolForKey:kLoggedInAsGuest]

// is logged out?
#define     kNotificationShouldLogout   @"Ccmba1073DRRQiDVPYSY"
#define     kLoggedOut                  @"MUqm3ChsnIBf3Svpt2yo"
#define     LOGGED_OUT                  [[NSUserDefaults standardUserDefaults] boolForKey:kLoggedOut]

// Walkthrough content
#define kWalkthroughContent  @[@"Đặt câu hỏi và được trả lời từ chuyên gia", @"Thảo luận cùng các mẹ", @"Nhận nhắc nhở lịch khám và chích ngừa"];
#define kWalkthoughImageNames [NSArray arrayWithObjects:@"walkthrough-1", @"walkthrough-2",@"walkthrough-3", nil]

// Store baby infomation
#define kBabyID              @"k861BWOQMTqnhlBHNktA"
#define kUserID              @"k861BWOQMTqnhlBHNktB"
#define kBabyName            @"4IQHRKBzx7oI8UGLP5nI"
#define kBabyBirthday        @"zwYpeQP7OZOaTi8puvI7"
#define kBabyGender          @"VPELprelPxTSsYp6mWsP"

// Local storage in home page
#define kWebViewContent      @"Yc9nIQbj2IjqB3i6XZJT"
#define kBabyAge             @"MRuSzopeQcGLq1RgIWlV"
#define kReminderData        @"yUdmMyIBYNc8Y0y24XPT"
#define kQuestionData        @"RncjLAZwK1CMhb5bgAB9"
#define kBookmarkData        @"VUGWwT0QsNjJ2re2nq9a"

// SIAlertView
#define kDismissText    @"Đóng lại"
#define kFBAppId              @"611232208991726"
// Store user information
#define kAPIAuthToken       @"w2moeFEcdOGCS6vFQpnU"
#define kAPIUserObject      @"LLIdXh7lyeRtdBgVnvSw"

// GA TrackingID
#define kGATrackingUID      @"UA-59105165-1"

//BabyImage
#define cpBabyImages @{@"BabyImg_Pregnancy_0":[UIImage imageNamed:@"BabyImg_Pregnancy_0"],@"BabyImg_Pregnancy_1":[UIImage imageNamed:@"BabyImg_Pregnancy_1"],@"BabyImg_Pregnancy_2":[UIImage imageNamed:@"BabyImg_Pregnancy_2"],@"BabyImg_Pregnancy_3":[UIImage imageNamed:@"BabyImg_Pregnancy_3"],@"BabyImg_Pregnancy_4":[UIImage imageNamed:@"BabyImg_Pregnancy_4"],@"BabyImg_Pregnancy_5":[UIImage imageNamed:@"BabyImg_Pregnancy_5"],@"BabyImg_Pregnancy_6":[UIImage imageNamed:@"BabyImg_Pregnancy_6"],@"BabyImg_Pregnancy_7":[UIImage imageNamed:@"BabyImg_Pregnancy_7"],@"BabyImg_Pregnancy_8":[UIImage imageNamed:@"BabyImg_Pregnancy_8"],@"BabyImg_Pregnancy_9":[UIImage imageNamed:@"BabyImg_Pregnancy_9"],@"BabyImg_Pregnancy_10":[UIImage imageNamed:@"BabyImg_Pregnancy_10"],@"BabyImg_Pregnancy_11":[UIImage imageNamed:@"BabyImg_Pregnancy_11"],@"BabyImg_Pregnancy_12":[UIImage imageNamed:@"BabyImg_Pregnancy_12"],@"BabyImg_Pregnancy_13":[UIImage imageNamed:@"BabyImg_Pregnancy_13"],@"BabyImg_Pregnancy_14":[UIImage imageNamed:@"BabyImg_Pregnancy_14"],@"BabyImg_Pregnancy_15":[UIImage imageNamed:@"BabyImg_Pregnancy_15"],@"BabyImg_Pregnancy_16":[UIImage imageNamed:@"BabyImg_Pregnancy_16"],@"BabyImg_Pregnancy_17":[UIImage imageNamed:@"BabyImg_Pregnancy_17"],@"BabyImg_Pregnancy_18":[UIImage imageNamed:@"BabyImg_Pregnancy_18"],@"BabyImg_Pregnancy_19":[UIImage imageNamed:@"BabyImg_Pregnancy_19"],@"BabyImg_Pregnancy_20":[UIImage imageNamed:@"BabyImg_Pregnancy_20"],@"BabyImg_Pregnancy_21":[UIImage imageNamed:@"BabyImg_Pregnancy_21"],@"BabyImg_Pregnancy_22":[UIImage imageNamed:@"BabyImg_Pregnancy_22"],@"BabyImg_Pregnancy_23":[UIImage imageNamed:@"BabyImg_Pregnancy_23"],@"BabyImg_Pregnancy_24":[UIImage imageNamed:@"BabyImg_Pregnancy_24"],@"BabyImg_Pregnancy_25":[UIImage imageNamed:@"BabyImg_Pregnancy_25"],@"BabyImg_Pregnancy_26":[UIImage imageNamed:@"BabyImg_Pregnancy_26"],@"BabyImg_Pregnancy_27":[UIImage imageNamed:@"BabyImg_Pregnancy_27"],@"BabyImg_Pregnancy_28":[UIImage imageNamed:@"BabyImg_Pregnancy_28"],@"BabyImg_Pregnancy_29":[UIImage imageNamed:@"BabyImg_Pregnancy_29"],@"BabyImg_Pregnancy_30":[UIImage imageNamed:@"BabyImg_Pregnancy_30"],@"BabyImg_Pregnancy_31":[UIImage imageNamed:@"BabyImg_Pregnancy_31"],@"BabyImg_Pregnancy_32":[UIImage imageNamed:@"BabyImg_Pregnancy_32"],@"BabyImg_Pregnancy_33":[UIImage imageNamed:@"BabyImg_Pregnancy_33"],@"BabyImg_Pregnancy_34":[UIImage imageNamed:@"BabyImg_Pregnancy_34"],@"BabyImg_Pregnancy_35":[UIImage imageNamed:@"BabyImg_Pregnancy_35"],@"BabyImg_Pregnancy_36":[UIImage imageNamed:@"BabyImg_Pregnancy_36"],@"BabyImg_Pregnancy_37":[UIImage imageNamed:@"BabyImg_Pregnancy_37"],@"BabyImg_Pregnancy_38":[UIImage imageNamed:@"BabyImg_Pregnancy_38"],@"BabyImg_Pregnancy_39":[UIImage imageNamed:@"BabyImg_Pregnancy_39"],@"BabyImg_Pregnancy_40":[UIImage imageNamed:@"BabyImg_Pregnancy_40"],@"BabyImg_AfterBirth_41":[UIImage imageNamed:@"BabyImg_AfterBirth_41"],@"BabyImg_AfterBirth_42":[UIImage imageNamed:@"BabyImg_AfterBirth_42"],@"BabyImg_AfterBirth_43":[UIImage imageNamed:@"BabyImg_AfterBirth_43"],@"BabyImg_AfterBirth_44":[UIImage imageNamed:@"BabyImg_AfterBirth_44"],@"BabyImg_AfterBirth_45":[UIImage imageNamed:@"BabyImg_AfterBirth_45"],@"BabyImg_AfterBirth_46":[UIImage imageNamed:@"BabyImg_AfterBirth_46"],@"BabyImg_AfterBirth_47":[UIImage imageNamed:@"BabyImg_AfterBirth_47"],@"BabyImg_AfterBirth_48":[UIImage imageNamed:@"BabyImg_AfterBirth_48"],@"BabyImg_AfterBirth_49":[UIImage imageNamed:@"BabyImg_AfterBirth_49"],@"BabyImg_AfterBirth_50":[UIImage imageNamed:@"BabyImg_AfterBirth_50"],@"BabyImg_AfterBirth_51":[UIImage imageNamed:@"BabyImg_AfterBirth_51"],@"BabyImg_AfterBirth_52":[UIImage imageNamed:@"BabyImg_AfterBirth_52"],@"BabyImg_AfterBirth_53":[UIImage imageNamed:@"BabyImg_AfterBirth_53"],@"BabyImg_AfterBirth_54":[UIImage imageNamed:@"BabyImg_AfterBirth_54"],@"BabyImg_AfterBirth_55":[UIImage imageNamed:@"BabyImg_AfterBirth_55"],@"BabyImg_AfterBirth_56":[UIImage imageNamed:@"BabyImg_AfterBirth_55"],@"BabyImg_AfterBirth_57":[UIImage imageNamed:@"BabyImg_AfterBirth_57"],@"BabyImg_AfterBirth_58":[UIImage imageNamed:@"BabyImg_AfterBirth_58"],@"BabyImg_AfterBirth_59":[UIImage imageNamed:@"BabyImg_AfterBirth_59"],@"BabyImg_AfterBirth_60":[UIImage imageNamed:@"BabyImg_AfterBirth_59"],@"BabyImg_AfterBirth_61":[UIImage imageNamed:@"BabyImg_AfterBirth_61"],@"BabyImg_AfterBirth_62":[UIImage imageNamed:@"BabyImg_AfterBirth_61"],@"BabyImg_AfterBirth_63":[UIImage imageNamed:@"BabyImg_AfterBirth_63"],@"BabyImg_AfterBirth_64":[UIImage imageNamed:@"BabyImg_AfterBirth_64"],@"BabyImg_AfterBirth_65":[UIImage imageNamed:@"BabyImg_AfterBirth_65"],@"BabyImg_AfterBirth_66":[UIImage imageNamed:@"BabyImg_AfterBirth_65"],@"BabyImg_AfterBirth_67":[UIImage imageNamed:@"BabyImg_AfterBirth_67"],@"BabyImg_AfterBirth_68":[UIImage imageNamed:@"BabyImg_AfterBirth_68"],@"BabyImg_AfterBirth_69":[UIImage imageNamed:@"BabyImg_AfterBirth_69"],@"BabyImg_AfterBirth_70":[UIImage imageNamed:@"BabyImg_AfterBirth_70"],@"BabyImg_AfterBirth_71":[UIImage imageNamed:@"BabyImg_AfterBirth_71"],@"BabyImg_AfterBirth_72":[UIImage imageNamed:@"BabyImg_AfterBirth_72"],@"BabyImg_AfterBirth_73":[UIImage imageNamed:@"BabyImg_AfterBirth_72"],@"BabyImg_AfterBirth_74":[UIImage imageNamed:@"BabyImg_AfterBirth_74"],@"BabyImg_AfterBirth_75":[UIImage imageNamed:@"BabyImg_AfterBirth_74"],@"BabyImg_AfterBirth_76":[UIImage imageNamed:@"BabyImg_AfterBirth_76"],@"BabyImg_AfterBirth_77":[UIImage imageNamed:@"BabyImg_AfterBirth_76"],@"BabyImg_AfterBirth_78":[UIImage imageNamed:@"BabyImg_AfterBirth_76"],@"BabyImg_AfterBirth_79":[UIImage imageNamed:@"BabyImg_AfterBirth_76"],@"BabyImg_AfterBirth_80":[UIImage imageNamed:@"BabyImg_AfterBirth_80"],@"BabyImg_AfterBirth_81":[UIImage imageNamed:@"BabyImg_AfterBirth_81"],@"BabyImg_AfterBirth_82":[UIImage imageNamed:@"BabyImg_AfterBirth_82"],@"BabyImg_AfterBirth_83":[UIImage imageNamed:@"BabyImg_AfterBirth_82"],@"BabyImg_AfterBirth_84":[UIImage imageNamed:@"BabyImg_AfterBirth_84"],@"BabyImg_AfterBirth_85":[UIImage imageNamed:@"BabyImg_AfterBirth_85"],@"BabyImg_AfterBirth_86":[UIImage imageNamed:@"BabyImg_AfterBirth_85"],@"BabyImg_AfterBirth_87":[UIImage imageNamed:@"BabyImg_AfterBirth_85"],@"BabyImg_AfterBirth_88":[UIImage imageNamed:@"BabyImg_AfterBirth_85"],@"BabyImg_AfterBirth_89":[UIImage imageNamed:@"BabyImg_AfterBirth_89"],@"BabyImg_AfterBirth_90":[UIImage imageNamed:@"BabyImg_AfterBirth_89"],@"BabyImg_AfterBirth_91":[UIImage imageNamed:@"BabyImg_AfterBirth_89"],@"BabyImg_AfterBirth_92":[UIImage imageNamed:@"BabyImg_AfterBirth_89"],@"BabyImg_AfterBirth_93":[UIImage imageNamed:@"BabyImg_AfterBirth_89"],@"BabyImg_AfterBirth_94":[UIImage imageNamed:@"BabyImg_AfterBirth_89"],@"BabyImg_AfterBirth_95":[UIImage imageNamed:@"BabyImg_AfterBirth_89"],@"BabyImg_AfterBirth_96":[UIImage imageNamed:@"BabyImg_AfterBirth_89"],@"BabyImg_AfterBirth_97":[UIImage imageNamed:@"BabyImg_AfterBirth_89"],@"BabyImg_AfterBirth_98":[UIImage imageNamed:@"BabyImg_AfterBirth_89"],@"BabyImg_AfterBirth_99":[UIImage imageNamed:@"BabyImg_AfterBirth_99"],@"BabyImg_AfterBirth_100":[UIImage imageNamed:@"BabyImg_AfterBirth_99"],@"BabyImg_AfterBirth_101":[UIImage imageNamed:@"BabyImg_AfterBirth_99"],@"BabyImg_AfterBirth_102":[UIImage imageNamed:@"BabyImg_AfterBirth_102"],@"BabyImg_AfterBirth_103":[UIImage imageNamed:@"BabyImg_AfterBirth_102"],@"BabyImg_AfterBirth_104":[UIImage imageNamed:@"BabyImg_AfterBirth_102"],@"BabyImg_AfterBirth_105":[UIImage imageNamed:@"BabyImg_AfterBirth_102"],@"BabyImg_AfterBirth_106":[UIImage imageNamed:@"BabyImg_AfterBirth_102"],@"BabyImg_AfterBirth_107":[UIImage imageNamed:@"BabyImg_AfterBirth_107"],@"BabyImg_AfterBirth_108":[UIImage imageNamed:@"BabyImg_AfterBirth_107"],@"BabyImg_AfterBirth_109":[UIImage imageNamed:@"BabyImg_AfterBirth_107"],@"BabyImg_AfterBirth_110":[UIImage imageNamed:@"BabyImg_AfterBirth_107"],@"BabyImg_AfterBirth_111":[UIImage imageNamed:@"BabyImg_AfterBirth_107"],@"BabyImg_AfterBirth_112":[UIImage imageNamed:@"BabyImg_AfterBirth_107"],@"BabyImg_AfterBirth_113":[UIImage imageNamed:@"BabyImg_AfterBirth_107"],@"BabyImg_AfterBirth_114":[UIImage imageNamed:@"BabyImg_AfterBirth_107"],@"BabyImg_AfterBirth_115":[UIImage imageNamed:@"BabyImg_AfterBirth_107"],@"BabyImg_AfterBirth_116":[UIImage imageNamed:@"BabyImg_AfterBirth_107"],@"BabyImg_AfterBirth_117":[UIImage imageNamed:@"BabyImg_AfterBirth_117"],@"BabyImg_AfterBirth_118":[UIImage imageNamed:@"BabyImg_AfterBirth_117"],@"BabyImg_AfterBirth_119":[UIImage imageNamed:@"BabyImg_AfterBirth_117"],@"BabyImg_AfterBirth_120":[UIImage imageNamed:@"BabyImg_AfterBirth_117"],@"BabyImg_AfterBirth_121":[UIImage imageNamed:@"BabyImg_AfterBirth_117"],@"BabyImg_AfterBirth_122":[UIImage imageNamed:@"BabyImg_AfterBirth_117"],@"BabyImg_AfterBirth_123":[UIImage imageNamed:@"BabyImg_AfterBirth_117"],@"BabyImg_AfterBirth_124":[UIImage imageNamed:@"BabyImg_AfterBirth_117"],@"BabyImg_AfterBirth_125":[UIImage imageNamed:@"BabyImg_AfterBirth_117"],@"BabyImg_AfterBirth_126":[UIImage imageNamed:@"BabyImg_AfterBirth_117"],@"BabyImg_AfterBirth_127":[UIImage imageNamed:@"BabyImg_AfterBirth_117"],@"BabyImg_AfterBirth_128":[UIImage imageNamed:@"BabyImg_AfterBirth_117"],@"BabyImg_AfterBirth_129":[UIImage imageNamed:@"BabyImg_AfterBirth_129"],@"BabyImg_AfterBirth_130":[UIImage imageNamed:@"BabyImg_AfterBirth_129"],@"BabyImg_AfterBirth_131":[UIImage imageNamed:@"BabyImg_AfterBirth_129"],@"BabyImg_AfterBirth_132":[UIImage imageNamed:@"BabyImg_AfterBirth_129"],@"BabyImg_AfterBirth_133":[UIImage imageNamed:@"BabyImg_AfterBirth_133"],@"BabyImg_AfterBirth_134":[UIImage imageNamed:@"BabyImg_AfterBirth_133"],@"BabyImg_AfterBirth_135":[UIImage imageNamed:@"BabyImg_AfterBirth_133"],@"BabyImg_AfterBirth_136":[UIImage imageNamed:@"BabyImg_AfterBirth_133"]}

// Icon for reminder tab in Home Page VC
#define reminderIconDict  @{@"chích ngừa":[UIImage imageNamed:@"Chich Ngua"],@"khám thai":[UIImage imageNamed:@"Kham Thai"],@"xét nghiệm":[UIImage imageNamed:@"Xet Nghiem"],@"siêu âm":[UIImage imageNamed:@"Sieu Am"],@"cảnh báo":[UIImage imageNamed:@"Canh Bao"],@"mẹo":[UIImage imageNamed:@"Meo"]}
#endif

#define UBL_SETTING_ASK_QUESTION @"UBL_SETTING_ASK_QUESTION"
