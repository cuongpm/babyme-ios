//
//  Question.h
//  BabyMe
//

#import <Foundation/Foundation.h>

@interface Question : NSObject

@property (nonatomic) int               questionid;
@property (nonatomic) int               categoryid;
@property (nonatomic, copy) NSString    *username;
@property (nonatomic, copy) NSString    *email;
@property (nonatomic, copy) NSString    *question;
@property (nonatomic, copy) NSString    *sourcename;
@property (nonatomic, copy) NSString    *sourcelogo;
@property (nonatomic, copy) NSString    *answer;
@property (nonatomic, copy) NSString    *sharedAnswer;
@property (nonatomic, copy) NSArray     *comments;
@property (nonatomic) int               commentsCount;
@property (nonatomic) int               articlesCount;
@property (nonatomic) int               votesCount;
@property (nonatomic) BOOL              isBookmark;
@property (nonatomic) BOOL              isVoted;

- (id)initWithQuestionDictionary:(NSDictionary *)dict;

@end