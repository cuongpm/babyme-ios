//
//  Question.m
//  BabyMe
//

#import "Question.h"

@implementation Question

- (id)initWithQuestionDictionary:(NSDictionary *)dict {
    if (self = [super init]) {
        self.questionid     = [dict[@"id"] intValue];
        self.categoryid     = [dict[@"category_id"] isEqual:[NSNull null]] ? 0 :[dict[@"category_id"] intValue];
        self.username       = dict[@"questioner_name"];
        self.email          = dict[@"questioner_email"];
        self.question       = dict[@"question"];
        self.sourcename     = dict[@"source_name"];
        self.sourcelogo     = dict[@"source_logo"];
        self.answer         = dict[@"answer"];
        self.sharedAnswer   = dict[@"desc"];
        self.comments       = dict[@"comments"];
        self.isBookmark     = [dict[@"bookmark"] boolValue];
        self.votesCount     = [dict[@"votes"] intValue];
        self.commentsCount  = [dict[@"comments_count"] intValue];
        self.articlesCount  = [dict[@"articles_count"] intValue];
        self.isVoted        = [dict[@"vote"] boolValue];
    }
    return self;
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder {
    if (!(self = [super init])) return nil;
    self.questionid  = (int)[decoder decodeIntegerForKey:@"questionid"];
    self.categoryid  = (int)[decoder decodeIntegerForKey:@"category_id"];
    self.username    = [decoder decodeObjectForKey:@"username"];
    self.question    = [decoder decodeObjectForKey:@"question"];
    self.email    = [decoder decodeObjectForKey:@"questioner_email"];
    self.sourcename  = [decoder decodeObjectForKey:@"sourcename"];
    self.sourcelogo  = [decoder decodeObjectForKey:@"sourcelogo"];
    self.answer      = [decoder decodeObjectForKey:@"answer"];
    self.sharedAnswer      = [decoder decodeObjectForKey:@"desc"];
    self.comments    = [decoder decodeObjectForKey:@"comments"];
    self.isBookmark  = [[decoder decodeObjectForKey:@"isBookmark"] boolValue];
    self.votesCount  = (int)[decoder decodeIntegerForKey:@"votes_count"];
    self.commentsCount  = (int)[decoder decodeIntegerForKey:@"comments_count"];
    self.articlesCount  = (int)[decoder decodeIntegerForKey:@"articles_count"];
    self.isVoted        = [[decoder decodeObjectForKey:@"vote"] boolValue];
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInteger:self.questionid forKey:@"questionid"];
    [encoder encodeInteger:self.categoryid forKey:@"category_id"];
    [encoder encodeObject:self.username forKey:@"username"];
    [encoder encodeObject:self.question forKey:@"question"];
    [encoder encodeObject:self.email forKey:@"questioner_email"];
    [encoder encodeObject:self.sourcename forKey:@"sourcename"];
    [encoder encodeObject:self.sourcelogo forKey:@"sourcelogo"];
    [encoder encodeObject:self.answer forKey:@"answer"];
    [encoder encodeObject:self.sharedAnswer forKey:@"desc"];
    [encoder encodeObject:self.comments forKey:@"comments"];
    [encoder encodeBool:self.isBookmark forKey:@"isBookmark"];
    [encoder encodeInteger:self.votesCount forKey:@"votes_count"];
    [encoder encodeInteger:self.commentsCount forKey:@"comments_count"];
    [encoder encodeInteger:self.articlesCount forKey:@"articles_count"];
    [encoder encodeBool:self.isVoted forKey:@"vote"];
}

@end
