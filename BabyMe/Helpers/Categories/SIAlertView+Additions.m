//
//  SIAlertView+Additions.m
//  BabyMe
//

#import "SIAlertView+Additions.h"

@implementation SIAlertView (Additions)

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
         cancelButtonTitle:(NSString*)titleButton
{
    UIImage * greenImage = [[SIAlertView imageWithColor:NAVBAR_COLOR] stretchableImageWithLeftCapWidth:1 topCapHeight:1];
    [[SIAlertView appearance] setDefaultButtonImage:greenImage forState:UIControlStateNormal];
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:title andMessage:message];
    [alertView addButtonWithTitle:titleButton
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                          }];
    
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    alertView.buttonColor = [UIColor whiteColor];
    [alertView show];
}

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
         cancelButtonTitle:(NSString*)titleButton
                   handler:(void (^)(void))handler
{
    UIImage * greenImage = [[SIAlertView imageWithColor:NAVBAR_COLOR] stretchableImageWithLeftCapWidth:1 topCapHeight:1];
    [[SIAlertView appearance] setDefaultButtonImage:greenImage forState:UIControlStateNormal];

    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:title andMessage:message];
    [alertView addButtonWithTitle:titleButton
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              if (handler) handler();
                          }];

    alertView.transitionStyle = SIAlertViewTransitionStyleSlideFromBottom;
    [alertView show];
}

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
         cancelButtonTitle:(NSString*)titleButton
                 animation:(SIAlertViewTransitionStyle) transitionStype
                   handler:(void (^)(void))handler
{
    UIImage * greenImage = [[SIAlertView imageWithColor:NAVBAR_COLOR] stretchableImageWithLeftCapWidth:1 topCapHeight:1];
    [[SIAlertView appearance] setDefaultButtonImage:greenImage forState:UIControlStateNormal];
    [[SIAlertView appearance] setButtonColor:[UIColor whiteColor]];

    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:title andMessage:message];
    [alertView addButtonWithTitle:titleButton
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              if (handler) handler();
                          }];
    
    alertView.transitionStyle = transitionStype;
    [alertView show];
}

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
         cancelButtonTitle:(NSString*)titleCancelButton
             okButtonTitle:(NSString*)titleOkButton
                   handler:(void (^)(void))handler
{
    UIImage * greenImage = [[SIAlertView imageWithColor:NAVBAR_COLOR] stretchableImageWithLeftCapWidth:1 topCapHeight:1];
    UIImage * grayImage = [[SIAlertView imageWithColor:[UIColor lightGrayColor]] stretchableImageWithLeftCapWidth:1 topCapHeight:1];
    [[SIAlertView appearance] setCancelButtonImage:grayImage forState:UIControlStateNormal];
    [[SIAlertView appearance] setDefaultButtonImage:greenImage forState:UIControlStateNormal];
    [[SIAlertView appearance] setCancelButtonColor:[UIColor whiteColor]];
    [[SIAlertView appearance] setButtonColor:[UIColor whiteColor]];

    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:title andMessage:message];
    [alertView addButtonWithTitle:titleCancelButton
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alert) {
                          }];
    [alertView addButtonWithTitle:titleOkButton
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              if (handler) handler();
                          }];

    alertView.transitionStyle = SIAlertViewTransitionStyleDropDown;
    [alertView show];
}



+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}


@end
