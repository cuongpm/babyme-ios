//
//  SIAlertView+Additions.h
//  BabyMe
//

#import "SIAlertView.h"

@interface SIAlertView (Additions)

+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
         cancelButtonTitle:(NSString*)titleButton;
+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
         cancelButtonTitle:(NSString*)titleButton
                   handler:(void (^)(void))handler;
+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
         cancelButtonTitle:(NSString*)titleCancelButton
             okButtonTitle:(NSString*)titleOkButton
                   handler:(void (^)(void))handler;
+ (void)showAlertWithTitle:(NSString *)title
                   message:(NSString *)message
         cancelButtonTitle:(NSString*)titleButton
                 animation:(SIAlertViewTransitionStyle) transitionStype
                   handler:(void (^)(void))handler;
@end
