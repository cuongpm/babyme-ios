//
//  BMNetworkEngine.m
//  BabyMe
//

#import "BMNetworkEngine.h"

@implementation BMNetworkEngine

#pragma mark - User

// Login with email
- (AFHTTPRequestOperation*)loginWithEmail:(NSString *)email
                                 password:(NSString *)password
                          completionBlock:(BMSuccessBlock)completionBlock
                               errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{@"email":email,
                                 @"password":password};
    AFHTTPRequestOperation *operation =
    [manager POST:[kBaseURL stringByAppendingString:@"users/signin?version=2.0.7"]
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSLog(@"Respond for login: %@", responseObject);
              
              // Basic validation for authentication token
              NSDictionary * dict = responseObject;
              if (![dict isKindOfClass:NSDictionary.class]) {
                  NSLog(@"Invalid Authentication response format %@", responseObject);
                  if (errorBlock) errorBlock([NSError errorWithDomain:@"Invalid response" code:999 userInfo:nil]);
              }
              NSString * authToken = dict[@"auth_token"];
              NSLog(@"Token key is %@", authToken);
              if (!authToken || ![authToken isKindOfClass:NSString.class]) {
                  if (errorBlock) errorBlock([NSError errorWithDomain:@"Invalid Authentication Response" code:888 userInfo:nil]);
                  return;
              }
              
              // Store user object, to be used in the future
              [[NSUserDefaults standardUserDefaults] setSecretObject:responseObject forKey:kAPIUserObject];
              
              // Store host name & auth_token if everything is ok
              [[NSUserDefaults standardUserDefaults] setSecretObject:authToken forKey:kAPIAuthToken];

              [[NSUserDefaults standardUserDefaults] synchronize];
              if (completionBlock) {
                  completionBlock(responseObject);
              }
              
              // Flag as logged in
              [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kLoggedIn];
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Failed to login: %@", error);
              if (errorBlock) {
                  errorBlock(error);
              }
          }];
    return operation;
}

// Login with Facebook

- (AFHTTPRequestOperation*)loginWithFacebookToken:(NSString *)facebookToken
                            andCreateBabyWithName:(NSString *)babyname
                                           gender:(NSString *)gender
                                         birthday:(NSString *)birthday
                                  completionBlock:(BMSuccessBlock)completionBlock
                                       errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameters = [@{@"facebook_token":facebookToken} mutableCopy];;
    if ([gender isEqualToString:@"male"] || [gender isEqualToString:@"female"]) {
        parameters[@"baby_gender"] = gender;
    }
    if (babyname != nil) {
        parameters [@"baby_name"]= babyname;
    }
    if (birthday != nil) {
        parameters [@"baby_dob"]= birthday;
    }

    AFHTTPRequestOperation *operation =
    [manager POST:[kBaseURL stringByAppendingString:@"users/signin_facebook?version=2.0.7"]
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSLog(@"Respond for login with Facebook: %@", responseObject);

              // Basic validation for authentication token
              NSDictionary * dict = responseObject;
              if (![dict isKindOfClass:NSDictionary.class]) {
                  NSLog(@"Invalid Authentication response format %@", responseObject);
                  if (errorBlock) errorBlock([NSError errorWithDomain:@"Invalid response" code:999 userInfo:nil]);
              }
              NSString * authToken = dict[@"auth_token"];
              if (!authToken || ![authToken isKindOfClass:NSString.class]) {
                  if (errorBlock) errorBlock([NSError errorWithDomain:@"Invalid Authentication Response" code:888 userInfo:nil]);
                  return;
              }
              
              // Store user object, to be used in the future
              [[NSUserDefaults standardUserDefaults] setSecretObject:responseObject forKey:kAPIUserObject];
              
              // Store host name & auth_token if everything is ok
              [[NSUserDefaults standardUserDefaults] setSecretObject:authToken forKey:kAPIAuthToken];
              [[NSUserDefaults standardUserDefaults] synchronize];
              if (completionBlock) {
                  completionBlock(responseObject);
              }
              
              // Flag as logged in
              [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kLoggedIn];
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Failed to login with Facebook: %@", error);
              if (errorBlock) {
                  errorBlock(error);
              }
          }];
    return operation;
}

// Reset password
- (AFHTTPRequestOperation *)resetPasswordWithEmail:(NSString*)email
                                 completionHandler:(void (^)(void))completionBlock
                                      errorHandler:(void (^)(NSString * errorMessage))errorBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    return [manager POST:[kBaseURL stringByAppendingString:@"users/forgotpassword?version=2.0.7"]
       parameters:@{@"email": email}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {

                  if(completionBlock) completionBlock();
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Failed to login: %@", error);
              if (errorBlock) {
                  errorBlock(error);
              }
          }];
}

// Signup with email
- (AFHTTPRequestOperation*)signUpWithEmail:(NSString *)email
                                  password:(NSString *)password
                                   baby_id:(NSString *)baby_id
                           completionBlock:(BMSuccessBlock)completionBlock
                               errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameters = [@{@"email":email,
                                      @"password":password,
                                         @"baby_id": baby_id} mutableCopy];
    
    AFHTTPRequestOperation *operation =
    [manager POST:[kBaseURL stringByAppendingString:@"users/signup?version=2.0.7"]
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSLog(@"Respond for sign up: %@", responseObject);
              
              // Basic validation for authentication token
              NSDictionary * dict = responseObject;
              if (![dict isKindOfClass:NSDictionary.class]) {
                  NSLog(@"Invalid Authentication response format %@", responseObject);
                  if (errorBlock) errorBlock([NSError errorWithDomain:@"Invalid response" code:999 userInfo:nil]);
              }
              NSString * authToken = dict[@"auth_token"];
              if (!authToken || ![authToken isKindOfClass:NSString.class]) {
                  if (errorBlock) errorBlock([NSError errorWithDomain:@"Invalid Authentication Response" code:888 userInfo:nil]);
                  return;
              }
              
              // Store user object, to be used in the future
              [[NSUserDefaults standardUserDefaults] setSecretObject:responseObject forKey:kAPIUserObject];
              [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"id"] forKey:kUserID];
              // Store host name & auth_token if everything is ok
              [[NSUserDefaults standardUserDefaults] setSecretObject:authToken forKey:kAPIAuthToken];
              [[NSUserDefaults standardUserDefaults] synchronize];
              if (completionBlock) {
                  completionBlock(responseObject);
              }
              
              // Flag as logged in
              //[[NSUserDefaults standardUserDefaults] setBool:YES forKey:kLoggedIn];
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Failed to sign up: %@", error);
              if (errorBlock) {
                  errorBlock(error);
              }
          }];
    return operation;
}


- (AFHTTPRequestOperation *)getMyQuestionCompletionBlock:(BMSuccessBlock)completionBlock
                                              errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GETWithToken:[kBaseURL stringByAppendingString:@"questions/mine?version=2.0.7"]
            parameters:nil
               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                   if (completionBlock) {
                       completionBlock(responseObject);
                   }
               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   NSLog(@"Error: %@", error);
                   if (errorBlock) {
                       errorBlock(error);
                   }
               }];
    return operation;
}

- (AFHTTPRequestOperation *)getBookmarkQuestionCompletionBlock:(BMSuccessBlock)completionBlock
                                              errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GETWithToken:[kBaseURL stringByAppendingString:@"questions/bookmarked?version=2.0.7"]
            parameters:nil
               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                   if (completionBlock) {
                       completionBlock(responseObject);
                   }
               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   NSLog(@"Error: %@", error);
                   if (errorBlock) {
                       errorBlock(error);
                   }
               }];
    return operation;
}

#pragma mark - Baby
- (AFHTTPRequestOperation*)sendBabyInfoWithName:(NSString *)name
                                         gender:(NSString *)gender
                                       birthday:(NSString *)birthday
                                        user_id:(NSString *)user_id
                                completionBlock:(BMSuccessBlock)completionBlock
                                     errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameters = [@{@"name":name,
                                  @"dob":birthday} mutableCopy];
    if ([gender isEqualToString:@"male"] || [gender isEqualToString:@"female"]) {
        parameters[@"gender"] = gender;
    }
    if(user_id != nil){
        parameters[@"user_id"] = user_id;
    }
    AFHTTPRequestOperation *operation =
    [manager POST:[kBaseURL stringByAppendingString:@"babies"]
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSLog(@"Respond for send baby info: %@", responseObject);
              if (completionBlock) {
                  completionBlock(responseObject);
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    
              NSLog(@"Failed to send baby info: %@", error);
              if (errorBlock) {
                  errorBlock(error);
              }
          }];
    return operation;
}

- (AFHTTPRequestOperation*)updateBabyInfoWithId:(NSInteger)babyId
                                           name:(NSString *)name
                                         gender:(NSString *)gender
                                       birthday:(NSString *)birthday
                                completionBlock:(BMSuccessBlock)completionBlock
                                     errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSMutableDictionary *parameters = [@{@"id":@(babyId), @"name":name,
                                         @"dob":birthday} mutableCopy];
    if ([gender isEqualToString:@"male"] || [gender isEqualToString:@"female"]) {
        parameters[@"gender"] = gender;
    }
    AFHTTPRequestOperation *operation =
    [manager PUT:[kBaseURL stringByAppendingString:@"babies"]
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSLog(@"Respond for send baby info: %@", responseObject);
              if (completionBlock) {
                  completionBlock(responseObject);
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              NSLog(@"Failed to send baby info: %@", error);
              if (errorBlock) {
                  errorBlock(error);
              }
          }];
    return operation;
}


#pragma mark - Homepage
- (AFHTTPRequestOperation *)getBabyAge:(NSString *)babyid
                       completionBlock:(BMSuccessBlock)completionBlock
                            errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GET:[NSString stringWithFormat:@"%@babies/%@/age?version=2.0.7",kBaseURL, babyid]
   parameters:nil
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          NSLog(@"Return age of baby: %@", responseObject);
          [[NSUserDefaults standardUserDefaults] setSecretObject:responseObject forKey:kBabyAge];
          if (completionBlock) {
              completionBlock(responseObject);
          }
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          NSLog(@"Error: %@", error);
          if (errorBlock) {
              errorBlock(error);
          }
      }];
    return operation;
}

- (AFHTTPRequestOperation *)getArticleOf:(NSString *)babyid
                         completionBlock:(BMSuccessBlock)completionBlock
                              errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GET:[kBaseURL stringByAppendingString:@"articles/find_by?version=2.0.7"]
   parameters:@{@"baby_id":babyid}
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          // NSLog(@"Return article: %@", responseObject);
          if (completionBlock) {
              [[NSUserDefaults standardUserDefaults] setSecretObject:responseObject forKey:kWebViewContent];
              completionBlock(responseObject);
          }
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          NSLog(@"Error: %@", error);
          if (errorBlock) {
              errorBlock(error);
          }
      }];
    return operation;
}

- (AFHTTPRequestOperation *)getReminderOf:(NSString *)babyid
                          completionBlock:(BMSuccessBlock)completionBlock
                               errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GET:[kBaseURL stringByAppendingString:@"reminders/find_by?version=2.0.7"]
   parameters:@{@"baby_id":babyid}
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          if (completionBlock) {
              [[NSUserDefaults standardUserDefaults] setSecretObject:responseObject forKey:kReminderData];
              completionBlock(responseObject);
          }
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          NSLog(@"Error: %@", error);
          if (errorBlock) {
              errorBlock(error);
          }
      }];
    return operation;
}

- (AFHTTPRequestOperation *)getNotificationOf:(NSString *)babyid
                          completionBlock:(BMSuccessBlock)completionBlock
                               errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GET:[kBaseURL stringByAppendingString:@"/pushes"]
   parameters:@{@"baby_id":babyid}
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          // NSLog(@"Return article: %@", responseObject);
          if (completionBlock) {
              completionBlock(responseObject);
          }
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          NSLog(@"Error: %@", error);
          if (errorBlock) {
              errorBlock(error);
          }
      }];
    return operation;
}

- (AFHTTPRequestOperation *)getQuestionOf:(NSString *)babyid
                          completionBlock:(BMSuccessBlock)completionBlock
                               errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GET:[kBaseURL stringByAppendingString:@"questions/find_by?version=2.0.7"]
   parameters:@{@"baby_id":babyid}
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          // NSLog(@"Return article: %@", responseObject);
          if (completionBlock) {
              completionBlock(responseObject);
          }
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          NSLog(@"Error: %@", error);
          if (errorBlock) {
              errorBlock(error);
          }
      }];
    return operation;
}

- (AFHTTPRequestOperation *)getWatchListCompletionBlock:(BMSuccessBlock)completionBlock
                                            errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GETWithToken:[kBaseURL stringByAppendingString:@"questions/watchlist?version=2.0.7"]
            parameters:nil
               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                   if (completionBlock) {
                       completionBlock(responseObject);
                   }
               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   NSLog(@"Error: %@", error);
                   if (errorBlock) {
                       errorBlock(error);
                   }
               }];
    return operation;
}

#pragma mark - Homepage by week
- (AFHTTPRequestOperation *)getArticleOfWeek:(int)week
                                 beforeBirth:(BOOL)before_birth
                             completionBlock:(BMSuccessBlock)completionBlock
                                  errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GET:[kBaseURL stringByAppendingString:@"articles/find_by_week?version=2.0.7"]
   parameters:@{@"week":@(week), @"before_birth":@(before_birth)}
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          // NSLog(@"Return article: %@", responseObject);
          if (completionBlock) {
              [[NSUserDefaults standardUserDefaults] setSecretObject:responseObject forKey:kWebViewContent];
              completionBlock(responseObject);
          }
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          NSLog(@"Error: %@", error);
          if (errorBlock) {
              errorBlock(error);
          }
      }];
    return operation;
}

- (AFHTTPRequestOperation *)getReminderOfWeek:(int)week
                                 beforeBirth:(BOOL)before_birth
                                  currentPage:(int) page
                             completionBlock:(BMSuccessBlock)completionBlock
                                  errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GET:[kBaseURL stringByAppendingString:@"reminders/find_by_week?version=2.0.7"]
   parameters:@{@"week":@(week), @"before_birth":@(before_birth), @"page": @(page)}
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          // NSLog(@"Return article: %@", responseObject);
          if (completionBlock) {
              [[NSUserDefaults standardUserDefaults] setSecretObject:responseObject forKey:kWebViewContent];
              completionBlock(responseObject);
          }
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          NSLog(@"Error: %@", error);
          if (errorBlock) {
              errorBlock(error);
          }
      }];
    return operation;
}

- (AFHTTPRequestOperation *)getQuestionOfWeek:(int)week
                                  beforeBirth:(BOOL)before_birth
                                  currentPage:(int) page
                              completionBlock:(BMSuccessBlock)completionBlock
                                   errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GET:[kBaseURL stringByAppendingString:@"questions/find_by_week?version=2.0.7"]
   parameters:@{@"week":@(week), @"before_birth":@(before_birth), @"page": @(page)}
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          // NSLog(@"Return article: %@", responseObject);
          if (completionBlock) {
              [[NSUserDefaults standardUserDefaults] setSecretObject:responseObject forKey:kWebViewContent];
              completionBlock(responseObject);
          }
      }
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          NSLog(@"Error: %@", error);
          if (errorBlock) {
              errorBlock(error);
          }
      }];
    return operation;
}

#pragma mark - Question Detail
// Bookmark question
- (AFHTTPRequestOperation *)bookmarkQuestion:(int)questionID
                             completionBlock:(BMSuccessBlock)completionBlock
                                  errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self POSTWithToken:[NSString stringWithFormat:@"%@bookmarks/question/%d?version=2.0.7",kBaseURL, questionID]
            parameters:nil
                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    if (completionBlock) {
                        completionBlock(responseObject);
                    }
               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   NSLog(@"Error: %@", error);
                   if (errorBlock) {
                       errorBlock(error);
                   }
               }];
    return operation;
}

// Unbookmark Question
- (AFHTTPRequestOperation *)unBookmarkQuestion:(int)questionID
                               completionBlock:(BMSuccessBlock)completionBlock
                                    errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self DELETEWithToken:[NSString stringWithFormat:@"%@bookmarks/question/%d?version=2.0.7",kBaseURL, questionID]
               parameters:nil
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      if (completionBlock) {
                          completionBlock(responseObject);
                      }
                  } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                      NSLog(@"Error: %@", error);
                      if (errorBlock) {
                          errorBlock(error);
                      }
                  }];
    return operation;
}

// Find question by keyword
- (AFHTTPRequestOperation *)findQuestionBy:(NSString *)keyword
                           completionBlock:(BMSuccessBlock)completionBlock
                                errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GET:[kBaseURL stringByAppendingString:@"questions/find?version=2.0.7"]
                                     parameters:@{@"keyword": keyword}
                                        success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                            if (completionBlock) {
                                                completionBlock(responseObject);
                                            }
                                        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                            NSLog(@"Error: %@", error);
                                            if (errorBlock) {
                                                errorBlock(error);
                                            }
                                        }];
    return operation;
}

// Get question detail
- (AFHTTPRequestOperation *)getQuestionDetailOf:(int)questionID
                                completionBlock:(BMSuccessBlock)completionBlock
                                     errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GETWithToken:[NSString stringWithFormat:@"%@questions/%d?version=2.0.7", kBaseURL, questionID]
            parameters:nil
               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                   if (completionBlock) {
                       completionBlock(responseObject);
                   }
               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   NSLog(@"Error: %@", error);
                   if (errorBlock) {
                       errorBlock(error);
                   }
               }];
    return operation;
}

#pragma mark - Article detail
- (AFHTTPRequestOperation *)getArticleDetailOf:(int)articleID
                        completionBlock:(BMSuccessBlock)completionBlock
                             errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GETWithToken:[NSString stringWithFormat:@"%@articles/%d?version=2.0.7", kBaseURL, articleID]
            parameters:nil
               success:^(AFHTTPRequestOperation *operation, id responseObject) {
                   if (completionBlock) {
                       completionBlock(responseObject);
                   }
               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   NSLog(@"Error: %@", error);
                   if (errorBlock) {
                       errorBlock(error);
                   }
               }];
    return operation;
}

#pragma mark - Related question/article
// Get related question
- (AFHTTPRequestOperation *)getRelatedQuestionOf:(int)questionID
                                 completionBlock:(BMSuccessBlock)completionBlock
                                      errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GET:[NSString stringWithFormat:@"%@/questions/%d/related?version=2.0.7", kBaseURL, questionID]
   parameters:nil
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          if (completionBlock) {
              completionBlock(responseObject);
          }
      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          NSLog(@"Error: %@", error);
          if (errorBlock) {
              errorBlock(error);
          }
      }];
    return operation;
}

// Get related article by category id
- (AFHTTPRequestOperation *)getRelatedArticleOf:(int)category_id
                                 completionBlock:(BMSuccessBlock)completionBlock
                                      errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GET:[kBaseURL stringByAppendingString:@"articles/related_by_category?version=2.0.7"]
   parameters:@{@"category_id":@(category_id)}
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          if (completionBlock) {
              completionBlock(responseObject);
          }
      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          NSLog(@"Error: %@", error);
          if (errorBlock) {
              errorBlock(error);
          }
      }];
    return operation;
}

// Get related article by tag id
- (AFHTTPRequestOperation *)getRelatedArticleByTag:(int)tag_id
                                   completionBlock:(BMSuccessBlock)completionBlock
                                        errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self GET:[kBaseURL stringByAppendingString:@"articles/related_by_tag?version=2.0.7"]
   parameters:@{@"tag_id":@(tag_id)}
      success:^(AFHTTPRequestOperation *operation, id responseObject) {
          if (completionBlock) {
              completionBlock(responseObject);
          }
      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          NSLog(@"Error: %@", error);
          if (errorBlock) {
              errorBlock(error);
          }
      }];
    return operation;
}

#pragma mark - Comment/Like
- (AFHTTPRequestOperation *)commentFor:(int)questionID
                           withContent:(NSString *) content
                       completionBlock:(BMSuccessBlock)completionBlock
                            errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self POSTWithToken:[NSString stringWithFormat:@"%@comments/question/%d?version=2.0.7", kBaseURL, questionID]
             parameters:@{@"content":content}
                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    if (completionBlock) {
                        completionBlock(responseObject);
                    }
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    NSLog(@"Error: %@", error);
                    if (errorBlock) {
                        errorBlock(error);
                    }
                }];
    return operation;
}

- (AFHTTPRequestOperation *)likeFor:(int)questionID
                    completionBlock:(BMSuccessBlock)completionBlock
                         errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self POSTWithToken:[NSString stringWithFormat:@"%@votes/question/%d?version=2.0.7", kBaseURL, questionID]
             parameters:nil
                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    if (completionBlock) {
                        completionBlock(responseObject);
                    }
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    NSLog(@"Error: %@", error);
                    if (errorBlock) {
                        errorBlock(error);
                    }
                }];
    return operation;
}

- (AFHTTPRequestOperation *)unLikeFor:(int)questionID
                    completionBlock:(BMSuccessBlock)completionBlock
                         errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self DELETEWithToken:[NSString stringWithFormat:@"%@votes/question/%d?version=2.0.7", kBaseURL, questionID]
             parameters:nil
                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    if (completionBlock) {
                        completionBlock(responseObject);
                    }
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    NSLog(@"Error: %@", error);
                    if (errorBlock) {
                        errorBlock(error);
                    }
                }];
    return operation;
}

#pragma mark - Create question
- (AFHTTPRequestOperation *)createQuestionWithTitle:(NSString *)title
                                            baby_id:(NSString *)baby_id
                                    completionBlock:(BMSuccessBlock)completionBlock
                                         errorBlock:(BMFailureBlock)errorBlock
{
    AFHTTPRequestOperation *operation =
    [self POSTWithToken:[kBaseURL stringByAppendingString:@"questions?version=2.0.7"]
             parameters:@{@"title":title,
                          @"baby_id":baby_id}
                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    if (completionBlock) {
                        completionBlock(responseObject);
                    }
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    NSLog(@"Error: %@", error);
                    if (errorBlock) {
                        errorBlock(error);
                    }
                }];
    return operation;
}


#pragma mark - Facebook session state change
// This method will handle ALL the session state changes in the app
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    // If the session was opened successfully
    if (!error && state == FBSessionStateOpen){
        NSLog(@"Session opened");
        // Show the user the logged-in UI
        return;
    }
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        // If the session is closed
        NSLog(@"Session closed");
    }

    // Handle errors
    if (error){
        NSLog(@"Error");
        NSString *alertText;
        NSString *alertTitle;
        // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
            [self showMessage:alertText withTitle:alertTitle];
        } else {

            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                NSLog(@"User cancelled login");

                // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
                [self showMessage:alertText withTitle:alertTitle];

                // For simplicity, here we just show a generic message for all other errors
                // You can learn how to handle other errors using our guide: https://developers.facebook.com/docs/ios/errors
            } else {
                //Get more error information from the error
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];

                // Show the user an error message
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                [self showMessage:alertText withTitle:alertTitle];
            }
        }
        // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
    }
}

// Show an alert message
- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:self
                      cancelButtonTitle:@"OK!"
                      otherButtonTitles:nil] show];
}

#pragma mark - Helpers
- (AFHTTPRequestOperation *)GETWithToken:(NSString *)URLString
                              parameters:(id)parameters
                                 success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                 failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSMutableDictionary *newParameter = parameters ? [parameters mutableCopy] : [NSMutableDictionary dictionary];
    NSString * token = [[NSUserDefaults standardUserDefaults] secretObjectForKey:kAPIAuthToken];
    if (token && token.length > 0) newParameter[@"auth_token"] = token;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    return [manager GET:URLString parameters:newParameter success:success failure:failure];
}

- (AFHTTPRequestOperation *)POSTWithToken:(NSString *)URLString
                               parameters:(id)parameters
                                  success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                  failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSMutableDictionary *newParameter = parameters ? [parameters mutableCopy] : [NSMutableDictionary dictionary];
    NSString * token = [[NSUserDefaults standardUserDefaults] secretObjectForKey:kAPIAuthToken];
    if (token && token.length > 0) newParameter[@"auth_token"] = token;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    return [manager POST:URLString parameters:newParameter success:success failure:failure];
}

- (AFHTTPRequestOperation *)DELETEWithToken:(NSString *)URLString
                                 parameters:(id)parameters
                                    success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSMutableDictionary *newParameter = parameters ? [parameters mutableCopy] : [NSMutableDictionary dictionary];
    NSString * token = [[NSUserDefaults standardUserDefaults] secretObjectForKey:kAPIAuthToken];
    if (token && token.length > 0) newParameter[@"auth_token"] = token;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    return [manager DELETE:URLString parameters:newParameter success:success failure:failure];
}



@end
