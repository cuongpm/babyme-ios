//
//  BMQuestionManager.m
//  BabyMe
//

#import "BMQuestionManager.h"
#import "NSString+Avatar.h"

@implementation BMQuestionManager
{
    NSMutableArray *questions;
    Question *question;

    NSMutableArray *watchLists;
    Question *watchItem;
    
    NSMutableArray *searchResults;
    Question *searchResult;
    
    NSMutableArray *myQuestions;
    
    NSMutableArray *myBookmarkQuestions;
}

static BMQuestionManager *shareInstance;

- (id)init
{
    if (self = [super init]) {
        [self loadQuestionListWithSuccessBlock:^{
        }];
    }
    return self;
}

#pragma mark - Singleton

+ (BMQuestionManager *)sharedInstance {
    if (shareInstance == nil) {
        shareInstance = [[BMQuestionManager alloc] init];
    }
    return shareInstance;
}

+ (void)removeSharedInstance {
    shareInstance = nil;
}

#pragma mark - Getter
-(NSArray*)questionList {
    return questions;
}

-(NSArray*)watchList {
    return watchLists;
}

-(NSArray*)searchResultList {
    return searchResults;
}

-(NSArray*)myQuestionList {
    return myQuestions;
}

-(NSArray*)myBookmarkQuestionList {
    return myBookmarkQuestions;
}

#pragma mark - Call API
-(void)loadQuestionListWithSuccessBlock:(void (^)())success {
    // Load question from network
    if (!BABY_ID) {
        return;
    }
    
    [ApplicationDelegate.networkEngine getQuestionOf:BABY_ID
                                     completionBlock:^(id responseObject) {
                                         questions = [NSMutableArray array];
                                         for (NSDictionary *tempDict in responseObject[@"data"]){
                                             question = [[Question alloc] initWithQuestionDictionary:tempDict];
                                             [questions addObject:question];
                                         }
                                         [[NSUserDefaults standardUserDefaults] setSecretObject:questions forKey:kQuestionData];
                                         success();
                                     } errorBlock:^(NSError *error) {
                                         [SIAlertView showAlertWithTitle:nil message:@"Có lỗi tải dữ liệu" cancelButtonTitle:kDismissText];
                                     }];
}

-(void)loadQuestionListFromWeek:(int)babyWeek
                    beforeBirth:(BOOL)before_birth
                    currentPage:(int) page
               withSuccessBlock:(void (^)())success {
    // Load question from network
    [ApplicationDelegate.networkEngine getQuestionOfWeek:babyWeek
                                             beforeBirth:before_birth
                                             currentPage:page
                                     completionBlock:^(id responseObject) {
                                         questions = [NSMutableArray array];
                                         for (NSDictionary *tempDict in responseObject[@"data"]){
                                             question = [[Question alloc] initWithQuestionDictionary:tempDict];
                                             [questions addObject:question];
                                         }
                                         [[NSUserDefaults standardUserDefaults] setSecretObject:questions forKey:kQuestionData];
                                         success();
                                     } errorBlock:^(NSError *error) {
                                         [SIAlertView showAlertWithTitle:nil message:@"Có lỗi tải dữ liệu" cancelButtonTitle:kDismissText];
                                     }];
}

-(void)loadWatchListWithSuccessBlock:(void (^)())success {
    // Load bookmark from network
    [ApplicationDelegate.networkEngine getWatchListCompletionBlock:^(id responseObject) {
        watchLists = [NSMutableArray array];
        for (NSDictionary *tempDict in responseObject[@"data"]){
            watchItem = [[Question alloc] initWithQuestionDictionary:tempDict];
            [watchLists addObject:watchItem];
        }
        [[NSUserDefaults standardUserDefaults] setSecretObject:watchLists forKey:kBookmarkData];
        success();
    } errorBlock:^(NSError *error) {
        [SIAlertView showAlertWithTitle:nil message:@"Có lỗi tải dữ liệu" cancelButtonTitle:kDismissText];
    }];
}
-(void)loadMyQuestionListWithSuccessBlock:(void (^)())success {
    // Load my questions from network
    [SVProgressHUD showWithStatus:@"Đang cập nhật dữ liệu" maskType:SVProgressHUDMaskTypeBlack];
    [ApplicationDelegate.networkEngine getMyQuestionCompletionBlock:^(id responseObject) {
        myQuestions = [NSMutableArray array];
        for (NSDictionary *tempDict in responseObject[@"data"]){
            [myQuestions addObject:[[Question alloc] initWithQuestionDictionary:tempDict]];
        }
        [SVProgressHUD dismiss];
        success();
    } errorBlock:^(NSError *error) {
        [SVProgressHUD dismiss];
        [SIAlertView showAlertWithTitle:nil message:@"Có lỗi tải dữ liệu" cancelButtonTitle:kDismissText];
    }];
}

-(void)loadBookmarkQuestionListWithSuccessBlock:(void (^)())success {
    // Load my questions from network
    [SVProgressHUD showWithStatus:@"Đang cập nhật dữ liệu" maskType:SVProgressHUDMaskTypeBlack];
    [ApplicationDelegate.networkEngine getBookmarkQuestionCompletionBlock:^(id responseObject) {
        myBookmarkQuestions = [NSMutableArray array];
        for (NSDictionary *tempDict in responseObject[@"data"]){
            [myBookmarkQuestions addObject:[[Question alloc] initWithQuestionDictionary:tempDict]];
        }
        [SVProgressHUD dismiss];
        success();
    } errorBlock:^(NSError *error) {
        [SVProgressHUD dismiss];
        [SIAlertView showAlertWithTitle:nil message:@"Có lỗi tải dữ liệu" cancelButtonTitle:kDismissText];
    }];
}


-(AFHTTPRequestOperation*)startSearchBy:(NSString *)keywordInput withSucessBlock:(void (^)())success {
    return
    [ApplicationDelegate.networkEngine findQuestionBy:keywordInput
                                      completionBlock:^(id responseObject) {
                                          searchResults = [NSMutableArray array];
                                          for (NSDictionary *tempDict in responseObject[@"data"]){
                                              searchResult = [[Question alloc] initWithQuestionDictionary:tempDict];
                                              [searchResults addObject:searchResult];
                                          }
                                          success();
                                      } errorBlock:^(NSError *error) {
                                      }];
}

-(void)getQuestionDetailOf:(int)questionID
          withSuccessBlock: (void (^) ())successBlock
             andErrorBlock: (void (^) ())errorBlock {
    [ApplicationDelegate.networkEngine getQuestionDetailOf:questionID
                                           completionBlock:^(id responseObject) {
                                               questions = [NSMutableArray array];
                                               question = [[Question alloc] initWithQuestionDictionary:responseObject];
                                               [questions addObject:question];
                                               successBlock();
                                           } errorBlock:^(NSError *error) {
                                               errorBlock();
                                           }];
}

-(void)loadRelatedQuestionListOf:(int)questionID
                withSuccessBlock:(void (^)())successBlock
                   andErrorBlock:(void (^)())errorBlock {
    // Load question from network
    [ApplicationDelegate.networkEngine getRelatedQuestionOf:(int) questionID
                                     completionBlock:^(id responseObject) {
                                         questions = [NSMutableArray array];
                                         for (NSDictionary *tempDict in responseObject[@"data"]){
                                             question = [[Question alloc] initWithQuestionDictionary:tempDict];
                                             [questions addObject:question];
                                         }
                                         [SVProgressHUD dismiss];
                                         successBlock();
                                     } errorBlock:^(NSError *error) {
                                         errorBlock();
                                     }];
}

#pragma mark - Get lists
-(NSArray *)getQuestionList:(NSArray *)questionObjectList
{
    NSMutableArray *questionArray = [NSMutableArray array];
    for (Question *tempQuestion in questionObjectList) {
        [questionArray addObject:tempQuestion.question];
    }
    return questionArray;
}

-(NSArray *)getAnswerList:(NSArray *)questionObjectList
{
    NSMutableArray *answerArray = [NSMutableArray array];
    for (Question *tempAnswer in questionObjectList) {
        [answerArray addObject:tempAnswer.answer];
    }
    return answerArray;
}

-(NSArray *)getAvatarList:(NSArray *)questionObjectList
{
    NSMutableArray *avatarArray = [NSMutableArray array];
    for (Question *tempAvatar in questionObjectList) {
        NSURL * t = [tempAvatar.email avatarFromEmail];
        [avatarArray addObject:t.absoluteString];
    };
    return avatarArray;
}

#pragma mark - Check to enabler ask question function
-(void)checkToEnableAskQuestionFunction{
    BMNetworkEngine * networkEngine = [BMNetworkEngine new];
    [networkEngine GET:[NSString stringWithFormat:@"%@settings",kBaseURL] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            // Get ask_question setting
            NSNumber * askQuestion = [responseObject objectForKey:@"ask_question"];
            [self enablerAskQuestionFunction:askQuestion.boolValue];
           
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Do nothing
        NSLog(@"Failed");
    }];
}

-(void)enablerAskQuestionFunction:(BOOL)isEnable{
    _isEnableAskQuestion = isEnable;
    // Post notification
     [[NSNotificationCenter defaultCenter] postNotificationName:UBL_SETTING_ASK_QUESTION object:nil];
}


@end
