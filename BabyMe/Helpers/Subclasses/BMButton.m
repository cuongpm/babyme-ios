//
//  BMButton.m
//  BabyMe
//

#import "BMButton.h"

@implementation BMButton

- (instancetype)init
{
    if (self = [super init]) {
        [self initialize];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self initialize];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self initialize];
    }
    
    return self;
}

- (void)awakeFromNib
{
    [self initialize];
}

- (void)initialize
{
    // Add border
    [self changeBorder:(_borderWidth  == 0 ? 2 : _borderWidth)
                radius:(_borderRadius == 0 ? self.frame.size.height/2 : _borderRadius)
                 color:(!_borderColor ? [UIColor colorWithRed:20/255.0f green:185/255.0f blue:82/255.0f alpha:1.0f] : _borderColor)];
}

- (void)changeBorder:(CGFloat)width
              radius:(CGFloat)radius
               color:(UIColor *)color
{
    self.layer.cornerRadius = radius;
    self.layer.borderWidth = width;
    self.layer.borderColor = color.CGColor;
    [self setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 5.0, 0.0, 5.0)];
}

@end
