//
//  BMFadeSegue.m
//  BabyMe
//

#import <QuartzCore/QuartzCore.h>
#import "BMFadeSegue.h"

@implementation BMFadeSegue

- (void)perform
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionFade;
    
    [[[[[self sourceViewController] view] window] layer] addAnimation:transition
                                                               forKey:kCATransitionFade];
    
    [[self sourceViewController]
     presentViewController:[self destinationViewController]
     animated:NO completion:NULL];
    }

@end
