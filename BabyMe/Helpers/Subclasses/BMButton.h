//
//  BMButton.h
//  BabyMe
//

@interface BMButton : UIButton

@property (nonatomic) float borderWidth;
@property (nonatomic) float borderRadius;
@property (nonatomic, strong) UIColor *borderColor;

- (void)changeBorder:(CGFloat)width
              radius:(CGFloat)radius
               color:(UIColor *)color;
@end
