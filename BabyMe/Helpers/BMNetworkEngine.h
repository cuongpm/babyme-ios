//
//  BMNetworkEngine.h
//  BabyMe
//

#import "AFHTTPRequestOperationManager.h"
#import <FacebookSDK/FacebookSDK.h>

typedef void (^BMSuccessBlock)(id responseObject);
typedef void (^BMFailureBlock)(NSError *error);

@interface BMNetworkEngine : AFHTTPRequestOperationManager

- (AFHTTPRequestOperation *)getNotificationOf:(NSString *)babyid
                              completionBlock:(BMSuccessBlock)completionBlock
                                   errorBlock:(BMFailureBlock)errorBlock;


// User

- (AFHTTPRequestOperation*)loginWithEmail:(NSString *)email
                                 password:(NSString *)password
                          completionBlock:(BMSuccessBlock)completionBlock
                               errorBlock:(BMFailureBlock)errorBlock;


- (AFHTTPRequestOperation*)loginWithFacebookToken:(NSString *)facebookToken
                            andCreateBabyWithName:(NSString *)babyname
                                           gender:(NSString *)gender
                                         birthday:(NSString *)birthday
                                  completionBlock:(BMSuccessBlock)completionBlock
                                       errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation*)signUpWithEmail:(NSString *)email
                                  password:(NSString *)password
                                   baby_id:(NSString *)baby_id
                           completionBlock:(BMSuccessBlock)completionBlock
                                errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation *)getMyQuestionCompletionBlock:(BMSuccessBlock)completionBlock
                                              errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation *)getBookmarkQuestionCompletionBlock:(BMSuccessBlock)completionBlock
                                                    errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation *)resetPasswordWithEmail:(NSString*)email
                                 completionHandler:(void (^)(void))completionBlock
                                      errorHandler:(void (^)(NSString * errorMessage))errorBlock;
// Baby
- (AFHTTPRequestOperation*)sendBabyInfoWithName:(NSString *)name
                                         gender:(NSString *)gender
                                       birthday:(NSString *)birthday
                                        user_id:(NSString *)user_id
                                completionBlock:(BMSuccessBlock)completionBlock
                                     errorBlock:(BMFailureBlock)errorBlock;
- (AFHTTPRequestOperation*)updateBabyInfoWithId:(NSInteger)babyId
                                           name:(NSString *)name
                                         gender:(NSString *)gender
                                       birthday:(NSString *)birthday
                                completionBlock:(BMSuccessBlock)completionBlock
                                     errorBlock:(BMFailureBlock)errorBlock;

// Homepage
- (AFHTTPRequestOperation *)getBabyAge:(NSString *)babyid
                       completionBlock:(BMSuccessBlock)completionBlock
                            errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation *)getArticleOf:(NSString *)babyid
                         completionBlock:(BMSuccessBlock)completionBlock
                              errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation *)getReminderOf:(NSString *)babyid
                          completionBlock:(BMSuccessBlock)completionBlock
                               errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation *)getQuestionOf:(NSString *)babyid
                          completionBlock:(BMSuccessBlock)completionBlock
                               errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation *)getWatchListCompletionBlock:(BMSuccessBlock)completionBlock
                                            errorBlock:(BMFailureBlock)errorBlock;

// Homepage by week
- (AFHTTPRequestOperation *)getArticleOfWeek:(int)week
                                 beforeBirth:(BOOL)before_birth
                             completionBlock:(BMSuccessBlock)completionBlock
                                  errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation *)getReminderOfWeek:(int)week
                                  beforeBirth:(BOOL)before_birth
                                  currentPage:(int) page
                              completionBlock:(BMSuccessBlock)completionBlock
                                   errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation *)getQuestionOfWeek:(int)week
                                  beforeBirth:(BOOL)before_birth
                                  currentPage:(int) page
                              completionBlock:(BMSuccessBlock)completionBlock
                                   errorBlock:(BMFailureBlock)errorBlock;

// Question Detail
- (AFHTTPRequestOperation *)bookmarkQuestion:(int)questionID
                             completionBlock:(BMSuccessBlock)completionBlock
                                  errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation *)unBookmarkQuestion:(int)questionID
                               completionBlock:(BMSuccessBlock)completionBlock
                                    errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation *)findQuestionBy:(NSString *)keyword
                           completionBlock:(BMSuccessBlock)completionBlock
                                errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation *)getQuestionDetailOf:(int)questionID
                                completionBlock:(BMSuccessBlock)completionBlock
                                     errorBlock:(BMFailureBlock)errorBlock;

// Article detail
- (AFHTTPRequestOperation *)getArticleDetailOf:(int)articleID
                               completionBlock:(BMSuccessBlock)completionBlock
                                    errorBlock:(BMFailureBlock)errorBlock;

// Related question/article
- (AFHTTPRequestOperation *)getRelatedQuestionOf:(int)questionID
                                 completionBlock:(BMSuccessBlock)completionBlock
                                      errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation *)getRelatedArticleOf:(int)category_id
                                completionBlock:(BMSuccessBlock)completionBlock
                                     errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation *)getRelatedArticleByTag:(int)tag_id
                                   completionBlock:(BMSuccessBlock)completionBlock
                                        errorBlock:(BMFailureBlock)errorBlock;
// Comment/Like
- (AFHTTPRequestOperation *)commentFor:(int)questionID
                           withContent:(NSString *) content
                       completionBlock:(BMSuccessBlock)completionBlock
                            errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation *)likeFor:(int)questionID
                    completionBlock:(BMSuccessBlock)completionBlock
                         errorBlock:(BMFailureBlock)errorBlock;

- (AFHTTPRequestOperation *)unLikeFor:(int)questionID
                      completionBlock:(BMSuccessBlock)completionBlock
                           errorBlock:(BMFailureBlock)errorBlock;

// Create question
- (AFHTTPRequestOperation *)createQuestionWithTitle:(NSString *)title
                                            baby_id:(NSString *) baby_id
                                    completionBlock:(BMSuccessBlock)completionBlock
                                         errorBlock:(BMFailureBlock)errorBlock;

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error;


-(AFHTTPRequestOperation *)loadOpenAskQuestionFunctionCompletionBlock:(BMSuccessBlock)completionBlock
                                                           errorBlock:(BMFailureBlock)errorBlock;

@end