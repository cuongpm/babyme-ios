//
//  BMQuestionManager.h
//  BabyMe
//

#import <Foundation/Foundation.h>
#import "Question.h"

@interface BMQuestionManager : NSObject

+ (BMQuestionManager *)sharedInstance;

@property (nonatomic, readonly) BOOL isEnableAskQuestion;
@property (nonatomic, readonly) NSArray * questionList;
@property (nonatomic, readonly) NSArray * watchList;
@property (nonatomic, readonly) NSArray * searchResultList;
@property (nonatomic, readonly) NSArray * myQuestionList;
@property (nonatomic, readonly) NSArray * myBookmarkQuestionList;

-(void)loadQuestionListWithSuccessBlock:(void (^)())success;
-(void)loadQuestionListFromWeek:(int)babyWeek
                    beforeBirth:(BOOL)before_birth
                    currentPage:(int)page
               withSuccessBlock:(void (^)())success;

-(void)loadWatchListWithSuccessBlock:(void (^)())success;

-(void)loadMyQuestionListWithSuccessBlock:(void (^)())success;
-(void)loadBookmarkQuestionListWithSuccessBlock:(void (^)())success;

-(AFHTTPRequestOperation*)startSearchBy:(NSString *)keywordInput
     withSucessBlock:(void (^)())success;

-(void)getQuestionDetailOf:(int)questionID
          withSuccessBlock: (void (^) ())successBlock
             andErrorBlock: (void (^) ())errorBlock;

-(void)loadRelatedQuestionListOf:(int)questionID
                withSuccessBlock:(void (^)())successBlock
                   andErrorBlock:(void (^)())errorBlock;

-(NSArray *)getQuestionList:(NSArray *)questionObjectList;
-(NSArray *)getAnswerList:(NSArray *)questionObjectList;
-(NSArray *)getAvatarList:(NSArray *)questionObjectList;
-(void)checkToEnableAskQuestionFunction;

@end
