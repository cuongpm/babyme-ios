//
//  BMAppDelegate.h
//  BabyMe
//

#import <UIKit/UIKit.h>
#import "BMNetworkEngine.h"
#import "BMRootSideMenuViewController.h"

#define ApplicationDelegate ((BMAppDelegate *)[UIApplication sharedApplication].delegate)

@interface BMAppDelegate : UIResponder <UIApplicationDelegate>{
    BOOL isAppInBackground;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) BMNetworkEngine *networkEngine;

@end
