//
//  BMReminderTableViewCell.h
//  BabyMe
//

#import <UIKit/UIKit.h>

@interface BMReminderTableViewCell : UITableViewCell

@property (readwrite, copy, nonatomic) NSString *reuseIdentifier;
@property (strong, nonatomic) IBOutlet UILabel *reminderContent;
@property (weak, nonatomic) IBOutlet UILabel *reminderTitle;
@property (weak, nonatomic) IBOutlet UIImageView *reminderAvatar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentToTitleContraint;

@end
