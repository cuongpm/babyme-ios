//
//  BMCommentViewCell.m
//  BabyMe
//

#import "BMCommentViewCell.h"
#import "UIImageView+WebCache.h"
#import "NSString+Avatar.h"
#import <QuartzCore/QuartzCore.h>

@implementation BMCommentViewCell {
    NSDictionary * _comment;
    UIImage * placeHolder;
}

-(void)awakeFromNib {
    [super awakeFromNib];
    placeHolder = self.avatar.image;

    // Make avatar round with grey border
    self.avatar.layer.cornerRadius = self.avatar.width/2;
    self.avatar.layer.borderColor = UIColorFromRGB(0xbdbdbd).CGColor;
    self.avatar.layer.borderWidth = 1.0;
}

-(void)setComment:(NSDictionary *)comment {
    _comment = comment;
    self.name.text = comment[@"author"];
    self.body.text = comment[@"content"];

    // Parse avatar ID from email
    NSString * email = comment[@"author_email"];
    [self.avatar sd_setImageWithURL:[email avatarFromEmail] placeholderImage:placeHolder];
}

-(void)dealloc {
    _comment = nil;
    placeHolder = nil;
}

@end
