//
//  BMQuestionTableViewCell.h
//  BabyMe
//

#import <UIKit/UIKit.h>

@interface BMQuestionTableViewCell : UITableViewCell

@property (readwrite, copy, nonatomic) NSString *reuseIdentifier;

@property (strong, nonatomic) IBOutlet UILabel *questionContent;
@property (strong, nonatomic) IBOutlet UILabel *answerContent;
@property (strong, nonatomic) IBOutlet UILabel *askQuestion;
@property (weak, nonatomic) IBOutlet UIImageView *questionerAvatar;

@end
