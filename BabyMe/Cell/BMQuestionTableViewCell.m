//
//  BMQuestionTableViewCell.m
//  BabyMe
//

#import "BMQuestionTableViewCell.h"

@implementation BMQuestionTableViewCell

@synthesize reuseIdentifier = _reuseIdentifier;

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
    self.reuseIdentifier = @"QuestionCell";
    self.questionContent.numberOfLines = 0;
    self.questionerAvatar.layer.cornerRadius = self.questionerAvatar.width/2;
    self.questionerAvatar.layer.borderColor = UIColorFromRGB(0xbdbdbd).CGColor;
    self.questionerAvatar.layer.borderWidth = 1.0;
}

@end
