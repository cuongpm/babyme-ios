//
//  BMWebViewFooter.m
//  BabyMe
//

#import "BMWebViewFooter.h"

@implementation BMWebViewFooter
{

    IBOutlet BMWebViewFooter *view;
}

- (id)init
{
    // Load view from xib 
    [[NSBundle mainBundle] loadNibNamed:@"BMWebViewFooter" owner:self options:nil];
    
    // Determine frame
    CGRect frame = view.frame;
    
    if (self = [super initWithFrame:frame]) {
        // Add the view from xib onto view itself
        [self addSubview:view];
    }
    
    return self;
}@end
