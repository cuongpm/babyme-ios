//
//  BMReminderTableViewCell.m
//  BabyMe
//

#import "BMReminderTableViewCell.h"

@implementation BMReminderTableViewCell

@synthesize reuseIdentifier = _reuseIdentifier;

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.reuseIdentifier = @"ReminderCell";
    self.reminderTitle.numberOfLines = self.reminderContent.numberOfLines = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
