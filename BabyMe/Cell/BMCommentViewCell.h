//
//  BMCommentViewCell.h
//  BabyMe
//

#import "BMButton.h"

@interface BMCommentViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel  * name;
@property (weak, nonatomic) IBOutlet UILabel  * body;
@property (weak, nonatomic) IBOutlet UIButton * likeButton;
@property (weak, nonatomic) IBOutlet UIImageView  * avatar;

@property (nonatomic, strong) NSDictionary * comment;

@end
