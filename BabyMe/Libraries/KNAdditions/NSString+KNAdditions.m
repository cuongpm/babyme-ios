//
//  NSString+NSString_KNAdditions.m
//  AirPay
//

#import "NSString+KNAdditions.h"

@implementation NSString (KNAdditions)

-(NSString*)stringByRemovingAccents {
    // Remove any accents including special cases
    NSString * a = [self stringByReplacingOccurrencesOfString:@"đ" withString:@"d"];
               a = [a stringByReplacingOccurrencesOfString:@"Đ" withString:@"D"];
    return [a stringByFoldingWithOptions:NSDiacriticInsensitiveSearch locale:[NSLocale currentLocale]];
}

@end
