//
//  NSDictionary+UrlEncoding.h
//  AirPay
//

#import <Foundation/Foundation.h>

@interface NSDictionary (UrlEncoding)

-(NSString*) urlEncodedString;

@end