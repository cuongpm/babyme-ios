//
//  NSString+Avatar.m
//

#import "NSString+Avatar.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (Avatar)

- (NSURL*)avatarFromEmail
{
    // Parse avatar ID from email
    NSString * email = [[self stringByReplacingOccurrencesOfString:@" " withString:@""] lowercaseString];
    NSString * avatar;
    if ([email rangeOfString:@"facebook.com"].location != NSNotFound) {
        avatar = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=normal", [email stringByReplacingOccurrencesOfString:@"@facebook.com" withString:@""]];
    } else {
        avatar = [NSString stringWithFormat:@"http://www.gravatar.com/avatar/%@?d=404&s=100", [email MD5]];
    }
    return [NSURL URLWithString:avatar];
}

- (NSString*)MD5
{
	// Create pointer to the string as UTF8
	const char *ptr = [self UTF8String];
    
	// Create byte array of unsigned chars
	unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
	// Create 16 byte MD5 hash value, store in buffer
	CC_MD5(ptr, (CC_LONG)strlen(ptr), md5Buffer);
    
	// Convert MD5 value in the buffer to NSString of hex values
	NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
	for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
		[output appendFormat:@"%02x",md5Buffer[i]];
    
	return output;
}


@end
