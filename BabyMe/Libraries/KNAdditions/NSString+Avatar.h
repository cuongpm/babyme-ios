//
//  NSString+Avatar.h
//

#import <Foundation/Foundation.h>

@interface NSString (Avatar)

- (NSURL*)avatarFromEmail;
- (NSString*)MD5;

@end
