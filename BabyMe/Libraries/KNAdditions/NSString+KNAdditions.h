//
//  NSString+KNAdditions.h
//  AirPay
//

@interface NSString (KNAdditions)

-(NSString*)stringByRemovingAccents;

@end