//
//  UIImage+PHAdition.h
//  Kraken
//


#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface UIImage (PHAdition)

+ (UIImage*)resizeImage:(UIImage *)image toFitSize:(CGSize)targetSize;
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
+ (UIImage*)imageWithColor:(UIColor *)color;
+ (UIImage *)imageWithView:(UIView *)view;

- (UIImage *)stackBlur:(NSUInteger)radius;
- (UIImage *)crop:(CGRect)rect;
- (UIImage *) normalize ;

+ (UIImage *)screenshotFromPlayer:(AVPlayer *)player
                      maximumSize:(CGSize)maxSize;

@end
