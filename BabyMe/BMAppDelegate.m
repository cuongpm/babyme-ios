//
//  BMAppDelegate.m
//  BabyMe
//
#import "BMQuestionManager.h"
#import "BMAppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import "BMWalkthroughPageContentViewController.h"
#import "BMRootSideMenuViewController.h"
#import <Parse/Parse.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "ACTReporter.h"
#import "iRate.h"

@implementation BMAppDelegate

static NSDate * lastUpdateCheck;

+ (void)initialize
{
    //configure iRate
    [iRate sharedInstance].appStoreID = 894635646; // Replace this
    [iRate sharedInstance].daysUntilPrompt = 5;
    [iRate sharedInstance].usesUntilPrompt = 15;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    isAppInBackground = NO;
    
    [Parse setApplicationId:@"WwSsmDgaBIZXz43tkIn1i6MW12lBB4ggF6X8O59P"
                  clientKey:@"KdAlOWZhLtXF9VuJF8NCuIGr7o4W5jkJTBWs3qx5"];
    
    [ACTAutomatedUsageTracker enableAutomatedUsageReportingWithConversionID:@"951759137"];
    
    [ACTConversionReporter reportWithConversionID:@"951759137" label:@"yoC0CO_SgloQoeLqxQM" value:@"0.99" isRepeatable:NO];
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        #ifdef __IPHONE_8_0
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge
                                                                                             |UIRemoteNotificationTypeSound
                                                                                             |UIRemoteNotificationTypeAlert) categories:nil];
        [application registerUserNotificationSettings:settings];
        #endif
    } else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [application registerForRemoteNotificationTypes:myTypes];
    }
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
      // Enable anonymous user
    [PFUser enableAutomaticUser];
    
    // Google Analytics
    // 1
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // 2
    [[GAI sharedInstance].logger setLogLevel:kGAILogLevelVerbose];
    
    // 3
    [GAI sharedInstance].dispatchInterval = 20;
    
    // 4
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-59105165-1"];
    [GAI sharedInstance].defaultTracker = tracker;

    // Secure key
    [[NSUserDefaults standardUserDefaults] setSecret:@"Y!p*THxdGC787sdXApJs^ThwCF*de"];

    // Initialize APi Engine
    self.networkEngine = [[BMNetworkEngine alloc] init];

    // Check logged in and skip walkthrough
    if (LOGGED_IN || LOGGED_IN_GUEST) {
        [[BMQuestionManager sharedInstance] loadQuestionListWithSuccessBlock:^{
            
        }];
        self.window.rootViewController = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"rootSideMenuNavigationController"];
    }

    // Notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doLogout) name:kNotificationShouldLogout object:nil];
    [Fabric with:@[CrashlyticsKit]];
    
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]) {
        NSDictionary *notifMessage = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        // get object_id
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
             [[NSNotificationCenter defaultCenter] postNotificationName:@"BM_DID_RECEIVE_NOTIFICATION" object:notifMessage];
        });
    }

    
    return YES;
}




#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}
#endif

- (void)application: (UIApplication *) application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData: deviceToken];

    [currentInstallation addUniqueObject:[NSString stringWithFormat:@"Everyone"] forKey:@"channels"];
    [currentInstallation saveInBackground];
    
    NSString *tokenString = [NSString
                             stringWithFormat:@"%@",deviceToken];
    tokenString = [tokenString substringFromIndex:1];
    NSInteger stringLength = [tokenString length];
    // trim all spaces
    tokenString = [[tokenString substringToIndex:stringLength - 1] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"APN  %@",tokenString);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"ERROR didFailToRegisterForRemoteNotificationsWithError %@",error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
//    [PFPush handlePush:userInfo];
    
    // If app is in background ... open notifis screen
    if ((LOGGED_IN || LOGGED_IN_GUEST) && !isAppInBackground) {
        
    }
    
    if (isAppInBackground) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"BM_DID_RECEIVE_NOTIFICATION" object:userInfo];
    }
    
    
}

- (void)showLoginScreen {
    self.window.rootViewController = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"rootLoginNavigationController"];
}

-(void)applicationWillResignActive:(UIApplication *)application{
    isAppInBackground = YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    isAppInBackground = NO;
    
    [[BMQuestionManager sharedInstance] checkToEnableAskQuestionFunction];
    // ADd Fb ads
    [FBSettings setDefaultAppID:kFBAppId];
    [FBAppEvents activateApp];
    // Should check for update?
    if (lastUpdateCheck != nil && ABS([lastUpdateCheck timeIntervalSinceNow]) < 60*5) return;
    
    // Detect update
    NSDictionary *updateDictionary = [NSDictionary dictionaryWithContentsOfURL:[NSURL URLWithString:APP_ENTERPRISE_PLIST]];
    lastUpdateCheck = [NSDate date];
    if(updateDictionary) {
        BOOL updateAvailable = NO;
        NSArray *items = [updateDictionary objectForKey:@"items"];
        NSDictionary *itemDict = [items lastObject];
        NSDictionary *metaData = [itemDict objectForKey:@"metadata"];
        NSString *newversion = [metaData valueForKey:@"bundle-version"];
        NSString *currentversion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
        updateAvailable = [newversion compare:currentversion options:NSNumericSearch] == NSOrderedDescending;
        if (updateAvailable) {
            [UIAlertView showWithTitle:@"Có phiên bản thử nghiệm mới"
                               message:[NSString stringWithFormat:@"Bạn có muốn nâng cấp phiên bản hiện tại (%@) lên phiên bản mới (%@) không?", currentversion, newversion]
                     cancelButtonTitle:@"Bỏ qua"
                     otherButtonTitles:@[@"Nâng cấp"]
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  if (buttonIndex != alertView.cancelButtonIndex) {
                                      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/sg/app/babyme/id894635646?mt=8"]];
                                  }
                              }];
        }
    }
}

// During the Facebook login flow, your app passes control to the Facebook iOS app or Facebook in a mobile browser.
// After authentication, your app will be called back with the session information.
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    
////    NSString *urlString = [url @"https://click.google-analytics.com/redirect?tid=UA-59105165-1&url=https%3A%2F%2Fitunes.apple.com%2Fsg%2Fapp%2Fbabyme%2Fid894635646%3Fmt%3D8&aid=vn.babyme&idfa={idfa}&cs=google_ads&hash=f6190ffb055c465eae06cb71fe3d5848"];
//    NSString *urlString = [url absoluteString];
//
//    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-59105165-1"];
//    GAIDictionaryBuilder *hitParams = [[GAIDictionaryBuilder alloc] init];
//    [[hitParams setCampaignParametersFromUrl:urlString] build];
//    if(![hitParams valueForKey:kGAICampaignSource] && [url host].length !=0) {
//        // Set campaign data on the map, not the tracker.
//        [hitParams set:@"referrer" forKey:kGAICampaignMedium];
//        [hitParams set:[url host] forKey:kGAICampaignSource];
//    }
//    NSDictionary *hitParamsDict = [hitParams build];
//    [tracker send:[[[GAIDictionaryBuilder createScreenView] setAll:hitParamsDict] build]];
    
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}

- (void) doLogout {
    // Pop to root (login screen)
    self.window.rootViewController = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"rootLogin"];

    // Clear data
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    // Save loggout status
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kLoggedOut];

}
@end

@implementation UINavigationController (StatusBarStyle)

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end