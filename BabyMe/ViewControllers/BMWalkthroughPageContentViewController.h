//
//  BMPageContentViewController.h
//  BabyMe
//

#import <UIKit/UIKit.h>
#import "BMButton.h"
#import "BMWalkthroughPageViewController.h"

@interface BMWalkthroughPageContentViewController : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel * walkthroughLabel;
@property (weak, nonatomic) IBOutlet UIImageView *walkthroughImageView;

@property (weak, nonatomic) IBOutlet UIView *babyInfoView;

// Text field
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *babyNameTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *babyBirthdayTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *babyGenderTextField;

// Register, Submit Button

@property (weak, nonatomic) IBOutlet BMButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@property (weak, nonatomic) UIViewController *walkthroughViewController;


@property (strong, nonatomic) IBOutlet UIButton *term;

@property NSUInteger   pageIndex;
@property NSUInteger   totalPageIndex;
@property NSString     * walkthroughTitle;
@property NSString     * walkthroughImageName;

- (IBAction)exitButtonDidTouch:(id)sender;

@end