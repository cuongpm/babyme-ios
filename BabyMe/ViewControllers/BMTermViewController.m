//
//  BMTermViewController.m
//  BabyMe
//
//  Created by Cuong Pham on 2/10/15.
//  Copyright (c) 2015 Silicon Straits Saigon. All rights reserved.
//

#import "BMTermViewController.h"

@interface BMTermViewController ()

@end

@implementation BMTermViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImage setImage:[UIImage imageNamed:@"WalkthroughBackground.png"]];
    [backgroundImage setContentMode:UIViewContentModeScaleAspectFill];
    [self.view addSubview:backgroundImage];
    [self.view sendSubviewToBack:backgroundImage];

    _webView.delegate = self;
    // Add background image into view controller

    NSString *urlString = @"http://www.babyme.vn/terms";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:urlRequest];
    [SVProgressHUD showWithStatus:@"Đang tải dữ liệu..." maskType:SVProgressHUDMaskTypeBlack];
    UIColor *greenColor  = [UIColor colorWithRed:24.0/255.0 green:255.0/255.0 blue:19.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = greenColor;
    // Do any additional setup after loading the view.
    //    self.navigationController.navigationBar.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"CuongPM");
    [SVProgressHUD dismiss];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
