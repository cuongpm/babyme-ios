//
//  BMIntroViewController.h
//  BabyMe
//
//  Created by Cuong Pham on 3/2/15.
//  Copyright (c) 2015 Silicon Straits Saigon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BMIntroViewController : UIViewController

-(IBAction)exitScreen:(id)sender;
@end
