//
//  BMIntroductionViewController.h
//  BabyMe
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface BMIntroductionViewController : GAITrackedViewController

@property (weak, nonatomic) IBOutlet UILabel *versionApp;

@end
