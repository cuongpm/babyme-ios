//
//  BMFillBabyInfoViewController.m
//  BabyMe
//

#import "BMFillBabyInfoViewController.h"
#import "BMWalkthroughPageViewController.h"
#import "BMRootSideMenuViewController.h"
#import "BMQuestionManager.h"

@interface BMFillBabyInfoViewController ()

@end

@implementation BMFillBabyInfoViewController

NSArray *pickerDataCopy = nil;

NSString *babyName;
NSString *babyBirthday;
NSString *babyGender;
NSDictionary *babyDict;

UIToolbar* toolBar;
UIDatePicker *birthdayPicker;
UIPickerView *genderPicker;

- (void)viewDidLoad {
    // Add background image into view controller
    UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImage setImage:[UIImage imageNamed:@"WalkthroughBackground.png"]];
    [backgroundImage setContentMode:UIViewContentModeScaleAspectFill];
    [self.view addSubview:backgroundImage];
    [self.view sendSubviewToBack:backgroundImage];

    // Set baby name delegate
    self.babyNameTextField.delegate = self;
    
    // Initialize toolbar (Done button)
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(closePicker)];
    NSArray* barItems = [NSArray arrayWithObjects:flexibleItem, doneButton, nil];
    [toolBar setItems:barItems animated:YES];
    
    // Initialize birthday picker
    birthdayPicker= [[UIDatePicker alloc] init];
    birthdayPicker.datePickerMode = UIDatePickerModeDate;
    birthdayPicker.backgroundColor = [UIColor whiteColor];
    [birthdayPicker addTarget:self action:@selector(birthdayPickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    // Initialize gender picker
    genderPicker = [[UIPickerView alloc] init];
    genderPicker.backgroundColor = [UIColor whiteColor];
    genderPicker.delegate = self;
    
    // Change keyboard to picker when tapped
    [self.babyBirthdayTextField setInputView:birthdayPicker];
    [self.babyBirthdayTextField setInputAccessoryView:toolBar];
    
    pickerDataCopy = [NSArray arrayWithObjects:@"Chưa biết", @"Con trai", @"Con gái", nil];
    [self.babyGenderTextField setInputView:genderPicker];
    self.babyGenderTextField.text = pickerDataCopy[0];
    
    
    // Populate data for editing baby info
    if ([[NSUserDefaults standardUserDefaults] secretIntegerForKey:kBabyID] > 0) {
        self.closeButton.hidden = NO;
        self.babyNameTextField.text = [[NSUserDefaults standardUserDefaults] secretStringForKey:kBabyName];
        self.babyBirthdayTextField.text = [[NSUserDefaults standardUserDefaults] secretStringForKey:kBabyBirthday];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        [birthdayPicker setDate:[dateFormatter dateFromString:[[NSUserDefaults standardUserDefaults] secretStringForKey:kBabyBirthday]]];
        self.babyGenderTextField.text = [[NSUserDefaults standardUserDefaults] secretStringForKey:kBabyGender];
        for (int i = 0; i < pickerDataCopy.count; i++){
            if([pickerDataCopy[i] isEqualToString:[[NSUserDefaults standardUserDefaults] secretStringForKey:kBabyGender]]){
                [genderPicker selectRow:i inComponent:0 animated:NO];
            }
        }
        [self.submitButton setTitle:@"Lưu" forState:UIControlStateNormal];
    }
}

- (void)closePicker {
    [self.view endEditing:YES];
}

#pragma mark - Update birthday text field
- (void)birthdayPickerValueChanged:(UIDatePicker *)datePicker {
    // Set format for birthday text field
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:datePicker.date];
    
    // Update birthday text field
    self.babyBirthdayTextField.text = strDate;
}

#pragma mark - Gender picker delegate
// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return pickerDataCopy.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return pickerDataCopy[row];
}

// Update text field when select row
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    [self closePicker];
    self.babyGenderTextField.text = pickerDataCopy[row];
}

#pragma mark - Textfield delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Button did touch

- (IBAction)submitButtonDidTouch:(id)sender {
    // Prepare baby info
    [self prepareBabyInfo];
    
    // Show progress
    [SVProgressHUD showWithStatus:@"Đang lưu thông tin của bé" maskType:SVProgressHUDMaskTypeBlack];
    if ([[NSUserDefaults standardUserDefaults] secretIntegerForKey:kBabyID] > 0) {
        // Edit baby info
        [ApplicationDelegate.networkEngine updateBabyInfoWithId:[[NSUserDefaults standardUserDefaults] secretIntegerForKey:kBabyID]
                                                           name:babyName
                                                         gender:babyGender
                                                       birthday:babyBirthday
                                                completionBlock:^(id responseObject) {
                                                    // Store baby info
                                                    [self storeBabyInfoFromServer:responseObject];
                                                    
                                                    // Dismiss progress
                                                    [SVProgressHUD dismiss];
                                                    [self dismissViewControllerAnimated:YES completion:nil];
                                                } errorBlock:^(NSError *error) {
                                                    // Dismiss progress
                                                    [SVProgressHUD dismiss];
                                                    
                                                    // Show alert
                                                    [SIAlertView showAlertWithTitle:nil message:@"Có lỗi xảy ra. Vui lòng thử lại" cancelButtonTitle:kDismissText];
                                                }];
    }else{
        [ApplicationDelegate.networkEngine loginWithFacebookToken:(NSString *)FBSession.activeSession.accessTokenData
                                            andCreateBabyWithName:babyName
                                                           gender:babyGender
                                                         birthday:babyBirthday
                                                  completionBlock:^(id responseObject) {
                                                      NSLog(@"Facebook Token: %@", (NSString *)FBSession.activeSession.accessTokenData);
                                                      // Save baby info
                                                      NSDictionary *babies = [responseObject[@"babies"] lastObject];
                                                      [self storeBabyInfoFromServer:babies];
                                                      
                                                      // Dismiss progress
                                                      [SVProgressHUD dismiss];
                                                      
                                                      // Do sign in
                                                      [self doLogin];
                                                  } errorBlock:^(NSError *error) {
                                                      // Close FB session
                                                      [FBSession.activeSession closeAndClearTokenInformation];
                                                      
                                                      // Dismiss progress
                                                      [SVProgressHUD dismiss];
                                                      
                                                      // Show alert
                                                      [SIAlertView showAlertWithTitle:nil message:@"Có lỗi xảy ra. Vui lòng thử lại" cancelButtonTitle:kDismissText];
                                                  }];}
    }
- (IBAction)closeButtonDidTouch:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Helpers

- (BOOL)verifyBabyInfomation {
    // Check validation of baby name
    if (self.babyNameTextField.text.length < 1) {
        [SIAlertView showAlertWithTitle:nil message:@"Nhập lại tên của bé" cancelButtonTitle:kDismissText];
        [self.babyNameTextField becomeFirstResponder];
        return NO;
    }
    
    // Check validation of baby birthday
    if (self.babyBirthdayTextField.text.length < 1) {
        [SIAlertView showAlertWithTitle:nil message:@"Chọn ngày sinh/dự sinh cho bé" cancelButtonTitle:kDismissText];
        [self.babyBirthdayTextField becomeFirstResponder];
        return NO;
    }
    return YES;
}

- (void)prepareBabyInfo {
    babyName = self.babyNameTextField.text;
    babyBirthday = self.babyBirthdayTextField.text;
    
    if ([self.babyGenderTextField.text isEqualToString:pickerDataCopy[1]])
        babyGender = @"male";
    else if ([self.babyGenderTextField.text isEqualToString:pickerDataCopy[2]])
        babyGender = @"female";
    else babyGender = @"unknown";
}

- (void)storeBabyInfoFromServer:(NSDictionary *)dict {
    [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"id"] forKey:kBabyID];
    [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"name"] forKey:kBabyName];
    [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"dob"] forKey:kBabyBirthday];
    [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"gender"] forKey:kBabyGender];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)doLogin {
    [[BMQuestionManager sharedInstance] loadQuestionListWithSuccessBlock:^{
        
    }];
    BMRootSideMenuViewController *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"rootSideMenuNavigationController"];
    [self presentViewController:rootViewController animated:YES completion:nil];
}


@end
