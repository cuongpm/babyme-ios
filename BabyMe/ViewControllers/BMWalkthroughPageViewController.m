//
//  BMWalkthroughViewController.m
//  BabyMe
//

#import "BMWalkthroughPageViewController.h"

@interface BMWalkthroughPageViewController ()

@end

@implementation BMWalkthroughPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Add background image into view controller
    UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImage setImage:[UIImage imageNamed:@"WalkthroughBackground.png"]];
    [backgroundImage setContentMode:UIViewContentModeScaleAspectFill];
    [self.view addSubview:backgroundImage];
    [self.view sendSubviewToBack:backgroundImage];

    // Make UINavigationBar transparent
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    // Create walkthrough content
    _pageTitles = kWalkthroughContent;
    
    // Set data source
    self.dataSource = self;
    self.delegate = self;

    // Check log out status. If YES, jump to baby fill in view
    if (LOGGED_OUT) {
        BMWalkthroughPageContentViewController *viewController = [self viewControllerAtIndex:0];
        NSArray *viewControllers = @[viewController];
        [self setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    }
    
    else {
        // Set starting view controller
        BMWalkthroughPageContentViewController *startingViewController = [self viewControllerAtIndex:0];
        NSArray *viewControllers = @[startingViewController];
        [self setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    }
}

- (BMWalkthroughPageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count] + 1)) {
        return nil;
    }
    
    // Create a new view controller and pass data.
    BMWalkthroughPageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WalkthroughPageContentViewController"];
    pageContentViewController.walkthroughViewController = self;

    // Total page index
    pageContentViewController.totalPageIndex = 0;
    
    // Check each page index
    if (index < [self.pageTitles count]) {
//        pageContentViewController.walkthroughTitle = self.pageTitles[index];
//        pageContentViewController.walkthroughImageName = kWalkthoughImageNames[index];
    }
    else pageContentViewController.walkthroughTitle = nil;
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((BMWalkthroughPageContentViewController*) viewController).pageIndex;
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((BMWalkthroughPageContentViewController*) viewController).pageIndex;
    if (index == NSNotFound) {
        return nil;
    }
    index++;
    if (index == [self.pageTitles count] + 1) {
        return nil;
    }
    
    return 0;
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return (LOGGED_OUT) ? 2 : 0;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"registerSegue"]) {
        BMRegisterViewController *controller = [segue destinationViewController];
        controller.babyDict = _tempBabyDict;
    }
}

@end
