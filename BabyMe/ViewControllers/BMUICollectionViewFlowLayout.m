//
//  BMUICollectionViewFlowLayout.m
//  BabyMe
//
//  Created by Cuong Pham on 2/24/15.
//  Copyright (c) 2015 Silicon Straits Saigon. All rights reserved.
//

#import "BMUICollectionViewFlowLayout.h"

@implementation BMUICollectionViewFlowLayout

-(id)init{
    if (!(self = [super init])) return nil;
    
    self.itemSize = CGSizeMake(320, 345);
    self.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    self.minimumInteritemSpacing = 10.0f;
    self.minimumLineSpacing = 10.0f;
    
    return self;
}

@end
