//
//  BMLeftMenuViewController.m
//  BabyMe
//

#import "BMLeftMenuViewController.h"
#import "BMRootSideMenuViewController.h"
#import "BMRegisterViewController.h"
#import "UIViewController+RESideMenu.h"
#import "BMLoginViewController.h"
#import "BMMyQuestionViewController.h"
#import "BMIntroductionViewController.h"
#import "BMIntroViewController.h"
#import "BMNotificationViewController.h"

@interface DEMOLeftMenuViewController ()

@property (strong, readwrite, nonatomic) UITableView *tableView;

@end

@implementation DEMOLeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"LeftMenuScreen";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMyQuestion) name:@"ShowMyQuestionNotification" object:nil];

    self.tableView = ({
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, (self.view.frame.size.height - 70 * 4) / 2.0f, self.view.frame.size.width, 54 * 6) style:UITableViewStylePlain];
        tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.opaque = NO;
        tableView.backgroundColor = [UIColor clearColor];
        tableView.backgroundView = nil;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.bounces = NO;
        tableView.scrollsToTop = NO;
        tableView;
    });
    [self.view addSubview:self.tableView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openNotifs:) name:@"BM_DID_RECEIVE_NOTIFICATION" object:nil];//
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)openNotifs:(NSNotification *)n{
    if (!n) {
        [self _openNotifs:YES];
    }
    else{
        NSDictionary * userInfo = n.object;
        if ([userInfo isKindOfClass:[NSDictionary class]]) {
            NSDictionary * aps = [userInfo objectForKey:@"aps"];
            // Type
            NSString * type = [aps objectForKey:@"type"];
            if ([type isKindOfClass:[NSString class]]) {
                if ([type isEqualToString:@"push"]) {
                    // Mo qua reminder
                    [self _openNotifs:YES];
                }
                
            }
        }
    }
}

-(void)_openNotifs:(BOOL)isAnimatied{
    BMNotificationViewController * notifsVc = [[BMNotificationViewController alloc] init];
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:notifsVc] animated:(isAnimatied)];
    [self.sideMenuViewController hideMenuViewController];
}

-(void)viewDidAppear:(BOOL)animated {
    [self.tableView reloadData];
}
#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"homePageViewController"]]
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
        case 1:
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"babyInfoViewController"]]
                                                         animated:YES];
            [self.sideMenuViewController hideMenuViewController];
            break;
        
        case 2:
            if (LOGGED_IN) {
                BMMyQuestionViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"myQuestionViewController"];
                vc.myQuestionType = MyQuestionTypeCreated;
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:vc] animated:YES];
                [self.sideMenuViewController hideMenuViewController];
            }
            else {
                [SIAlertView showAlertWithTitle:nil
                                        message:@"Bạn cần đăng nhập để sử dụng tính năng này"
                              cancelButtonTitle:@"Đóng lại"
                                  okButtonTitle:@"ĐĂNG NHẬP"
                                        handler:^{
                                            // Go to login screen
                                            BMIntroViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"introViewController"];
                                            [self presentViewController:controller animated:YES completion:nil];
                                        }];
            }
            break;
        case 4: {
            BMIntroductionViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"IntroductionViewController"];
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:vc] animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
            break;
        case 5:
            if (LOGGED_IN) {
                [SIAlertView showAlertWithTitle:nil
                                        message:@"Bạn muốn đăng xuất và xóa tất cả dữ liệu ngay bây giờ?"
                              cancelButtonTitle:@"Đóng lại"
                                  okButtonTitle:@"ĐỒNG Ý"
                                        handler:^{
                                            [self dismissViewControllerAnimated:YES completion:nil];
                                            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationShouldLogout object:nil];
                                        }];
            }
            else {
                BMIntroViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"introViewController"];
                [self presentViewController:controller animated:YES completion:nil];
            }
            
        case 3:
        {
            [self openNotifs:nil];
            break;
        }
        default:
            break;
    }
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 54;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex {
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        if (indexPath.row == 0) {
            cell.textLabel.font = [UIFont fontWithName:@"Comfortaa" size:17];
        }
        else cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
        cell.selectedBackgroundView = [[UIView alloc] init];
    }
    NSMutableArray *titles=[[NSMutableArray alloc]initWithObjects:@"babyMe", @"Thông tin của bé", @"Câu hỏi của bạn",@"Thông báo", @"Giới thiệu", @"Đăng ký/Đăng nhập", nil];
    NSArray *images = @[@"Babyme Icon", @"Baby Info Icon", @"Question Icon",@"About Icon", @"About Icon", @"Register Icon"];
    if (LOGGED_IN) {
        [titles replaceObjectAtIndex:5 withObject:@"Đăng xuất"];
    }
    cell.textLabel.text = titles[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:images[indexPath.row]];
    
    return cell;
}

- (void)showMyQuestion {
    BMMyQuestionViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"myQuestionViewController"];
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:vc] animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}
@end
