//
//  BMForgotPasswordViewController.m
//  BabyMe
//
//  Created by Cuong Pham on 2/26/15.
//  Copyright (c) 2015 Silicon Straits Saigon. All rights reserved.
//

#import "BMForgotPasswordViewController.h"

@interface BMForgotPasswordViewController ()

@end

@implementation BMForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImage setImage:[UIImage imageNamed:@"WalkthroughBackground.png"]];
    [backgroundImage setContentMode:UIViewContentModeScaleAspectFill];
    [self.view addSubview:backgroundImage];
    [self.view sendSubviewToBack:backgroundImage];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)exitButtonDidTouch:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)sendPassword:(id)sender{
    
    [SVProgressHUD showWithStatus:@"Đang xử lý" maskType:SVProgressHUDMaskTypeBlack];
    
    [ApplicationDelegate.networkEngine resetPasswordWithEmail:_email.text
                                            completionHandler:^
    {
        [SVProgressHUD dismiss];
        [SIAlertView showAlertWithTitle:nil message:@"Đổi mật khẩu thành công. Vui lòng kiểm tra email trong vài phút tới." cancelButtonTitle:kDismissText];
        [self dismissViewControllerAnimated:YES completion:nil];
    } errorHandler:^(NSString *errorMessage) {
        [SVProgressHUD dismiss];
        [SIAlertView showAlertWithTitle:nil message:@"Email không tồn tại trên hệ thống." cancelButtonTitle:kDismissText];
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
