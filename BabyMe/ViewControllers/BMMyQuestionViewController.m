//
//  BMMyQuestionViewController.m
//  BabyMe
//

#import "BMMyQuestionViewController.h"
#import "BMQuestionManager.h"
#import "BMQuestionTableViewCell.h"
#import "BMQuestionDetailViewController.h"
#import "UIImageView+WebCache.h"

@implementation BMMyQuestionViewController {
    NSArray * myQuestions;
    NSArray * questionAvatars;
    NSMutableDictionary    *rowHeightCache;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"MyQuestionsScreen";
    
    // Format Button
    NSMutableAttributedString *attrBtn = [[NSMutableAttributedString alloc] init];
    UIColor *defaulColor = [UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1.0];
    UIColor *greenColor  = [UIColor colorWithRed:24.0/255.0 green:163.0/255.0 blue:19.0/255.0 alpha:1.0];
    
    [attrBtn appendAttributedString:[[NSAttributedString alloc] initWithString:@"Bấm " attributes:@{NSForegroundColorAttributeName:  defaulColor}]];
    [attrBtn appendAttributedString:[[NSAttributedString alloc] initWithString:@"vào đây " attributes: @{NSForegroundColorAttributeName: greenColor}]];
    [attrBtn appendAttributedString:[[NSAttributedString alloc] initWithString:@"để đặt câu hỏi." attributes: @{NSForegroundColorAttributeName: defaulColor}]];
    [self.questionHint setAttributedTitle: attrBtn forState: UIControlStateNormal];
    
    // Format Navbar
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Sidebar Indicator"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(sideMenuButtonDidTouch:)]; //Cho~ nay' no' goi 1 function de show left menu, o viewcontroller nay chua cai dat cai function nay;
    
    [menuButton setTintColor:[UIColor whiteColor]];
    self.navigationItem.leftBarButtonItem = menuButton;
    self.navigationController.navigationBar.barTintColor = NAVBAR_COLOR;
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UILabel * titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.textColor = [UIColor whiteColor];

            titleLabel.text = @"Câu hỏi của bạn";
            [[BMQuestionManager sharedInstance] loadMyQuestionListWithSuccessBlock:^{
                // Get question data
                myQuestions = [BMQuestionManager sharedInstance].myQuestionList;
                questionAvatars = [[BMQuestionManager sharedInstance] getAvatarList:myQuestions];
                // Check quesiton data
                if (myQuestions != nil && myQuestions.count>0) {
                    // Reload table view
                    [self.tableView reloadData];
                }
                else {

                    self.questionHint.hidden = NO;
                    self.tableView.hidden = YES;
                }
                    
            }];

    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    // Register notification to on off ask question
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isNeedEnableAskQuestion:) name:UBL_SETTING_ASK_QUESTION object:nil];
    [self isNeedEnableAskQuestion:nil];
}



-(void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
}

- (IBAction)sideMenuButtonDidTouch:(id)sender {
    [self presentLeftMenuViewController:nil];
    NSLog(@"Dang bi click ne ma'");
}

#pragma mark - Table delegate and data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return myQuestions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BMQuestionTableViewCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"BMQuestionTableViewCell" owner:nil options:nil] lastObject];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    Question * q = [myQuestions objectAtIndex:indexPath.row];
    cell.questionContent.text = q.question;
    cell.answerContent.text = q.answer;
    [cell.questionerAvatar sd_setImageWithURL:[NSURL URLWithString:[questionAvatars objectAtIndex:indexPath.row]] placeholderImage:cell.questionerAvatar.image];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Look up in height cache
    rowHeightCache = [NSMutableDictionary dictionary];
    NSNumber * h = rowHeightCache[@(indexPath.row)];
    if (h) return h.floatValue;
    
    // Temp var
    CGSize maximumLabelSize = CGSizeMake(self.view.width-58-20, CGFLOAT_MAX);
    CGRect frame;
    CGFloat padding = 80;
    NSString * body;
    
    // Height for original question
    Question *q = [myQuestions objectAtIndex:indexPath.row];
    body = q.question;
    
    // Calculate content height and cache height
    frame = [body boundingRectWithSize:maximumLabelSize
                               options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                            attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                               context:nil];
    CGFloat t = frame.size.height + padding;
    rowHeightCache[@(indexPath.row)] = @(t);
    return t;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Perform segue
    [self performSegueWithIdentifier:@"myQuestionToQuestionDetailSegue" sender:nil];
    // Deselect after select
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)tapHereToAskQuestionButtonDidTouch:(id)sender {
    [self performSegueWithIdentifier:@"myQuestionToAskQuestionSegue" sender:nil];
}

#pragma mark - Prepare segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"myQuestionToQuestionDetailSegue"]) {
        BMQuestionDetailViewController *controller = [segue destinationViewController];
        NSIndexPath * indexPath = [self.tableView indexPathForSelectedRow];
        Question *q = [myQuestions objectAtIndex:indexPath.row];
        controller.questionid = q.questionid;
    }
}

#pragma mark - isNeedEnableAskQuestion 
-(void)isNeedEnableAskQuestion:(NSNotification *)n{
    BOOL hiden  = ![BMQuestionManager sharedInstance].isEnableAskQuestion;
    self.questionHint.alpha = hiden?0:1;
}
@end
