//
//  BMMyQuestionViewController.h
//  BabyMe
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

typedef enum { MyQuestionTypeBookmark, MyQuestionTypeCreated } MyQuestionType;

@interface BMMyQuestionViewController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) MyQuestionType myQuestionType;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;
@property (strong, nonatomic) IBOutlet UIImageView *bookmarkHint;

@property (strong, nonatomic) IBOutlet UIButton *questionHint;

- (IBAction) tapHereToAskQuestionButtonDidTouch:(id)sender;

- (void) setAttributedTitle:(NSAttributedString *) title forState:(UIControlState) state;

@end
