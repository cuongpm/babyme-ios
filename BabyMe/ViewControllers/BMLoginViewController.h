//
//  BMLoginViewController.h
//  BabyMe
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface BMLoginViewController : GAITrackedViewController <UITextFieldDelegate>
{
    __weak IBOutlet UIImageView *divider;
    __weak IBOutlet UIButton *loginFacebook;
    __weak IBOutlet JVFloatLabeledTextField *emailField;
    __weak IBOutlet JVFloatLabeledTextField *passwordField;
    __weak IBOutlet UIButton *exitButton;
}
-(IBAction)moveToSignupScreen:(id)sender;


@end
