//
//  BMHomePageViewController.m
//  BabyMe
//

#import "BMHomePageViewController.h"
#import "UIViewController+RESideMenu.h"
#import "BMQuestionManager.h"
#import "BMQuestionDetailViewController.h"
#import "BMLoginViewController.h"
#import "UIView+MWParallax.h"
#import "UIImageView+WebCache.h"
#import "BMWebViewFooter.h"
#import "BMRelatedQuestionViewController.h"
#import <Parse/Parse.h>
#import <sys/utsname.h>
#define WEEKS_BEFORE_BIRTHDAY  40
#define MONTH_AFTER_BIRTHDAY   24

@implementation BMHomePageViewController

{
    __weak IBOutlet NSLayoutConstraint *headerViewBottomToTopGuideContraint;
    __weak IBOutlet NSLayoutConstraint *topViewTopToSuperContraint;
    __weak IBOutlet NSLayoutConstraint *menuButtonTopToGuideContraint;
    __weak IBOutlet NSLayoutConstraint *indicatorCenterXToTabCenterX;
    __weak IBOutlet UIImageView *bookmarkHint;
    __weak IBOutlet UIView *askQuestionWhenNoData;
    
    
    CGFloat webViewHeight;
    CGFloat newWebViewContentSizeHeight;
    
    int currentBabyWeek;
    int currentPageReminder;
    int currentPageQuestion;
    int currentMonth;
    BOOL currentBeforeBirth;
    int babyWeek;
    int totalBabyWeek;
    BOOL beforeBirth;
    
    int category_id;
    int tag_id;
    int tempCategory_id;
    
    NSInteger selectedIndex;
    BOOL extendedCellFlag;

    ButtonTouched tabButtonTouched;
    NSString *age_unit;
    NSString *age_number;
    BOOL contentViewMovedUp;
    
    int initialHeaderPosition;
    int initialSearchBarPosition;
    
    // Reminder table view
    NSMutableArray *reminderContent;
    NSMutableArray *reminderTitle;
    NSMutableArray *reminderCategory;
    NSInteger reminderCount;
    NSArray *reminders;
    
    // Question table view
    NSMutableArray *questions;
    NSArray *questionContents;
    NSArray *answerContents;
    NSArray *questionAvatars;
    NSInteger questionCount;
    NSArray *questions_;
    // Bookmark table view
    NSMutableArray *bookmarks;
    NSArray *bookmarkQuestionContents;
    NSArray *bookmarkAnswerContents;
    NSArray *bookmarkAvatars;
    NSInteger bookmarkCount;
    NSDictionary *babiesImage;
    UICollectionViewFlowLayout *flowLayout;
    // Webview footer
    BMWebViewFooter *webViewFooter;

}
- (void)awakeFromNib{
    self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}


//-(NSString *)platformRawString {
//    size_t size;
//    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
//    char *machine = malloc(size);
//    sysctlbyname("hw.machine", machine, &size, NULL, 0);
//    NSString *platform = [NSString stringWithUTF8String:machine];
//    free(machine);
//    return platform;
//}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    currentPageQuestion = 1;
    currentPageReminder = 1;
    self.screenName = @"HomePageScreen";

    questions = [NSMutableArray new];
    bookmarks = [NSMutableArray new];
    // Detect swipe gesture on Content View
    UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeUp:)];
    swipeUp.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:swipeUp];
    swipeUp.delegate = self;
    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeDown:)];
    swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
    [headerView addGestureRecognizer:swipeDown];
    swipeDown.delegate = self;
    
    // Remove separator of empty cell table view
    contentTableView.tableFooterView = [[UIView alloc] init];
    //
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    // Initial set up when first loading home page
    askQuestionWhenNoData.hidden = YES;
    bookmarkHint.hidden = YES;
    noDataLabel.hidden = YES;
    [self questionButtonDidTouch:questionButton];
    [self reserveIndicatorPosition];
    flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    if(screenWidth > 320){
        [flowLayout setItemSize:CGSizeMake(375, 417)];
    }else{
        [flowLayout setItemSize:CGSizeMake(320, 356)];
    }
    
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [_collectionView setCollectionViewLayout:flowLayout];
    _collectionView.bounces = YES;
    [_collectionView setShowsHorizontalScrollIndicator:NO];
    [_collectionView setShowsVerticalScrollIndicator:NO];
    // Webview delegate
    contentWebView.scrollView.delegate = self;
    contentWebView.delegate = self;
    
    // Related article button
    //    webViewFooter = [[BMWebViewFooter alloc] init];
    //    UIButton *relatedArticleButton = (UIButton *)[webViewFooter viewWithTag:1];
    //    [relatedArticleButton addTarget:self action:@selector(relatedArticleButtonDidTouch:) forControlEvents:UIControlEventTouchUpInside];
    
    // Set default selectedIndex value
    selectedIndex = -1;
    
    // BabyImage scroll view
    pageControl.numberOfPages = 4;
    thisWeekLeftButton.hidden = thisWeekRightButton.hidden = YES;

    // Display age of baby on header view
    [self ageDisplay];
    
    // Register notification to on off ask question
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(isNeedEnableAskQuestion:) name:UBL_SETTING_ASK_QUESTION object:nil];
    [self isNeedEnableAskQuestion:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlerNotification:) name:@"BM_DID_RECEIVE_NOTIFICATION" object:nil];//
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Paste category_id to tempCategory_id again (tempCategory_id may be equal to 0
    // when coming back from related question VC
    tempCategory_id = category_id;

    // Force to hide navigation bar
    self.navigationController.navigationBarHidden = YES;

}

- (void)handleSwipeUp:(UISwipeGestureRecognizer *)gesture {
    // Reserve indicator position
    [self reserveIndicatorPosition];

    // Move up main view
    headerViewBottomToTopGuideContraint.constant = 20;
    topViewTopToSuperContraint.constant = -305;
    [headerView setNeedsUpdateConstraints];
    [topView setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.view layoutIfNeeded];
                         babyImage.alpha = 0;
                     } completion:nil];

    // Move up search bar, side menu button
    menuButtonTopToGuideContraint.constant = -70;
    [UIView animateWithDuration:0.4
                          delay:0.1
                        options: UIViewAnimationOptionTransitionNone
                     animations:^{
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    contentViewMovedUp = contentTableView.scrollEnabled = contentWebView.scrollView.scrollEnabled = YES;
    

}

- (void)handleSwipeDown:(UISwipeGestureRecognizer *)gesture {
    NSLog(@"SWIDE DOWN");
    if (!contentViewMovedUp) return;
//    contentTableView.scrollsToTop = YES;
    [contentTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    [contentWebView.scrollView setContentOffset:CGPointZero animated:YES];
    contentViewMovedUp = contentTableView.scrollEnabled = contentWebView.scrollView.scrollEnabled = NO;
    [self reserveIndicatorPosition];

    // Move down main view
    headerViewBottomToTopGuideContraint.constant = 275;
    topViewTopToSuperContraint.constant = -20;
    [topView setNeedsUpdateConstraints];
    [headerView setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.6
                     animations:^{
                         [self.view layoutIfNeeded];
                         babyImage.alpha = 1;
                     } completion:nil];
    
    // Move down search bar, side menu button
    menuButtonTopToGuideContraint.constant = 0;
    [UIView animateWithDuration:0.4
                          delay:0.1
                        options: UIViewAnimationOptionTransitionNone
                     animations:^{
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Pragma mark collection view method

-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [cpBabyImages count];
}

-(UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"babyImg" forIndexPath: indexPath];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    if(screenWidth > 320)
        [imageView setFrame:CGRectMake(20, 15, imageView.frame.size.width, imageView.frame.size.height)];
    else
        [imageView setFrame:CGRectMake(-14, -10, imageView.frame.size.width, imageView.frame.size.height)];
    NSString *imageName = @"";


    if(indexPath.row > 40){
        imageName = [NSString stringWithFormat:@"BabyImg_AfterBirth_%zd", (indexPath.row)];
    }else{
        imageName = [NSString stringWithFormat:@"BabyImg_Pregnancy_%zd", (indexPath.row)];
    }
    UIImage *image = (UIImage *)[cpBabyImages objectForKey:imageName];
    [imageView setImage: image];

    
//    if ([self.platformRawString isEqualToString:@"iPhone3,1"] || [self.platformRawString isEqualToString:@"iPhone3,3"] || [self.platformRawString isEqualToString:@"iPhone4,1"]){
//        NSLog(@"DONT SUPPORT THIS MODEL");
//    }else{
//        imageView.iOS6ParallaxIntensity = 20;
//        homePageBackground.iOS6ParallaxIntensity = 6;
//    }
    
    return cell;
}

// Detect scroll offset to swipe down entire view
- (void) scrollViewDidScroll: (UIScrollView *) scrollView willDecelerate:(BOOL) decelerate {
    
        CGFloat scrollViewOffset = contentWebView.scrollView.contentOffset.y;
        CGFloat tableViewOffset = contentTableView.contentOffset.y;
        if ((scrollViewOffset < -50) || (tableViewOffset <-50)) {
            [self handleSwipeDown:nil];
        }
    
        // Check web view content size
        if (contentWebView.scrollView.contentSize.height < newWebViewContentSizeHeight) {
            CGSize f = contentWebView.scrollView.contentSize;
            f.height = newWebViewContentSizeHeight;
            contentWebView.scrollView.contentSize = f;
        }
        else return;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {

    if(scrollView == _collectionView ){
        [self scrollViewDidStop:scrollView];
        reminders = nil;
        currentPageReminder = 1;
        [self getMoreData:scrollView];
        [contentTableView reloadData];
    }
    if(scrollView == contentTableView){
        currentPageQuestion = 1;
        [self getMoreData:scrollView];
        [contentTableView reloadData];
        NSLog(@"%f", scrollView.contentOffset.y);
    }
    if(scrollView.contentOffset.y < 1.0){
        [self handleSwipeDown:nil];
    }


    
}

-(void)getMoreData: (UIScrollView *) scrollView{
//    switch (tabButtonTouched) {
//        case reminder:{
//            currentPageReminder++;
//            [self loadReminderTableDataFromWeek];
//        }
//            break;
//        
//    }
}

- (void)scrollViewDidStop:(UIScrollView *)scrollView  {

//    [imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"BabyImg_AfterBirth_%zd", i]];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    if (scrollView != nil){
        babyWeek = scrollView.contentOffset.x / screenWidth;

    }
    else{
        if (!currentBeforeBirth) {
            babyWeek = currentBabyWeek + 40;
        }else
            babyWeek = currentBabyWeek;
    }

    int tempBabyMonth;
    
    if (babyWeek > WEEKS_BEFORE_BIRTHDAY) {
        beforeBirth = NO;
        if(babyWeek != currentBabyWeek)
            babyWeek = babyWeek - WEEKS_BEFORE_BIRTHDAY;

        if (((double)babyWeek)/4 == (int)(babyWeek/4))
            tempBabyMonth = (int)(babyWeek/4);
        else
            tempBabyMonth = (int)(babyWeek/4) + 1;


        
        // Update UI
        [weekLabel setText:@"THÁNG"];
        
        [weekButton setTitle:[NSString stringWithFormat:@"%d",tempBabyMonth] forState:UIControlStateNormal];
        [self updatePageControl];
    }
    else {
        beforeBirth = YES;
        // Update UI
        [weekLabel setText:@"TUẦN"];
        NSLog(@"MOA%zd", babyWeek);
        [weekButton setTitle:[NSString stringWithFormat:@"%d", babyWeek] forState:UIControlStateNormal];
        [self updatePageControl];
    }
//    if (babyWeek == currentBabyWeek && beforeBirth == currentBeforeBirth) {
//        [weekLabel setText:(currentBeforeBirth ? @"TUẦN" : @"THÁNG")];
//        
////        [weekButton setTitle:[NSString stringWithFormat:@"%zd", babyWeek] forState:UIControlStateNormal];
////        [self updatePageControl];
//    }
    
    if (beforeBirth == currentBeforeBirth) {
        if (babyWeek > currentBabyWeek)
            thisWeekRightButton.hidden = !(thisWeekLeftButton.hidden = NO);
        else if (babyWeek < currentBabyWeek)
            thisWeekRightButton.hidden = !(thisWeekLeftButton.hidden = YES);
        else
            thisWeekRightButton.hidden = thisWeekLeftButton.hidden = YES;
    }
    else if (beforeBirth == NO && currentBeforeBirth == YES) {
        thisWeekRightButton.hidden = !(thisWeekLeftButton.hidden = NO);
    }
    else {
        thisWeekRightButton.hidden = !(thisWeekLeftButton.hidden = YES);
    }
    
    switch (tabButtonTouched) {
        case week:
            [self weekButtonDidTouch:weekButton];
            break;
        case reminder:
            [self reminderButtonDidTouch:reminderButton];
            break;
        case question:
            [self questionButtonDidTouch:questionButton];
            break;
            
        default:
            break;
    }
}



// Side Menu Button
- (IBAction)sideMenuButtonDidTouch:(id)sender {
    [self presentLeftMenuViewController:nil];
}

#pragma mark - Table View Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (tabButtonTouched) {
        case week:
            return 0;
            break;
        case reminder:
            return reminderCount;
            break;
        case question:
            currentPageQuestion = 1;
            return questionCount + 1;
            break;
        case bookmark:
            return bookmarkCount;
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    switch (tabButtonTouched)
    {
        case week:
            cell = [[UITableViewCell alloc] init];
            break;
        case reminder:
            cell = [tableView dequeueReusableCellWithIdentifier:@"ReminderCell"];
            cell = [[[NSBundle mainBundle] loadNibNamed:@"BMReminderTableViewCell" owner:nil options:nil] lastObject];
            if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
                ((BMReminderTableViewCell *)cell).contentToTitleContraint.constant = -2;
            }
            ((BMReminderTableViewCell *)cell).reminderContent.text = [reminderContent objectAtIndex:indexPath.row];
            ((BMReminderTableViewCell *)cell).reminderTitle.text = [reminderTitle objectAtIndex:indexPath.row];
            ((BMReminderTableViewCell *)cell).reminderAvatar.image = (UIImage *)[reminderIconDict objectForKey:[reminderCategory objectAtIndex:indexPath.row]];
            break;
        case question:
            cell = [tableView dequeueReusableCellWithIdentifier:@"QuestionCell"];
            cell = [[[NSBundle mainBundle] loadNibNamed:@"BMQuestionTableViewCell" owner:nil options:nil] lastObject];
            if(indexPath.row == questionCount){
                ((BMQuestionTableViewCell *)cell).questionContent.hidden = YES;
                ((BMQuestionTableViewCell *)cell).answerContent.hidden = YES;
                ((BMQuestionTableViewCell *)cell).questionerAvatar.hidden = YES;
                ((BMQuestionTableViewCell *)cell).askQuestion.hidden = ![BMQuestionManager sharedInstance].isEnableAskQuestion;
                
            }else{
                ((BMQuestionTableViewCell *)cell).questionContent.text = [questionContents objectAtIndex:indexPath.row];
            
                ((BMQuestionTableViewCell *)cell).answerContent.text = [[answerContents objectAtIndex:indexPath.row] isEqualToString:@""] ? @"Hiện tại chưa có câu trả lời" : [answerContents objectAtIndex:indexPath.row];
            
                [((BMQuestionTableViewCell *)cell).questionerAvatar sd_setImageWithURL:[NSURL URLWithString:[questionAvatars objectAtIndex:indexPath.row]] placeholderImage:((BMQuestionTableViewCell *)cell).questionerAvatar.image];
            }
            break;
        case bookmark:
            cell = [tableView dequeueReusableCellWithIdentifier:@"QuestionCell"];
            cell = [[[NSBundle mainBundle] loadNibNamed:@"BMQuestionTableViewCell" owner:nil options:nil] lastObject];
            
            ((BMQuestionTableViewCell *)cell).questionContent.text = [bookmarkQuestionContents objectAtIndex:indexPath.row];
            ((BMQuestionTableViewCell *)cell).answerContent.text = [[bookmarkAnswerContents objectAtIndex:indexPath.row] isEqualToString:@""] ? @"Hiện tại chưa có câu trả lời" : [bookmarkAnswerContents objectAtIndex:indexPath.row];
            [((BMQuestionTableViewCell *)cell).questionerAvatar sd_setImageWithURL:[NSURL URLWithString:[bookmarkAvatars objectAtIndex:indexPath.row]] placeholderImage:((BMQuestionTableViewCell *)cell).questionerAvatar.image];
            break;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat padding;
    CGRect frame;
    CGSize maximumLabelSize = CGSizeMake(self.view.width-56-20, CGFLOAT_MAX);
    NSString * body;
    
    switch (tabButtonTouched) {
        case week:
            padding = 60;
            break;
        case reminder:
        {
            if (indexPath.row == selectedIndex) {
                extendedCellFlag = YES;
                padding = 40;
                NSString *body1 = [reminderTitle objectAtIndex:indexPath.row];
                NSString *body2 = [reminderContent objectAtIndex:indexPath.row];
                body = [NSString stringWithFormat:@"%@\n%@", body1, body2];
                NSLog(@"TESTing %@", body);
            }
            else {
                return 80;
            }
        }
            break;
        case question:
        {
            padding = 65;
            if(indexPath.row < questionCount - 1){
                body = [questionContents objectAtIndex:indexPath.row];
            }else
            {
                body = @"";
            }
        }
            break;
        case bookmark:
        {
            padding = 65;
            body = [bookmarkQuestionContents objectAtIndex:indexPath.row];

        }
            break;
    }
    // Calculate content height and cache height
    frame = [body boundingRectWithSize:maximumLabelSize
                               options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                            attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Roboto-Light" size:16]}
                               context:nil];
    CGFloat t = frame.size.height + padding;
    return t;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Reserve indicator position (for iOS7)
    [self reserveIndicatorPosition];

    // Perform segue
    switch (tabButtonTouched) {
        case week:
            break;
        case reminder:
        {
            // Check condition if cell was extended or not
            selectedIndex = (extendedCellFlag && selectedIndex == indexPath.row) ? -1 : indexPath.row;
            // Reload row at index path
            [contentTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
            break;
        case question:
        {
            if(indexPath.row < questionCount)
                [self performSegueWithIdentifier:@"questionDetailSegue" sender:nil];
//            else
//                [self performSegueWithIdentifier:@"HomePageToAskQuestionSegue" sender:nil];
        }
            break;
        case bookmark:
            [self performSegueWithIdentifier:@"questionDetailSegue" sender:nil];
            break;
    }
    // Deselect after select
    [contentTableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (tabButtonTouched) {
        case week:
            break;
        case reminder:
            [contentTableView.delegate tableView:tableView didSelectRowAtIndexPath:indexPath];
            return NO;
            break;
        case question:
            break;
        case bookmark:
            break;
    }
    return YES;
}

#pragma mark - Implement Button Did Touch
- (IBAction)weekButtonDidTouch:(id)sender {
    tabButtonTouched = week;
    [self tabButtonDidTouch:sender];
    if (babyWeek >= 0 && babyWeek != currentBabyWeek)
        [self webViewDisplayFromWeek];
    else
        [self webViewDisplayFromBabyID];
}
- (IBAction)reminderButtonDidTouch:(id)sender {
    tabButtonTouched = reminder;
    [self tabButtonDidTouch:sender];
    if (babyWeek >= 0 && babyWeek != currentBabyWeek)
        [self loadReminderTableDataFromWeek];
    else{
        if(currentBeforeBirth != beforeBirth)
            [self loadReminderTableDataFromWeek];            
        else
            [self loadReminderTableDataFromBabyID];
    }
}
- (IBAction)questionButtonDidTouch:(id)sender {
    tabButtonTouched = question;
    [self tabButtonDidTouch:sender];
    if (babyWeek >= 0 && babyWeek != currentBabyWeek)
        [self loadQuestionTableDataFromWeek];
    else
        [self loadQuestionTableDataFromBabyID];
}

- (IBAction)followButtonDidTouch:(id)sender {
    if (LOGGED_IN) {
        tabButtonTouched = bookmark;
        [self tabButtonDidTouch:sender];
        [self loadBookmarkTableData];
    }
    else {
        [SIAlertView showAlertWithTitle:nil
                                message:@"Bạn cần đăng nhập để sử dụng tính năng này"
                      cancelButtonTitle:@"Đóng lại"
                          okButtonTitle:@"ĐĂNG NHẬP"
                                handler:^{
                                    // Go to login screen
                                    BMLoginViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"loginViewController"];
                                    [self presentViewController:controller animated:YES completion:nil];
                                }];
    }

}

-(void)tabButtonDidTouch:(UIButton*)sender {
    [UIView animateWithDuration:0.2
                     animations:^{
                         indicator.centerX = sender.superview.centerX;
                     } completion:^(BOOL finished) {
                     }];
    contentWebView.hidden = !(contentTableView.hidden = (sender == weekButton));
    contentTableView.scrollEnabled = contentWebView.scrollView.scrollEnabled = contentViewMovedUp;
    noDataLabel.hidden = YES;
}

-(void)relatedArticleButtonDidTouch: (id)sender {
    [self performSegueWithIdentifier:@"homePageToRelatedQuestionSegue" sender:nil];
}
- (IBAction)tapHereToAskQuestionButtonDidTouch:(id)sender {
//    [self performSegueWithIdentifier:@"HomePageToAskQuestionSegue" sender:nil];
}



-(void) scrollToCurrentWeek{
    reminders = nil;
    currentPageReminder = 1;
    totalBabyWeek = totalBabyWeek > 136 ? 136 : totalBabyWeek;
    [_collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:totalBabyWeek inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    [self scrollViewDidStop:nil];
}

- (IBAction)thisWeekLeftButtonDidTouch:(id)sender {
    switch (tabButtonTouched) {
        case reminder:{
            currentPageReminder = 1;
            reminders = nil;
            [self loadReminderTableDataFromWeek];
        }
            break;
    }

    [self scrollToCurrentWeek];
}


- (IBAction)thisWeekRightButtonDidTouch:(id)sender {

    [self scrollToCurrentWeek];
}


#pragma mark - Webview delegate
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    // Re-format the html
    [contentWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.body.style.padding='0px 0px 0px 0px';document.body.style.margin='0px 0px 0px 0px';var x = document.getElementsByTagName('p'); var x = document.getElementsByTagName('span');for (var i = 0; i < x.length; i++)"]];
    
    webViewHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight;"] intValue];
    
    // Add web view footer
    webViewFooter.frame = CGRectMake(0, webViewHeight + 10, self.view.width, webViewFooter.height);
    [webView.scrollView addSubview:webViewFooter];
    
    // New content size height value
    newWebViewContentSizeHeight = webViewHeight + webViewFooter.height + 20;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([request.URL.scheme isEqualToString:@"inapp"] && (request.URL.host)) {
        // Set category_id = 0 to make sure related article API called in Related Question VC
        tempCategory_id = 0;
        tag_id = request.URL.host.intValue;
        [self performSegueWithIdentifier:@"homePageToRelatedQuestionSegue" sender:nil];
    }
    return YES;
}

#pragma mark - Webview display
-(void)webViewDisplayFromWeek {
    [actInd startAnimating];
    [ApplicationDelegate.networkEngine getArticleOfWeek:babyWeek
                                            beforeBirth:beforeBirth
                                        completionBlock:^(id responseObject) {
                                            [actInd stopAnimating];
                                            [self populateWebViewDataFrom:responseObject];
                                        } errorBlock:^(NSError *error) {
                                        }];
}

-(void)webViewDisplayFromBabyID {
    // Load web view from local storage
    NSDictionary *responseObjectFromStorage = [[NSUserDefaults standardUserDefaults] secretObjectForKey:kWebViewContent];
    if (responseObjectFromStorage) [self populateWebViewDataFrom:responseObjectFromStorage];
    // Then, load web view from server
    [ApplicationDelegate.networkEngine getArticleOf:BABY_ID
                                    completionBlock:^(id responseObject) {
                                        if (responseObject == responseObjectFromStorage) return;
                                        if (babyWeek == currentBabyWeek && beforeBirth == currentBeforeBirth) {
                                            [self populateWebViewDataFrom:responseObject];
                                        }
                                    } errorBlock:^(NSError *error) {
                                    }];
}

-(void)populateWebViewDataFrom:(NSDictionary *)dict {
    NSDictionary *data = [dict[@"data"] lastObject];
    NSString *htmlString = data[@"content"];
    if (htmlString == nil) {
        noDataLabel.hidden = NO;
        bookmarkHint.hidden = YES;
        contentWebView.hidden = YES;
        askQuestionWhenNoData.hidden = YES;
    } else {
        [contentWebView setBackgroundColor:[UIColor clearColor]];
        [contentWebView loadHTMLString:htmlString baseURL:nil];
        noDataLabel.hidden = YES;
        bookmarkHint.hidden = YES;
        contentWebView.hidden = NO;
        askQuestionWhenNoData.hidden = YES;
    }
}

#pragma mark - Reminder data
-(void)loadReminderTableDataFromWeek {
    [actInd startAnimating];
    [ApplicationDelegate.networkEngine getReminderOfWeek:babyWeek
                                            beforeBirth:beforeBirth
                                             currentPage:currentPageReminder
                                        completionBlock:^(id responseObject) {
                                            [actInd stopAnimating];
                                            [self populateReminderTableDataFrom:responseObject];
                                        } errorBlock:^(NSError *error) {
                                        }];
}

- (void)loadReminderTableDataFromBabyID {
    // Load web view from local storage
    NSDictionary *responseObjectFromStorage = [[NSUserDefaults standardUserDefaults] secretObjectForKey:kReminderData];
    if (responseObjectFromStorage) [self populateReminderTableDataFrom:responseObjectFromStorage];
    // Then, load reminder data from server
    [ApplicationDelegate.networkEngine getReminderOf:BABY_ID
                                     completionBlock:^(id responseObject) {
                                         if (responseObject == responseObjectFromStorage) return;
                                         if (babyWeek == currentBabyWeek && beforeBirth == currentBeforeBirth) [self populateReminderTableDataFrom:responseObject];
                                     } errorBlock:^(NSError *error) {
                                     }];
}

- (void)populateReminderTableDataFrom:(NSDictionary *)dict {
    
     reminders = dict[@"data"];
    
//    NSArray *data = dict[@"data"];
    if (reminders != nil && reminders.count > 0) {
        reminderContent = [NSMutableArray new];
        reminderTitle = [NSMutableArray new];
        reminderCategory = [NSMutableArray new];
        for (NSDictionary *dict in reminders) {
            NSString *content = [dict objectForKey:@"content"];
            NSString *title   = [dict objectForKey:@"title"];
            NSString *category = [dict objectForKey:@"category"];
            [reminderContent addObject:content];
            [reminderTitle addObject:title];
            [reminderCategory addObject:category];
        }
        reminderCount = reminders.count;
        noDataLabel.hidden = YES;
        bookmarkHint.hidden = YES;
        askQuestionWhenNoData.hidden = YES;
        contentTableView.hidden = NO;
        [contentTableView reloadData];
    }
    else{
        noDataLabel.hidden = NO;
        bookmarkHint.hidden = YES;
        askQuestionWhenNoData.hidden = YES;
        contentTableView.hidden = YES;
    }
}

#pragma mark - Question data
-(void)loadQuestionTableDataFromWeek {
    [actInd startAnimating];
    bookmarkHint.hidden = YES;
    noDataLabel.hidden = YES;
    contentTableView.hidden = YES;
    askQuestionWhenNoData.hidden = YES;
    [[BMQuestionManager sharedInstance] loadQuestionListFromWeek:babyWeek
                                                     beforeBirth:beforeBirth
                                                     currentPage:currentPageQuestion
                                                withSuccessBlock:^{
        [actInd stopAnimating];
        // Get question data

        questions = [[BMQuestionManager sharedInstance].questionList mutableCopy];
        
        [self populateQuestionDataFrom:questions];
    }];
}

- (void)loadQuestionTableDataFromBabyID {
    // Load question from local storage
    NSArray *questionsFromStorage = [[NSUserDefaults standardUserDefaults] secretObjectForKey:kQuestionData];
    questions = [questionsFromStorage mutableCopy];
    if (questionsFromStorage) [self populateQuestionDataFrom:questionsFromStorage];
    
    // Load question from server
    [[BMQuestionManager sharedInstance] loadQuestionListWithSuccessBlock:^{
            // Get question data
        [questions removeAllObjects];
        questions = [[BMQuestionManager sharedInstance].questionList mutableCopy];
        if (questions == questionsFromStorage) return;
        if (babyWeek == currentBabyWeek && beforeBirth == currentBeforeBirth) [self populateQuestionDataFrom:questions];
        }];
    
    if (!BABY_ID) {
        return;
    }
    
    [ApplicationDelegate.networkEngine getNotificationOf:BABY_ID completionBlock:^(id responseObject) {
        NSLog(@"responseObject %@",responseObject);
    } errorBlock:^(NSError *error) {
        
    }];
}

-(void)populateQuestionDataFrom:(NSArray *)aArray {
    // Check quesiton data
    if (aArray != nil && aArray.count > 0) {
        // Count question object
        questionCount = aArray.count;
        
        // Get question/answer content data
        questionContents = [[BMQuestionManager sharedInstance] getQuestionList:aArray];
        answerContents = [[BMQuestionManager sharedInstance] getAnswerList:aArray];
        questionAvatars = [[BMQuestionManager sharedInstance] getAvatarList:aArray];
        
        // Hide "no data" label
        bookmarkHint.hidden = YES;
        noDataLabel.hidden = YES;
        askQuestionWhenNoData.hidden = YES;
        contentTableView.hidden = NO;
        [contentTableView reloadData];
    }
    else {
        noDataLabel.hidden = YES;
        askQuestionWhenNoData.hidden = NO;
        bookmarkHint.hidden = YES;
        contentTableView.hidden = YES;
    }
}

#pragma mark - Helpers
- (void)loadBookmarkTableData {
    // Load question from local storage
    NSArray *bookmarksFromStorage = [[NSUserDefaults standardUserDefaults] secretObjectForKey:kBookmarkData];
    bookmarks = [bookmarksFromStorage mutableCopy];
    if (bookmarksFromStorage) [self populateBookmarkDataFrom:bookmarksFromStorage];
    // Load question from server
    [[BMQuestionManager sharedInstance] loadWatchListWithSuccessBlock:^{
        // Get bookmark data
        [bookmarks removeAllObjects];
        bookmarks = [[BMQuestionManager sharedInstance].watchList mutableCopy];
        if ([bookmarks isEqualToArray: bookmarksFromStorage]) return;
        [self populateBookmarkDataFrom:bookmarks];
    }];
}

-(void)populateBookmarkDataFrom:(NSArray *)aArray {
    // Check bookmark data
    if (aArray != nil && aArray.count > 0) {
        // Count bookmark object
        bookmarkCount = aArray.count;
        
        // Get question/answer content data
        bookmarkQuestionContents = [[BMQuestionManager sharedInstance] getQuestionList:aArray];
        bookmarkAnswerContents = [[BMQuestionManager sharedInstance] getAnswerList:aArray];
        bookmarkAvatars = [[BMQuestionManager sharedInstance] getAvatarList:aArray];
        
        // Hide "no data" label
        noDataLabel.hidden = YES;
        bookmarkHint.hidden = YES;
        askQuestionWhenNoData.hidden = YES;
        contentTableView.hidden = NO;
        [contentTableView reloadData];
    }
    else {
        noDataLabel.hidden = NO;
        bookmarkHint.hidden = NO;
        askQuestionWhenNoData.hidden = YES;
        contentTableView.hidden = YES;
    }

}

- (IBAction)searchBarMask:(id)sender {
    [self performSegueWithIdentifier:@"searchInHomePageSegue" sender:nil];
}

- (void)ageDisplay {
//    // Load age from local storage
//    NSDictionary *responseObjectFromStorage = [[NSUserDefaults standardUserDefaults] secretObjectForKey:kBabyAge];
//    if (responseObjectFromStorage) {
//        age_unit   = [responseObjectFromStorage[@"unit"] isEqualToString:@"week"] ? @"TUẦN" : @"THÁNG";
//        age_number = [NSString stringWithFormat:@"%@", responseObjectFromStorage[@"number"]];
//        babyWeek = [responseObjectFromStorage[@"raw_week"] intValue];
//        currentBabyWeek = [responseObjectFromStorage[@"raw_week"] intValue];
//        currentMonth = [responseObjectFromStorage[@"month"] intValue];
//        currentBeforeBirth = [responseObjectFromStorage[@"before_birth"] boolValue];
//        if(currentBabyWeek > 40 || (!currentBeforeBirth && currentMonth == 0))
//            babyWeek = currentBabyWeek + 40;
//        totalBabyWeek = babyWeek;
//        if( currentBeforeBirth && babyWeek > 40){
//            babyWeek = 0;
//        }
//        // Update UI
//        [weekLabel setText:age_unit];
//        [weekButton setTitle:age_number forState:UIControlStateNormal];
//        [self updatePageControl];
//    }else{
        // Load age from server

        [ApplicationDelegate.networkEngine getBabyAge:BABY_ID
                                      completionBlock:^(id responseObject) {
                                          age_unit   = [responseObject[@"unit"] isEqualToString:@"week"] ? @"TUẦN" : @"THÁNG";
                                          
                                          age_number = [NSString stringWithFormat:@"%@", responseObject[@"number"]];

                                          babyWeek = [responseObject[@"raw_week"] intValue];
                                          currentBabyWeek = [responseObject[@"raw_week"] intValue];
                                          currentBeforeBirth = [responseObject[@"before_birth"] boolValue];
                                          
                                          if(currentBabyWeek > 40 || (!currentBeforeBirth && currentMonth == 0))
                                              babyWeek = currentBabyWeek + 40;
                                          if( currentBeforeBirth && currentMonth == 0 && babyWeek > 40){
                                              babyWeek = 0;
                                          }
                                          totalBabyWeek = babyWeek;
                                          
                                          // Update UI
                                          [weekLabel setText:age_unit];
                                          [weekButton setTitle:age_number forState:UIControlStateNormal];
                                          if([responseObject[@"month"] intValue] >= 24){
                                              [SIAlertView showAlertWithTitle:nil message:@"Hiện tại ứng dụng chỉ hỗ trợ giai đoạn mang thai và bé dưới 2 tuổi." cancelButtonTitle:kDismissText];
                                          }

                                          [self scrollToCurrentWeek];
                                          [self updatePageControl];
                                          
                                      }    errorBlock:^(NSError *error) {
                                      }];
//    }


    [self updateUserPushManager];
}
-(void) updateUserPushManager{
    PFInstallation *installation = [PFInstallation currentInstallation];
    NSString *baby = [NSString stringWithFormat:@"baby_%@", BABY_ID];
    NSMutableArray * channels = [NSMutableArray arrayWithObjects:@"Everyone", baby, nil];
    [installation setChannels: channels];
    [installation saveEventually];

}
-(void)reserveIndicatorPosition {
    switch (tabButtonTouched) {
        case week:
            indicatorCenterXToTabCenterX.constant = 0;
            break;
        case reminder:
            indicatorCenterXToTabCenterX.constant = (self.view.width/4) * 1;
            break;
        case question:
            indicatorCenterXToTabCenterX.constant = (self.view.width/4) * 2;
            break;
        case bookmark:
            indicatorCenterXToTabCenterX.constant = (self.view.width/4) * 3;
            break;
    }
}

- (void)jumpBabyImageToTime:(int)time {
//    [babyImageScrollView scrollRectToVisible:CGRectMake((self.view.frame.origin.x + self.view.frame.size.width) * time, babyImageScrollView.frame.origin.y,self.view.frame.size.width,babyImageScrollView.frame.size.height) animated:YES];
}

- (void)updatePageControl {
    int currentPage = ((int)((float)((float)babyWeek/4 - (float)floor(babyWeek/4))*4) - 1);
    if(babyWeek >= 96){
        pageControl.currentPage = 3;
    }else{
        pageControl.currentPage = (currentPage != -1)? currentPage: 3;
    }

}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Pass question object to the next VC
    if ([segue.identifier isEqualToString:@"questionDetailSegue"]) {
        BMQuestionDetailViewController *controller = [segue destinationViewController];
        NSIndexPath * indexPath = [contentTableView indexPathForSelectedRow];
        switch (tabButtonTouched) {
            case week:
                break;
            case reminder:
                break;
            case question:
            {
                Question *tempQuestion = [questions objectAtIndex:indexPath.row];
                controller.questionid = tempQuestion.questionid;
            }
                break;
            case bookmark:
            {
                Question *tempQuestion = [bookmarks objectAtIndex:indexPath.row];
                controller.questionid = tempQuestion.questionid;
            }
                break;
        }
    }
    if ([segue.identifier isEqualToString:@"homePageToRelatedQuestionSegue"]) {
        BMRelatedQuestionViewController *controller = [segue destinationViewController];
        controller.categoryid = tempCategory_id;
        controller.tagid = tag_id;
        controller.relatedButtonTouched = relatedArticle;
    }
}

#pragma mark - isNeedEnableAskQuestion
-(void)isNeedEnableAskQuestion:(NSNotification *)n{
    //askQuestionWhenNoData
    BMQuestionManager * questionManager = [BMQuestionManager sharedInstance];
    askQuestionWhenNoData.alpha = questionManager.isEnableAskQuestion?1:0;
    askQuestionWhenNoData.backgroundColor = questionManager.isEnableAskQuestion?[UIColor greenColor]:[UIColor redColor];
    if (n) {
        [contentTableView reloadData];
    }
}

#pragma mark - Handler 
-(void)handlerNotification:(NSNotification *)n{
    // aps id type
    NSDictionary * userInfo = n.object;
    if ([userInfo isKindOfClass:[NSDictionary class]]) {
        NSDictionary * aps = [userInfo objectForKey:@"aps"];
        // Type
        NSString * type = [aps objectForKey:@"type"];
        if ([type isKindOfClass:[NSString class]]) {
            if ([type isEqualToString:@"reminder"]) {
                // Mo qua reminder
                [self reminderButtonDidTouch:reminderButton];
            }
            else if ([type isEqualToString:@"question"]){
                // Get id
                NSNumber * __id = [aps objectForKey:@"id"];
                if ([__id isKindOfClass:[NSNumber class]]) {
                    BMRelatedQuestionViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"questionDetailViewController"];
                   
                    controller.questionid = __id.doubleValue;
                    [self.navigationController pushViewController:controller animated:YES];
                }
            }
        }
    }
}

@end
