//
//  BMQuestionDetailViewController.h
//  BabyMe
//

#import "BMButton.h"
#import "BMQuestionManager.h"
#import "GAITrackedViewController.h"
typedef enum {
    relatedQuestion,
    relatedArticle
} RelatedButtonTouched;

@interface BMQuestionDetailViewController : GAITrackedViewController <UIScrollViewDelegate, UITextFieldDelegate,UIWebViewDelegate>

// Outlets
@property (weak, nonatomic) IBOutlet UITableView * tableView;
@property (weak, nonatomic) IBOutlet UIView      * headerView;
@property (weak, nonatomic) IBOutlet UIButton    * bookmarkButton;
@property (weak, nonatomic) IBOutlet UIView      * commentView;
@property (weak, nonatomic) IBOutlet UITextField * commentTextField;
@property (weak, nonatomic) IBOutlet UIButton    * commentSendButton;
@property (weak, nonatomic) IBOutlet UIView      * footerView;
@property (weak, nonatomic) IBOutlet UIButton * likeButton;
@property (weak, nonatomic) IBOutlet UIButton * commentButton;
@property (weak, nonatomic) IBOutlet UIButton * shareButton;

@property (weak, nonatomic) IBOutlet UIImageView * questionerAvatar;
@property (weak, nonatomic) IBOutlet UILabel     * questionerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel     * questionBodyLabel;
@property (weak, nonatomic) IBOutlet UILabel     * answererNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView * answererAvatar;
@property (weak, nonatomic) IBOutlet UIWebView   * answerBodyWebView;
@property (weak, nonatomic) IBOutlet UILabel *voteCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *articleRelateLink;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *anwerWebViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *articleLine;
@property (weak, nonatomic) IBOutlet UIView *answerView;

// Data from segue
@property (nonatomic) int      questionid;
@property (nonatomic, strong)  Question * question;

- (IBAction)commentSendButtonDidTouch:(id)sender;
- (IBAction)commentButtonDidTouch:(id)sender;
- (IBAction)likeButtonDidTouch:(id)sender;
- (IBAction)shareButtonDidTouch:(id)sender;

@end
