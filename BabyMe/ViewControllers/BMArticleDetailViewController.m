//
//  BMArticleDetailViewController.m
//  BabyMe
//

#import "BMArticleDetailViewController.h"
#import "BMWebViewFooter.h"
#import "BMRelatedQuestionViewController.h"

@implementation BMArticleDetailViewController
{
    int category_id;
    int tag_id;
    CGFloat   webViewHeight;
    CGFloat   newWebViewContentSizeHeight;

    // Webview footer
    BMWebViewFooter *webViewFooter;
    
    // Activity indicator
    __weak IBOutlet UIActivityIndicatorView *actInd;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"ArticleDetailScreen";
    
    [self webViewDisplay];

    // Change color of navigation background
    self.navigationController.navigationBarHidden = NO;
    self.view.backgroundColor = NAVBAR_COLOR;

    // Webview delegate
    contentWebView.scrollView.delegate = self;
    contentWebView.delegate = self;
    
    
    // Webview footer
    webViewFooter = [[BMWebViewFooter alloc] init];
    UIButton *relatedArticleButton = (UIButton *)[webViewFooter viewWithTag:1];
    [relatedArticleButton addTarget:self action:@selector(relatedArticleButtonDidTouch:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)webViewDisplay {
    [actInd startAnimating];
    [ApplicationDelegate.networkEngine getArticleDetailOf:_articleid
                                    completionBlock:^(id responseObject) {
                                        [actInd stopAnimating];
                                        NSString *htmlString = responseObject[@"content"];
                                        category_id = [responseObject[@"category_id"] intValue];
                                        [contentWebView setBackgroundColor:[UIColor clearColor]];
                                        [contentWebView loadHTMLString:htmlString baseURL:nil];
                                        contentWebView.hidden = NO;
                                    } errorBlock:^(NSError *error) {
                                        [actInd stopAnimating];
                                        [self dismissViewControllerAnimated:YES completion:nil];
                                    }];
}

#pragma mark - Scrollview delegate
- (void) scrollViewDidScroll: (UIScrollView *) scrollView {
    // Check web view content size
    if (contentWebView.scrollView.contentSize.height < newWebViewContentSizeHeight) {
        CGSize f = contentWebView.scrollView.contentSize;
        f.height = newWebViewContentSizeHeight;
        contentWebView.scrollView.contentSize = f;
    }

    else return;
}

#pragma mark - Webview delegate
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    // Re-format the html
    [contentWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.body.style.padding='0px 0px 0px 0px';document.body.style.margin='0px 0px 0px 0px';var x = document.getElementsByTagName('p'); var x = document.getElementsByTagName('span');for (var i = 0; i < x.length; i++)"]];
    
    webViewHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight;"] intValue];
    
    // Add web view footer
    webViewFooter.frame = CGRectMake(0, webViewHeight + 10, self.view.width, webViewFooter.height);
    [webView.scrollView addSubview:webViewFooter];
    
    // New content size height value
    newWebViewContentSizeHeight = webViewHeight + webViewFooter.height + 20;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([request.URL.scheme isEqualToString:@"inapp"] && (request.URL.host)) {
        // Set category_id = 0 to make sure related article API called in Related Question VC
        category_id = 0;
        tag_id = request.URL.host.intValue;
        [self performSegueWithIdentifier:@"articleDetailToRelatedQuestionSegue" sender:nil];
    }
    return YES;
}

-(void)relatedArticleButtonDidTouch:(id)sender {
    [self performSegueWithIdentifier:@"articleDetailToRelatedQuestionSegue" sender:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"articleDetailToRelatedQuestionSegue"]) {
        BMRelatedQuestionViewController *controller = [segue destinationViewController];
        controller.categoryid = category_id;
        controller.tagid = tag_id;
        controller.relatedButtonTouched = relatedArticle;
    }
}

@end
