//
//  BMSubmitQuestionViewController.h
//  BabyMe
//

#import <UIKit/UIKit.h>
#import "BMButton.h"
#import "GAITrackedViewController.h"

@interface BMSubmitQuestionViewController : GAITrackedViewController

@property (copy, nonatomic)          NSString   *question;
@property (weak, nonatomic) IBOutlet UITextView *confirmQuestion;

@end
