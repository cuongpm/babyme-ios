//
//  BMPageContentViewController.m
//  BabyMe
//

#import "BMWalkthroughPageContentViewController.h"
#import "RNBlurModalView.h"
#import <QuartzCore/QuartzCore.h> 
#import "BMQuestionManager.h"

@interface BMWalkthroughPageContentViewController ()

@end

@implementation BMWalkthroughPageContentViewController

NSArray *pickerData = nil;

NSString *babyName;
NSString *babyBirthday;
NSString *babyGender;
NSString *userID;
NSDictionary *babyDict;

UIToolbar* toolBar;
UIDatePicker *birthdayPicker;
UIPickerView *genderPicker;

- (void)viewDidLoad {
    // Implement walkthrought
    
    // Set baby name delegate
    self.babyNameTextField.delegate = self;
    
    // Initialize toolbar (Done button)
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(closePicker)];
    NSArray* barItems = [NSArray arrayWithObjects:flexibleItem, doneButton, nil];
    [toolBar setItems:barItems animated:YES];

    // Initialize birthday picker
    birthdayPicker= [[UIDatePicker alloc] init];
    birthdayPicker.datePickerMode = UIDatePickerModeDate;
    birthdayPicker.backgroundColor = [UIColor whiteColor];
    [birthdayPicker addTarget:self action:@selector(birthdayPickerValueChanged:) forControlEvents:UIControlEventValueChanged];

    // Initialize gender picker
    genderPicker = [[UIPickerView alloc] init];
    genderPicker.backgroundColor = [UIColor whiteColor];
    genderPicker.delegate = self;

    // Change keyboard to picker when tapped
    [self.babyBirthdayTextField setInputView:birthdayPicker];
    [self.babyBirthdayTextField setInputAccessoryView:toolBar];

    pickerData = [NSArray arrayWithObjects:@"Chưa biết", @"Con trai", @"Con gái", nil];
    [self.babyGenderTextField setInputView:genderPicker];
    self.babyGenderTextField.text = pickerData[0];
    
}

- (void)closePicker {
    [self.view endEditing:YES];
}


#pragma mark - Update birthday text field
- (void)birthdayPickerValueChanged:(UIDatePicker *)datePicker {
    // Set format for birthday text field
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:datePicker.date];

    // Update birthday text field
    self.babyBirthdayTextField.text = strDate;
}

#pragma mark - Gender picker delegate
// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return pickerData[row];
}

// Update text field when select row
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    [self closePicker];
    self.babyGenderTextField.text = pickerData[row];
}

#pragma mark - Textfield delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)exitButtonDidTouch:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Button did touch

- (IBAction)loginButtonDidTouch:(id)sender {
        [self.parentViewController performSegueWithIdentifier:@"loginSegue" sender:nil];
    }

- (IBAction)submitButtonDidTouch:(id)sender {
    if ([self verifyBabyInfomation]) {
        [self prepareBabyInfo];
        [SVProgressHUD showWithStatus:@"Đang lưu thông tin của bé" maskType:SVProgressHUDMaskTypeBlack];
        [ApplicationDelegate.networkEngine sendBabyInfoWithName:babyName
                                                             gender:babyGender
                                                           birthday:babyBirthday
                                                            user_id:userID
                                                    completionBlock:^(id responseObject) {
                                                        // Store baby info
                                                        [self storeBabyInfoFromServer:responseObject];
        
                                                        // Dismiss progress
                                                        [SVProgressHUD dismiss];
                                                        
                                                        if(USER_ID == nil)
                                                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kLoggedInAsGuest];
                                                        else
                                                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kLoggedIn];
                                                        // Do sign up
                                                        [self doSignUp];
                                                    } errorBlock:^(NSError *error) {
                                                        // Dismiss progress
                                                        [SVProgressHUD dismiss];
        
                                                        // Show alert
                                                        [SIAlertView showAlertWithTitle:nil message:@"Có lỗi xảy ra. Vui lòng thử lại" cancelButtonTitle:kDismissText];
                                                    }];
//        [self.parentViewController performSegueWithIdentifier:@"registerSegue" sender:nil];
    }
}

- (void)doSignUp {
     [[BMQuestionManager sharedInstance] loadQuestionListWithSuccessBlock:^{
         
     }];
    BMRootSideMenuViewController *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"rootSideMenuNavigationController"];
    [self presentViewController:rootViewController animated:YES completion:nil];
}

#pragma mark - Helpers

- (BOOL)verifyBabyInfomation {
    // Check validation of baby name
    if (self.babyNameTextField.text.length < 1) {
        [SIAlertView showAlertWithTitle:nil message:@"Nhập lại tên của bé" cancelButtonTitle:kDismissText];
        [self.babyNameTextField becomeFirstResponder];
        return NO;
    }

    // Check validation of baby birthday
    if (self.babyBirthdayTextField.text.length < 1) {
        [SIAlertView showAlertWithTitle:nil message:@"Chọn ngày sinh/dự sinh cho bé" cancelButtonTitle:kDismissText];
        [self.babyBirthdayTextField becomeFirstResponder];
        return NO;
    }
    return YES;
}

- (void)prepareBabyInfo {
    babyName = self.babyNameTextField.text;
    babyBirthday = self.babyBirthdayTextField.text;
    userID = nil;
    if(USER_ID != nil){
        userID = USER_ID;
    }
    if ([self.babyGenderTextField.text isEqualToString:pickerData[1]])
        babyGender = @"male";
    else if ([self.babyGenderTextField.text isEqualToString:pickerData[2]])
        babyGender = @"female";
    else babyGender = @"unknown";

    babyDict = @{@"babyName"      : babyName,
                 @"babyBirthday"  : babyBirthday,
                 @"babyGender"    : babyGender,
    };
    BMWalkthroughPageViewController *controller = (BMWalkthroughPageViewController *)self.parentViewController;
    controller.tempBabyDict = babyDict;
}

- (void)storeBabyInfoFromServer:(NSDictionary *)dict {
    [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"id"] forKey:kBabyID];
    [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"name"] forKey:kBabyName];
    [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"dob"] forKey:kBabyBirthday];
    [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"gender"] forKey:kBabyGender];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
