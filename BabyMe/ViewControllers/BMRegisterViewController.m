//
//  BMRegisterViewController.m
//  BabyMe
//

#import "BMRegisterViewController.h"
#import "BMRootSideMenuViewController.h"
#import "RNBlurModalView.h"
#import "BMRootLoginNavigationViewController.h"
#import "BMQuestionManager.h"

@interface BMRegisterViewController ()
@property (nonatomic, readwrite) CGPoint *originalCenter;
@end

@implementation BMRegisterViewController

NSString *babyName;
NSString *babyGender;
NSString *babyBirthday;
float initialViewCenterY;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.screenName = @"SignupScreen";
    
    // Add background image into view controller
    UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImage setImage:[UIImage imageNamed:@"WalkthroughBackground.png"]];
    [backgroundImage setContentMode:UIViewContentModeScaleAspectFill];
    [self.view addSubview:backgroundImage];
    [self.view sendSubviewToBack:backgroundImage];
    
    UIColor *defaulColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
    UIColor *greenColor  = [UIColor colorWithRed:24.0/255.0 green:255.0/255.0 blue:19.0/255.0 alpha:1.0];
    NSMutableAttributedString *attrBtn = [[NSMutableAttributedString alloc] init];
    
    [attrBtn appendAttributedString:[[NSAttributedString alloc] initWithString:@"Bằng cách đăng ký tôi chấp thuận " attributes:@{NSForegroundColorAttributeName:  defaulColor}]];
    [attrBtn appendAttributedString:[[NSAttributedString alloc] initWithString:@"Điều khoản sử dụng " attributes: @{NSForegroundColorAttributeName: greenColor}]];
    [attrBtn appendAttributedString:[[NSAttributedString alloc] initWithString:@" và " attributes:@{NSForegroundColorAttributeName:  defaulColor}]];
    [attrBtn appendAttributedString:[[NSAttributedString alloc] initWithString:@"Chính sách bảo bật" attributes: @{NSForegroundColorAttributeName: greenColor}]];
    [attrBtn appendAttributedString:[[NSAttributedString alloc] initWithString:@" của babyMe" attributes:@{NSForegroundColorAttributeName:  defaulColor}]];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    [paragraphStyle setLineSpacing: 10.0];
    [attrBtn addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attrBtn.length)];
    [self.btnTerm setAttributedTitle: attrBtn forState: UIControlStateNormal];

    // Make UINavigationBar transparent
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];

    // Change color of loginFacebook Button
    registerFacebook.backgroundColor = [UIColor colorWithRed:76/255.f green:217/255.f blue:100/255.f alpha:1.0];

    // Textfield delegate
    emailField.delegate = self;
    passwordField.delegate = self;
    confirmPasswordField.delegate = self;

    // Paste baby info from previous screen
    babyName = _babyDict[@"babyName"];
    babyGender = _babyDict[@"babyGender"];
    babyBirthday = _babyDict[@"babyBirthday"];
    
    // Just show exit button when coming back from inside the app
    if ([self.parentViewController isKindOfClass:[UINavigationController class]]) {
        exitButton.hidden = YES;
        registerFacebook.translatesAutoresizingMaskIntoConstraints = YES;
        registerFacebook.centerX = self.view.centerX;
        registerFacebook.top = 100;
    }
    
    initialViewCenterY = self.view.center.y;
}

- (IBAction)onTermButton:(id)sender{
    NSLog(@"CLick");
    RNBlurModalView *modal;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, screenRect.size.width - 20, screenRect.size.height - 60)];
    
    webview.layer.cornerRadius = 5.f;
    webview.layer.borderColor = [UIColor whiteColor].CGColor;
    webview.layer.borderWidth = 5.f;
    webview.scalesPageToFit = YES;
    webview.autoresizesSubviews = YES;
    
    webview.contentMode = UIViewContentModeCenter;
    webview.scrollView.bounces = NO;
    webview.scrollView.zoomScale = 0.5;
    //    UIViewContentModeScaleAspectFit;
    NSString *urlString = @"http://www.babyme.vn/terms";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [webview loadRequest:urlRequest];
    modal = [[RNBlurModalView alloc] initWithViewController:self.parentViewController view:webview ];
    
    [modal show];
}


#pragma mark - Text field delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == emailField) {
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.navigationController.navigationBar.hidden = registerFacebook.hidden = NO;
                             divider.hidden = (IS_IPHONE4 ? YES : NO);
                             self.view.center = CGPointMake(self.view.center.x, (IS_IPHONE4 ? 220 : initialViewCenterY));
                         }];

    }
    if (textField == confirmPasswordField || textField == passwordField) {
        [UIView animateWithDuration:0.3
                         animations:^{
                             registerFacebook.hidden = YES;
                             self.navigationController.navigationBar.hidden = divider.hidden = (IS_IPHONE4 ? YES : NO);
                             self.view.center = CGPointMake(self.view.center.x, (IS_IPHONE4 ? 90 : 220));
                         }];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == emailField) [passwordField becomeFirstResponder];
    if (textField == passwordField) [confirmPasswordField becomeFirstResponder];
    if (textField == confirmPasswordField) {
        [self.view endEditing:YES];
        [self verifyCredential];
    }
    return YES;
}

#pragma mark - Button did touch

- (IBAction)submitButtonDidTouch:(id)sender {
    [self verifyCredential];
}

- (IBAction)signUpWithFacebookDidTouch:(id)sender {
    // Open a session showing the user the login UI
    // You must ALWAYS ask for public_profile permissions when opening a session
    [FBSession openActiveSessionWithReadPermissions:@[@"public_profile", @"email"]
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session, FBSessionState state, NSError *error) {
         // Call sessionStateChanged:state:error method to handle session state changes
         [ApplicationDelegate.networkEngine sessionStateChanged:session state:state error:error];
         if (!error && state == FBSessionStateOpen){
             NSLog(@"Session opened");
             
             // Call API
             [SVProgressHUD showWithStatus:@"Đang đăng ký" maskType:SVProgressHUDMaskTypeBlack];
             [ApplicationDelegate.networkEngine loginWithFacebookToken:(NSString *)FBSession.activeSession.accessTokenData
                                                 andCreateBabyWithName:(BABY_NAME ? BABY_NAME : babyName)
                                                                gender:(BABY_GENDER ? BABY_GENDER : babyGender)
                                                              birthday:(BABY_BIRTHDAY ? BABY_BIRTHDAY : babyBirthday)
                                                        completionBlock:^(id responseObject) {
                                                            // Store baby info
                                                            NSDictionary *babies = [responseObject[@"babies"] lastObject];
                                                            [self storeBabyInfoFromServer:babies];

                                                            // Dismiss progress
                                                            [SVProgressHUD dismiss];

                                                            // Do sign up
                                                            [self doSignUp];
                                                        } errorBlock:^(NSError *error) {
                                                            [FBSession.activeSession closeAndClearTokenInformation];
                                                            [SVProgressHUD dismiss];
                                                            [SIAlertView showAlertWithTitle:nil message:@"Có lỗi xảy ra. Vui lòng thử lại" cancelButtonTitle:kDismissText];
                                                        }];
             return;
         }
     }];
}

- (IBAction)skipButtonDidTouch:(id)sender {
    // Show progress
    if(!LOGGED_IN_GUEST){
        [self performSegueWithIdentifier:@"signUptoFillBaby" sender:nil];
    }else{
         [[BMQuestionManager sharedInstance] loadQuestionListWithSuccessBlock:^{
             
         }];
        BMRootSideMenuViewController *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"rootSideMenuNavigationController"];
        [self presentViewController:rootViewController animated:YES completion:nil];
    }
    
    
//    [ApplicationDelegate.networkEngine sendBabyInfoWithName:babyName
//                                                     gender:babyGender
//                                                   birthday:babyBirthday
//                                            completionBlock:^(id responseObject) {
//                                                // Store baby info
//                                                [self storeBabyInfoFromServer:responseObject];
//
//                                                // Dismiss progress
//                                                [SVProgressHUD dismiss];
//
//                                                // Set flag
//                                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kLoggedInAsGuest];
//
//                                                // Do sign up
//                                                [self doSignUp];
//                                            } errorBlock:^(NSError *error) {
//                                                // Dismiss progress
//                                                [SVProgressHUD dismiss];
//
//                                                // Show alert
//                                                [SIAlertView showAlertWithTitle:nil message:@"Có lỗi xảy ra. Vui lòng thử lại" cancelButtonTitle:kDismissText];
//                                            }];
}

- (IBAction)exitButtonDidTouch:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Helpers

- (void)storeBabyInfoFromServer:(NSDictionary *)dict {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kBabyID];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kBabyName];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kBabyBirthday];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kBabyGender];
    [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"id"] forKey:kBabyID];
    [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"name"] forKey:kBabyName];
    [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"dob"] forKey:kBabyBirthday];
    [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"gender"] forKey:kBabyGender];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)verifyCredential {

    // Simple validations
    if (emailField.text.length < 5 || [emailField.text rangeOfString:@"@"].location == NSNotFound || [emailField.text rangeOfString:@"."].location == NSNotFound){
        [SIAlertView showAlertWithTitle:nil message:@"Vui lòng nhập lại địa chỉ email" cancelButtonTitle:kDismissText];
        [emailField becomeFirstResponder];
        return;
    }
    if (passwordField.text.length < 1){
        [SIAlertView showAlertWithTitle:nil message:@"Vui lòng nhập lại mật khẩu" cancelButtonTitle:kDismissText];
        [passwordField becomeFirstResponder];
        return;
    }
    if (passwordField.text.length < 8){
        [SIAlertView showAlertWithTitle:nil message:@"Mật khẩu quá ngắn (ít nhất 8 ký tự)" cancelButtonTitle:kDismissText];
        [passwordField becomeFirstResponder];
        return;
    }
    if (![passwordField.text isEqualToString:confirmPasswordField.text]){
        [SIAlertView showAlertWithTitle:nil message:@"Mật khẩu không khớp. Vui lòng nhập lại" cancelButtonTitle:kDismissText];
        passwordField.text = confirmPasswordField.text = @"";
        [passwordField becomeFirstResponder];
        return;
    }

    // Verify credentials with server
    [SVProgressHUD showWithStatus:@"Đang đăng ký" maskType:SVProgressHUDMaskTypeBlack];

    // Call API
    
    [ApplicationDelegate.networkEngine signUpWithEmail:emailField.text
                                              password:passwordField.text
                                            baby_id:[NSString stringWithFormat:@"%zd",[[NSUserDefaults standardUserDefaults] secretIntegerForKey:kBabyID]]
                                      completionBlock:^(id responseObject) {
                                          // Store baby info
                                          NSDictionary *babies = [responseObject[@"babies"] lastObject];
                                          [self storeBabyInfoFromServer:babies];

                                          // Dismiss progress
                                          [SVProgressHUD dismiss];

                                          // Do sign up
                                          [self doSignUp];
                                       } errorBlock:^(NSError *error) {
                                           [SVProgressHUD dismiss];
                                           [SIAlertView showAlertWithTitle:nil message:@"Có lỗi xảy ra. Vui lòng thử lại" cancelButtonTitle:kDismissText];
                                       }];
}

- (void)doSignUp {
    if([[NSUserDefaults standardUserDefaults] secretIntegerForKey:kBabyID]){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kLoggedIn];
         [[BMQuestionManager sharedInstance] loadQuestionListWithSuccessBlock:^{
             
         }];
        BMRootSideMenuViewController *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"rootSideMenuNavigationController"];
            
            [self presentViewController:rootViewController animated:YES completion:nil];
    }else{
        [self performSegueWithIdentifier:@"signUptoFillBaby" sender:nil];
    }
    

}

-(IBAction)moveToLoginSreen:(id)sender{
    BMRootLoginNavigationViewController *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"rootLogin"];
    [self presentViewController:rootViewController animated:YES completion:nil];
}





@end
