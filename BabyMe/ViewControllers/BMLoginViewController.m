//
//  BMLoginViewController.m
//  BabyMe
//

#import "BMLoginViewController.h"
#import "BMRootSideMenuViewController.h"
#import "BMWalkthroughPageViewController.h"
#import "BMFillBabyInfoViewController.h"
#import "BMRegisterViewController.h"
#import "BMQuestionManager.h"

@implementation BMLoginViewController
float initialViewCenterY;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Add background image into view controller
    self.screenName = @"LoginScreen";
    UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:self.view.frame];
    [backgroundImage setImage:[UIImage imageNamed:@"WalkthroughBackground.png"]];
    [backgroundImage setContentMode:UIViewContentModeScaleAspectFill];
    [self.view addSubview:backgroundImage];
    [self.view sendSubviewToBack:backgroundImage];
        
    // Make UINavigationBar transparent
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    
    // Change color of loginFacebook Button
    loginFacebook.backgroundColor = [UIColor colorWithRed:76/255.f green:217/255.f blue:100/255.f alpha:1.0];

    // Just show exit button when coming back from inside the app
    if ([self.parentViewController isKindOfClass:[UINavigationController class]]) {
        exitButton.hidden = YES;
        loginFacebook.translatesAutoresizingMaskIntoConstraints = YES;
        loginFacebook.centerX = self.view.centerX;
        loginFacebook.top = 100;
    }

    
    initialViewCenterY = self.view.center.y;
}

#pragma mark - Textfield delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == emailField) {
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.navigationController.navigationBar.hidden = loginFacebook.hidden = NO;
                             divider.hidden = (IS_IPHONE4 ? YES : NO);
                             self.view.center = CGPointMake(self.view.center.x, (IS_IPHONE4 ? 230 : initialViewCenterY));
                         }];
        
    }
    if (textField == passwordField) {
        [UIView animateWithDuration:0.3
                         animations:^{
                             loginFacebook.hidden = YES;
                             self.navigationController.navigationBar.hidden = divider.hidden = (IS_IPHONE4 ? YES : NO);
                             self.view.center = CGPointMake(self.view.center.x, (IS_IPHONE4 ? 90 : 240));
                         }];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == emailField) [passwordField becomeFirstResponder];
    if (textField == passwordField) {
        [self verifyCredential];
    }
    return YES;
}

#pragma mark - Button did touch

- (IBAction)exitButtonDidTouch:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)submitButtonDidTouch:(id)sender {
    [self verifyCredential];
}

- (IBAction)signInWithfacebookLoginDidTouch:(id)sender {
    // Open a session showing the user the login UI
    // You must ALWAYS ask for public_profile permissions when opening a session
    [FBSession openActiveSessionWithReadPermissions:@[@"public_profile", @"email"]
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session, FBSessionState state, NSError *error) {
         // Call the app delegate's sessionStateChanged:state:error method to handle session state changes
         [ApplicationDelegate.networkEngine sessionStateChanged:session state:state error:error];
         if (!error && state == FBSessionStateOpen){
             NSLog(@"Session opened");
             [SVProgressHUD showWithStatus:@"Đang đăng nhập" maskType:SVProgressHUDMaskTypeBlack];
             [ApplicationDelegate.networkEngine loginWithFacebookToken:(NSString *)FBSession.activeSession.accessTokenData
                                                 andCreateBabyWithName:(BABY_NAME ? BABY_NAME : nil)
                                                                gender:(BABY_GENDER ? BABY_GENDER : nil)
                                                              birthday:(BABY_BIRTHDAY ? BABY_BIRTHDAY : nil)
                                                       completionBlock:^(id responseObject) {
                                                           NSLog(@"Facebook Token: %@", (NSString *)FBSession.activeSession.accessTokenData);
                                                           
                                                           // Check baby availability
                                                           NSDictionary *babies = [responseObject[@"babies"] lastObject];
                                                           if (babies == nil) {
                                                               // Dismiss progress
                                                               [SVProgressHUD dismiss];
                                                               
                                                               // Show pop up view
                                                               [self showPopUpToFillInfo];
                                                               return;
                                                           }

                                                           // Dismiss progress
                                                           [SVProgressHUD dismiss];
                                                           
                                                           // Store baby info
                                                           [self storeBabyInfoFromServer:babies];
                                                           
                                                           // Do sign in
                                                           [self doLogin];
                                                       } errorBlock:^(NSError *error) {
                                                           // Dismiss progress
                                                           [SVProgressHUD dismiss];

                                                           // Close FB session
                                                           [FBSession.activeSession closeAndClearTokenInformation];
                                                           
                                                           // Show alert
                                                           [SIAlertView showAlertWithTitle:nil message:@"Có lỗi xảy ra. Vui lòng thử lại" cancelButtonTitle:kDismissText];
                                                       }];
             return;
         }
     }];
}
- (IBAction)registerButtonDidTouch:(id)sender {
    BMRegisterViewController *registerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"registerViewController"];
    [self presentViewController:registerViewController animated:YES completion:nil];
}

#pragma mark - Helpers

- (void)storeBabyInfoFromServer:(NSDictionary *)dict {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kBabyID];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kBabyName];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kBabyBirthday];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kBabyGender];
    [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"id"] forKey:kBabyID];
    [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"name"] forKey:kBabyName];
    [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"dob"] forKey:kBabyBirthday];
    [[NSUserDefaults standardUserDefaults] setSecretObject:dict[@"gender"] forKey:kBabyGender];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)verifyCredential {
    // Simple validations
    if (emailField.text.length < 5 || [emailField.text rangeOfString:@"@"].location == NSNotFound || [emailField.text rangeOfString:@"."].location == NSNotFound){
        [SIAlertView showAlertWithTitle:nil message:@"Vui lòng nhập lại địa chỉ email" cancelButtonTitle:kDismissText];
        [emailField becomeFirstResponder];
        return;
    }
    if (passwordField.text.length < 1){
        [SIAlertView showAlertWithTitle:nil message:@"Vui lòng nhập lại mật khẩu" cancelButtonTitle:kDismissText];
        [passwordField becomeFirstResponder];
        return;
    }
    // Verify credentials with server
    [SVProgressHUD showWithStatus:@"Đang đăng nhập" maskType:SVProgressHUDMaskTypeBlack];
    [ApplicationDelegate.networkEngine loginWithEmail:emailField.text
                                             password:passwordField.text
                                      completionBlock:^(id responseObject) {
                                          NSDictionary *babies = [responseObject[@"babies"] firstObject];

                                          // Store baby info
                                          [self storeBabyInfoFromServer:babies];
                                          
                                          // Dismiss progress
                                          [SVProgressHUD dismiss];
                                          
                                          // Do login
                                          [self doLogin];
                                      } errorBlock:^(NSError *error) {
                                          // Dismiss progress
                                          [SVProgressHUD dismiss];

                                          // Show message login fail
                                          [SIAlertView showAlertWithTitle:nil message:@"Email hoặc mật khẩu của bạn không đúng." cancelButtonTitle:kDismissText];
                                      }];
}

- (void)showPopUpToFillInfo {
        BMFillBabyInfoViewController *fillBabyInfoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"fillBabyInfoViewController"];
        [self.navigationController presentViewController:fillBabyInfoViewController animated:YES completion:nil];
}

- (void)doLogin {
    if (![self.parentViewController isKindOfClass:[UINavigationController class]]) {
        [[[self presentingViewController] presentingViewController] dismissModalViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    else {
        [[BMQuestionManager sharedInstance] loadQuestionListWithSuccessBlock:^{
            
        }];
        BMRootSideMenuViewController *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"rootSideMenuNavigationController"];
        [self presentViewController:rootViewController animated:YES completion:nil];
    }
}


- (void)viewWillDisappear:(BOOL)animated {
    self.view.center = CGPointMake(self.view.center.x, initialViewCenterY);
}

-(IBAction)moveToSignupScreen:(id)sender{
    BMRootSideMenuViewController *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"rootNavigationController"];
    [self presentViewController:rootViewController animated:YES completion:nil];
}

@end
