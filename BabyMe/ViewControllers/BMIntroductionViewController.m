//
//  BMIntroductionViewController.m
//  BabyMe
//

#import "BMIntroductionViewController.h"

@interface BMIntroductionViewController ()

@end

@implementation BMIntroductionViewController
{
    
    __weak IBOutlet UIButton *facebookButton;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"IntroScreen";
    
    // Format Navbar
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Sidebar Indicator"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(sideMenuButtonDidTouch:)];
    [menuButton setTintColor:[UIColor whiteColor]];
    self.navigationItem.leftBarButtonItem = menuButton;
    self.navigationController.navigationBar.barTintColor = NAVBAR_COLOR;
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UILabel * titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"Giới thiệu";
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    _versionApp.text = [@"Version " stringByAppendingString:appVersion];
    // Set atributedstring for button title
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:@"babyme trên facebook"];
    UIColor *color= NAVBAR_COLOR;
    UIFont *font1=[UIFont fontWithName:@"Comfortaa" size:16.0f];
    UIFont *font2=[UIFont fontWithName:@"Roboto-Light" size:16.0f];
    [string addAttribute:NSFontAttributeName value:font1 range:NSMakeRange(0, 6)];
        [string addAttribute:NSFontAttributeName value:font2 range:NSMakeRange(7, 13)];
    [string addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(0, 20)];
    [facebookButton setAttributedTitle:string forState:UIControlStateNormal];
}

- (IBAction)sideMenuButtonDidTouch:(id)sender {
    [self presentLeftMenuViewController:nil];
}

- (IBAction)linkFacebookButtonDidTouch:(id)sender {
    NSURL *url = [NSURL URLWithString:@"fb://profile/287585591415339"];
    [[UIApplication sharedApplication] openURL:url];
}


@end
