//
//  BMRelatedQuestionViewController.m
//  BabyMe
//

#import "BMRelatedQuestionViewController.h"
#import "BMQuestionTableViewCell.h"
#import "BMQuestionManager.h"
#import "BMQuestionDetailViewController.h"
#import "BMArticleDetailViewController.h"
#import "UIImageView+WebCache.h"

@implementation BMRelatedQuestionViewController
{
    NSMutableDictionary    *rowHeightCache;
    NSArray      *questions;
    
    NSArray      *questionContents;
    NSArray      *answerContents;
    NSArray      *questionAvatars;
    NSInteger          questionCount;
    
    NSInteger           articleCount;
    int           articleid;
    NSMutableArray      *articleTitles;
    NSMutableArray      *articleContents;
    
    NSArray      *articleArray;
    
    __weak IBOutlet UIActivityIndicatorView *actInd;
    
    __weak IBOutlet UILabel *noDataLabel;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"RelatedQuestionsScreen";
    // Hide table view
    self.tableView.hidden = YES;
     // Load data
    if (_relatedButtonTouched == relatedQuestion) {
        [self setTitle:@"Câu hỏi liên quan"];
        [self loadQuestionData];
    }
    if (_relatedButtonTouched == relatedArticle && _categoryid > 0) {
        [self setTitle:@"Bài viết liên quan"];
        [self loadArticleDataByCategory];
    }
    if (_relatedButtonTouched == relatedArticle && _categoryid == 0) {
        [self setTitle:@"Bài viết liên quan"];
        [self loadArticleDataByTag];
    }

    // Make UINavigationBar transparent
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];

    // Change color of navigation background
    self.navigationController.navigationBarHidden = NO;
    self.view.backgroundColor = NAVBAR_COLOR;
    
    // Change color of navigation title
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

}

- (void)setTitle:(NSString *)title
{
    UILabel *titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont systemFontOfSize:17.0];
        titleView.textColor = [UIColor whiteColor];
        self.navigationItem.titleView = titleView;
    }
    titleView.text = title;
    [titleView sizeToFit];
}

- (void)viewDidAppear:(BOOL)animated {
    // Compact navigationController's VCs array to remove the intermediary VC.
    // Back button now goes back to home page viewController
    if (self.navigationController.viewControllers.count > 3) {
        UIViewController * firstVC = [self.navigationController.viewControllers firstObject];
        [self.navigationController setViewControllers:@[firstVC, self]];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (_relatedButtonTouched) {
        case relatedQuestion:
            return questionCount;
            break;
        case relatedArticle:
            return articleCount;
            break;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    BMQuestionTableViewCell *cell = (BMQuestionTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"BMQuestionTableViewCell" owner:self options:nil] lastObject];
    }
    
    switch (_relatedButtonTouched) {
        case relatedQuestion:
            cell.questionContent.text  = [questionContents objectAtIndex:indexPath.row];
            cell.answerContent.text = [answerContents objectAtIndex:indexPath.row];
            [cell.questionerAvatar sd_setImageWithURL:[NSURL URLWithString:[questionAvatars objectAtIndex:indexPath.row]] placeholderImage:cell.questionerAvatar.image];
            break;
        case relatedArticle:
            cell.questionContent.text  = [articleTitles objectAtIndex:indexPath.row];
            cell.answerContent.text = [articleContents objectAtIndex:indexPath.row];
            break;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    // Look up in height cache
    rowHeightCache = [NSMutableDictionary dictionary];
    NSNumber * h = rowHeightCache[@(indexPath.row)];
    if (h) return h.floatValue;
    
    // Temp var
    CGSize maximumLabelSize = CGSizeMake(self.view.width-58-20, CGFLOAT_MAX);
    CGRect frame;
    CGFloat padding = 80;
    NSString * body;
    
    switch (_relatedButtonTouched) {
        case relatedQuestion:
            // Height for question content
            body = [questionContents objectAtIndex:indexPath.row];
            break;
        case relatedArticle:
            // Height for article title
            body = [articleTitles objectAtIndex:indexPath.row];
            break;
    }

    // Calculate content height and cache height
    frame = [body boundingRectWithSize:maximumLabelSize
                               options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                            attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                               context:nil];
    CGFloat t = frame.size.height + padding;
    rowHeightCache[@(indexPath.row)] = @(t);
    return t;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (_relatedButtonTouched) {
        case relatedQuestion:
            // Perform segue
            [self performSegueWithIdentifier:@"relatedQuestionToQuestionDetailSegue" sender:nil];
            break;
        case relatedArticle:
            // Perform segue
            [self performSegueWithIdentifier:@"relatedQuestionToArticleDetailSegue" sender:nil];
            break;
    }
    // Deselect after select
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(void)loadQuestionData {
    [actInd startAnimating];
    [[BMQuestionManager sharedInstance] loadRelatedQuestionListOf:_questionid
                                                 withSuccessBlock:^{
                                                     // Hide activity indicator
                                                     [actInd stopAnimating];

                                                     // Get question data
                                                     questions = [BMQuestionManager sharedInstance].questionList;
                                                     
                                                     // Check quesiton data
                                                     if (questions != nil) {
                                                         // Count question object
                                                         questionCount = questions.count;
                                                         noDataLabel.hidden = !(questionCount == 0);
                                                         
                                                         // Get question/answer content data
                                                         questionContents = [[BMQuestionManager sharedInstance] getQuestionList:questions];
                                                         answerContents = [[BMQuestionManager sharedInstance] getAnswerList:questions];
                                                         questionAvatars = [[BMQuestionManager sharedInstance] getAvatarList:questions];
                                                     }
                                                     if (questionCount == 0) {
                                                         noDataLabel.hidden = NO;
                                                         return;
                                                     }

                                                     // Show table view when data loaded
                                                     self.tableView.hidden = NO;
                                                     
                                                     // Reload table view
                                                     [self.tableView reloadData];
                                                 }
                                                    andErrorBlock:^{
                                                        [actInd stopAnimating];
                                                        [self dismissViewControllerAnimated:YES completion:nil];
                                                    }];
}

-(void)loadArticleDataByCategory {
    [actInd startAnimating];
    [[ApplicationDelegate networkEngine] getRelatedArticleOf:_categoryid
                                             completionBlock:^(id responseObject) {
                                                 // Hide activity indicator
                                                 [actInd stopAnimating];

                                                 articleArray = responseObject[@"data"];
                                                 articleTitles = [NSMutableArray array];
                                                 articleContents = [NSMutableArray array];
                                                 articleCount = [articleArray count];
                                                 
                                                 for (NSDictionary *d in responseObject[@"data"]) {
                                                     [articleTitles addObject:d[@"title"]];
                                                     [articleContents addObject:d[@"content"]];
                                                 }
                                                 if (articleCount == 0) {
                                                     noDataLabel.hidden = NO;
                                                     return;
                                                 }

                                                 // Show table view when data loaded
                                                 self.tableView.hidden = NO;
                                                 
                                                 // Reload table view
                                                 [self.tableView reloadData];
                                             } errorBlock:^(NSError *error) {
                                                 [actInd stopAnimating];
                                                 [self dismissViewControllerAnimated:YES completion:nil];
                                             }];
}

-(void)loadArticleDataByTag {
    [actInd startAnimating];
    [[ApplicationDelegate networkEngine] getRelatedArticleByTag:_tagid
                                                completionBlock:^(id responseObject) {
                                                    // Hide activity indicator
                                                    [actInd stopAnimating];

                                                    articleArray = responseObject[@"data"];
                                                    articleTitles = [NSMutableArray array];
                                                    articleContents = [NSMutableArray array];
                                                    articleCount = [articleArray count];
                                                    
                                                    for (NSDictionary *d in responseObject[@"data"]) {
                                                        [articleTitles addObject:d[@"title"]];
                                                        [articleContents addObject:d[@"content"]];
                                                    }
                                                    
                                                    if (articleCount == 0) {
                                                        noDataLabel.hidden = NO;
                                                        return;
                                                    }
                                                    // Show table view when data loaded
                                                    self.tableView.hidden = NO;
                                                    
                                                    // Reload table view
                                                    [self.tableView reloadData];
                                                } errorBlock:^(NSError *error) {
                                                    [actInd stopAnimating];
                                                    [self dismissViewControllerAnimated:YES completion:nil];
                                                }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"relatedQuestionToQuestionDetailSegue"]) {
        BMQuestionDetailViewController *controller = [segue destinationViewController];
        NSIndexPath * indexPath = [self.tableView indexPathForSelectedRow];
        Question *tempQuestion = [questions objectAtIndex:indexPath.row];
        controller.questionid = tempQuestion.questionid;
    }
    if ([segue.identifier isEqualToString:@"relatedQuestionToArticleDetailSegue"]) {
        BMArticleDetailViewController *controller = [segue destinationViewController];
        NSIndexPath * indexPath = [self.tableView indexPathForSelectedRow];
        articleid = [[articleArray objectAtIndex:indexPath.row][@"id"] intValue];
        controller.articleid = articleid;
    }
}
@end
