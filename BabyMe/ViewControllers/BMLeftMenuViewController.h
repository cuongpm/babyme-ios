//
//  BMLeftMenuViewController.h
//  BabyMe
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
#import "GAITrackedViewController.h"

@interface DEMOLeftMenuViewController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate, RESideMenuDelegate>

@end