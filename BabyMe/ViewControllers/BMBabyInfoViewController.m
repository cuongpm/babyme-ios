//
//  BMBabyInfoViewController.m
//  BabyMe
//

#import "BMBabyInfoViewController.h"
#import "UIViewController+RESideMenu.h"
#import "BMBabyInfoViewController.h"

@interface BMBabyInfoViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *babyImageView;
@property (weak, nonatomic) IBOutlet UILabel *babyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *babyDobLabel;
@property (weak, nonatomic) IBOutlet UILabel *babyGenderLabel;

@end

@implementation BMBabyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"BabyInfoScreen";
    // Format Navbar
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Sidebar Indicator"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(sideMenuButtonDidTouch:)];
    [menuButton setTintColor:[UIColor whiteColor]];
    self.navigationItem.leftBarButtonItem = menuButton;
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Edit"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(editButtonDidTouch:)];
    [editButton setTintColor:[UIColor whiteColor]];
    self.navigationItem.rightBarButtonItem = editButton;
    
    self.navigationController.navigationBar.barTintColor = NAVBAR_COLOR;
    [self.navigationController.navigationBar setTranslucent:NO];
    UILabel * titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.text = @"Thông tin của bé";
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    
    // Format the image
    self.babyImageView.layer.borderWidth = 10;
    self.babyImageView.layer.borderColor = [UIColorFromRGB(0xf2f2f2) CGColor];
    self.babyImageView.clipsToBounds = YES;
    self.babyImageView.layer.cornerRadius = self.babyImageView.width/2;
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // Populate data
    self.babyNameLabel.text = [[NSUserDefaults standardUserDefaults] secretStringForKey:kBabyName];
    self.babyDobLabel.text = [[NSUserDefaults standardUserDefaults] secretStringForKey:kBabyBirthday];
    self.babyGenderLabel.text = [[[NSUserDefaults standardUserDefaults] secretStringForKey:kBabyGender] isEqualToString:@""] ? @"Chưa rõ giới tính" : [[NSUserDefaults standardUserDefaults] secretStringForKey:kBabyGender];
}
- (void)sideMenuButtonDidTouch:(id)sender {
    [self presentLeftMenuViewController:nil];
}

- (void)editButtonDidTouch:(id)sender {
    BMBabyInfoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"fillBabyInfoViewController"];
    [self presentViewController:controller animated:YES completion:nil];
}
@end
