//
//  BMArticleDetailViewController.h
//  BabyMe
//

#import <UIKit/UIKit.h>
#import "Question.h"
#import "GAITrackedViewController.h"

@interface BMArticleDetailViewController : GAITrackedViewController <UIWebViewDelegate, UIScrollViewDelegate>
{
    __weak IBOutlet UIWebView *contentWebView;
    
}

// Data from segue
@property (nonatomic) int  articleid;
@end
