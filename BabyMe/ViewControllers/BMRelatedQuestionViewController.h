//
//  BMRelatedQuestionViewController.h
//  BabyMe
//

#import <UIKit/UIKit.h>
#import "BMQuestionDetailViewController.h"
#import "GAITrackedViewController.h"

@interface BMRelatedQuestionViewController : GAITrackedViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic)       int  questionid;
@property (nonatomic)       int  categoryid;
@property (nonatomic)       int  tagid;
@property (nonatomic)       RelatedButtonTouched relatedButtonTouched;

@end
