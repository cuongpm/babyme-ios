//
//  BMRootLoginNavigationViewController.m
//  BabyMe
//
//  Created by Cuong Pham on 3/2/15.
//  Copyright (c) 2015 Silicon Straits Saigon. All rights reserved.
//

#import "BMRootLoginNavigationViewController.h"

@interface BMRootLoginNavigationViewController ()

@end

@implementation BMRootLoginNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
