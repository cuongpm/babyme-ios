//
//  BMSearchViewController.m
//  BabyMe
//

#import "BMSearchViewController.h"
#import "BMQuestionTableViewCell.h"
#import "BMQuestionDetailViewController.h"
#import "BMQuestionManager.h"
#import "BMSubmitQuestionViewController.h"
#import "UIImageView+WebCache.h"
#import "BMLoginViewController.h"
#import "BMIntroViewController.h"
@implementation BMSearchViewController
{
    __weak IBOutlet UITextField *searchBar;
    __weak IBOutlet UIView *fakeNavigationBar;
    
    NSString    *keyword;
    NSArray     *searchResults;
    NSInteger   searchResultCount;

    NSArray *questionContents;
    NSArray *answerContents;
    NSArray *questionAvatars;
    int questionID;
    
    NSMutableDictionary * rowHeightCache;

    AFHTTPRequestOperation * currentSearchRequest;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"SearchScreen";
    // Observe search bar text field change
    [searchBar addTarget:self
                  action:@selector(searchBarTextFieldDidChange)
        forControlEvents:UIControlEventEditingChanged];

    // Point to search bar text field
    [searchBar becomeFirstResponder];

    // Set navigation bar color
    fakeNavigationBar.backgroundColor = NAVBAR_COLOR;
    
    // Hide table view
    self.tableView.hidden = YES;
    
    // Observe post question notification to dismiss this view controller
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissThisViewController) name:@"ShowMyQuestionNotification" object:nil];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    self.navigationController.navigationBarHidden = NO;

    // Make sure request stopped, otherwise it will crash
    [currentSearchRequest cancel];
    currentSearchRequest = nil;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return searchResultCount;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    BMQuestionTableViewCell *cell = (BMQuestionTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"BMQuestionTableViewCell" owner:self options:nil] lastObject];
    }
    cell.questionContent.text  = [questionContents objectAtIndex:indexPath.row];
    cell.answerContent.text = [answerContents objectAtIndex:indexPath.row];
    [cell.questionerAvatar sd_setImageWithURL:[NSURL URLWithString:[questionAvatars objectAtIndex:indexPath.row]] placeholderImage:cell.questionerAvatar.image];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    // Look up in height cache
    NSNumber * h = rowHeightCache[@(indexPath.row)];
    if (h) return h.floatValue;

    // Temp var
    CGSize maximumLabelSize = CGSizeMake(self.view.width-58-20, CGFLOAT_MAX);
    CGRect frame;
    CGFloat padding = 80;
    NSString * body;

    // Height for original question
    body = [questionContents objectAtIndex:indexPath.row];

    // Calculate content height and cache height
    frame = [body boundingRectWithSize:maximumLabelSize
                               options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                            attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                               context:nil];
    CGFloat t = frame.size.height + padding;
    rowHeightCache[@(indexPath.row)] = @(t);
    return t;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"searchToDetailSegue" sender:nil];
}

#pragma mark - Search bar Textfield delegate

-(void)searchBarTextFieldDidChange {
    // Start activity indicator
    [_actInd startAnimating];

    // Minimum keyword length is 2
    keyword = searchBar.text;
    if (keyword.length < 2) {
        [_actInd stopAnimating];
        self.tableView.hidden = YES;
        [currentSearchRequest cancel];
        currentSearchRequest = nil;
        return;
    }

    // Cancel previous request
    if (currentSearchRequest != nil && currentSearchRequest.isExecuting) {
        [currentSearchRequest cancel];
    }

    // Call API
    currentSearchRequest =
    [[BMQuestionManager sharedInstance] startSearchBy:keyword
                                      withSucessBlock:^{
                                          // Stop activity indicator
                                          [_actInd stopAnimating];

                                          // Get search result fata
                                          searchResults = [BMQuestionManager sharedInstance].searchResultList;
                                          
                                          // Count question object
                                          searchResultCount = searchResults.count;
                                          
                                          // Get question/answer content data
                                          questionContents = [[BMQuestionManager sharedInstance] getQuestionList:searchResults];
                                          answerContents = [[BMQuestionManager sharedInstance] getAnswerList:searchResults];
                                          questionAvatars = [[BMQuestionManager sharedInstance] getAvatarList:searchResults];
                                          
                                          // Reload table view
                                          self.tableView.hidden = NO;
                                          [self.tableView reloadData];

                                          // Clear request
                                          currentSearchRequest = nil;
                                      }];
}

- (IBAction)exitButtonDidTouch:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)questionButtonDidTouch:(id)sender {
    if(LOGGED_IN){
        [self performSegueWithIdentifier:@"searchtoSubmitQuestionSegue" sender:nil];
    }else{
        [SIAlertView showAlertWithTitle:nil
                                message:@"Bạn cần đăng nhập để sử dụng tính năng này"
                      cancelButtonTitle:@"Đóng lại"
                          okButtonTitle:@"ĐĂNG NHẬP"
                                handler:^{
                                    // Go to login screen
                                    BMIntroViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"introViewController"];
                                    [self presentViewController:controller animated:YES completion:nil];
                                }];
    }
}

- (void) dismissThisViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"searchToDetailSegue"]) {
        BMQuestionDetailViewController *controller = [segue destinationViewController];
        NSIndexPath * indexPath = [self.tableView indexPathForSelectedRow];
        Question *tempResult = [searchResults objectAtIndex:indexPath.row];
        controller.questionid = tempResult.questionid;
    }
    if ([segue.identifier isEqualToString:@"searchtoSubmitQuestionSegue"]) {
        BMSubmitQuestionViewController *controller = [segue destinationViewController];
        controller.question = keyword;
    }
}
@end
