//
//  BMSubmitQuestionViewController.m
//  BabyMe
//

#import "BMSubmitQuestionViewController.h"
#import "BMMyQuestionViewController.h"
#import "BMLeftMenuViewController.h"
#import "BMSearchViewController.h"
@implementation BMSubmitQuestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"SubmitQuestionScreen";
    // Pass data from previous VC
    self.confirmQuestion.text = _question;
    
    // Remove space on top
    self.automaticallyAdjustsScrollViewInsets = NO;

    // Set navigation bar color
    self.navigationController.navigationBar.barTintColor = NAVBAR_COLOR;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Gửi câu hỏi"
                                                                              style:UIBarButtonItemStyleDone target:self
                                                                             action:@selector(submitButtonDidTouch:)];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    // Auto bring up keyboard
    [self.confirmQuestion becomeFirstResponder];
}

- (IBAction)submitButtonDidTouch:(id)sender {
    [SVProgressHUD showWithStatus:@"Đang gửi câu hỏi" maskType:SVProgressHUDMaskTypeBlack];
    [ApplicationDelegate.networkEngine createQuestionWithTitle:self.confirmQuestion.text
                                                       baby_id:BABY_ID
                                               completionBlock:^(id responseObject) {
                                                   [SVProgressHUD dismiss];
                                                   [self.view endEditing:YES];
                                                   [SIAlertView showAlertWithTitle:nil
                                                                           message:@"Chúng tôi đã nhận được câu hỏi, câu trả lời sẽ được cập nhật tại \"Câu hỏi của bạn\" sớm nhất có thể!" cancelButtonTitle:kDismissText animation:SIAlertViewTransitionStyleFade handler:^{
//                                                                               [self dismissViewControllerAnimated:YES completion:nil];
//                                                                               mySubmitToQuestionSegue
                                                                               
                                                                                
                                                                               [self.navigationController popToViewController:self.navigationController.viewControllers[0] animated:YES];
                                                                           }];

                                               } errorBlock:^(NSError *error) {
                                                   [SVProgressHUD dismiss];
                                                   [SIAlertView showAlertWithTitle:nil message:@"Có lỗi xảy ra. Vui lòng thử lại" cancelButtonTitle:kDismissText];
                                               }];
}

@end
