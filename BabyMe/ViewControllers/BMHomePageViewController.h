//
//  BMHomePageViewController.h
//  BabyMe
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
#import "BMQuestionTableViewCell.h"
#import "BMReminderTableViewCell.h"
#import "GAITrackedViewController.h"

typedef enum {
    week,
    reminder,
    question,
    bookmark,
} ButtonTouched;

@interface BMHomePageViewController : GAITrackedViewController <UIGestureRecognizerDelegate, UIScrollViewDelegate, UISearchBarDelegate, UIWebViewDelegate, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate>
{
    __weak IBOutlet UIView *topView;
    __weak IBOutlet UIButton *sideMenuButton;
    __weak IBOutlet UITextField *searchBar;

    // For parallax effect
    __weak IBOutlet UIImageView *babyImage;
    __weak IBOutlet UIImageView *homePageBackground;
    __weak IBOutlet UIPageControl *pageControl;
    __weak IBOutlet UIButton *thisWeekLeftButton;
    __weak IBOutlet UIButton *thisWeekRightButton;
    
    __weak IBOutlet UIImageView *imageView;
    
    // Header
    __weak IBOutlet UIView *headerView;
    __weak IBOutlet UILabel *weekLabel;
    __weak IBOutlet UIButton *weekButton;
    __weak IBOutlet UIButton *reminderButton;
    __weak IBOutlet UIButton *questionButton;
    __weak IBOutlet UIButton *followButton;
    __weak IBOutlet UIImageView *indicator;
    
    // Content
    __weak IBOutlet UIWebView *contentWebView;
    __weak IBOutlet UITableView *contentTableView;
    __weak IBOutlet UILabel *noDataLabel;
    __weak IBOutlet UIActivityIndicatorView *actInd;
    
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;


-(NSString *)platformRawString;
-(NSString *)platformNiceString;
- (IBAction)weekButtonDidTouch:(id)sender;
- (IBAction)reminderButtonDidTouch:(id)sender;
- (IBAction)questionButtonDidTouch:(id)sender;
- (IBAction)followButtonDidTouch:(id)sender;
- (IBAction)searchBarMask:(id)sender;


@end
