//
//  BMQuestionDetailViewController.m
//  BabyMe
//

#import "BMQuestionDetailViewController.h"
#import "BMSearchViewController.h"
#import "BMRelatedQuestionViewController.h"
#import "BMHomePageViewController.h"
#import "BMCommentViewCell.h"
#import "BMLoginViewController.h"
#import "NSString+Avatar.h"
#import "UIImageView+WebCache.h"
#import "BMIntroViewController.h"

@implementation BMQuestionDetailViewController {
    NSMutableArray  *comments;
    CGFloat webViewHeight;
    int     questionID;
    int     categoryID;
    NSString *sourceLogoLink;
    BOOL    isKeyboardUp;
    int votesCount;
    int commentsCount;
    RelatedButtonTouched relatedButtonTouched;
    __weak IBOutlet UIActivityIndicatorView *actInd;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"QuestionsScreen";
    // Call API
    [self getQuestionDetail];

    // Hide table view when having no data
    self.bookmarkButton.hidden = self.tableView.hidden = YES;

    // Add footer view to tableview
    self.tableView.tableFooterView = self.footerView;
    self.answerBodyWebView.delegate = self;
    [_answerBodyWebView loadHTMLString:@"" baseURL:nil];
    _answerBodyWebView.delegate = self;
    // Keyboard notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidChangeFrame:)
                                                 name:UIKeyboardDidChangeFrameNotification
                                               object:nil];
    // Misc
    self.answerBodyWebView.scrollView.scrollsToTop = NO;

    self.tableView.scrollsToTop = YES;

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // Show navbar
    self.navigationItem.prompt = nil;
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = NAVBAR_COLOR;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    // Compact navigationController's VCs array to remove the intermediary VC.
    // Back button now goes back to home page viewController
    if (self.navigationController.viewControllers.count > 3) {
        UIViewController * firstVC = [self.navigationController.viewControllers firstObject];
        [self.navigationController setViewControllers:@[firstVC, self]];
    }
}

-(void)populateQuestionAndAnswer {
    // Parse data from previous controller
    Question * q = self.question;
    self.questionerNameLabel.text = q.username;
    self.questionBodyLabel.text = q.question;
    self.answererNameLabel.text = q.sourcename;
    [self.answerBodyWebView loadHTMLString:q.answer baseURL:nil];
    questionID = q.questionid;
    categoryID = q.categoryid;
    votesCount = q.votesCount;
    sourceLogoLink = q.sourcelogo;
    commentsCount = q.commentsCount;
    comments   = [NSMutableArray arrayWithArray:_question.comments];
    if(q.articlesCount == 0){
        _articleRelateLink.hidden = YES;
        _articleLine.hidden = YES;
    }else{
        _articleRelateLink.hidden = NO;
        _articleLine.hidden = NO;
    }
    self.bookmarkButton.hidden = self.tableView.hidden = NO;
    [self.tableView reloadData];
    
    // Answer avatar (source logo)
    [self.answererAvatar sd_setImageWithURL:[NSURL URLWithString:sourceLogoLink] placeholderImage:self.answererAvatar.image];
    
    // Hide avatar if there is no answer (answer length will be 140, not 0 because of HTML tags)
//    if (self.question.answer == nil || self.question.answer.length < 141) {
//        NSMutableString *html = [NSMutableString stringWithString: @"<html><head><title></title></head><body style=\"background:transparent;\">"];
//        [html appendString:@"<div style=\"font-family: 'Roboto-Light' !important; font-size: 16px !important; line-height: 1.2; text-align: justify; padding: 0px;\" <p> Hiện tại chưa có câu trả lời </p> </div>"];
//        [self.answerBodyWebView loadHTMLString:html baseURL:nil];
//        self.answererAvatar.hidden = YES;
//    }

    // Parse avatar ID from email
    self.questionerAvatar.layer.cornerRadius = self.questionerAvatar.width/2;
    self.questionerAvatar.layer.borderColor = UIColorFromRGB(0xbdbdbd).CGColor;
    self.questionerAvatar.layer.borderWidth = 1.0;
    NSString * email = self.question.email;
    [self.questionerAvatar sd_setImageWithURL:[email avatarFromEmail] placeholderImage:self.questionerAvatar.image];

    // Vote & comment count
    self.voteCountLabel.text = [NSString stringWithFormat:@"%d Hữu ích", votesCount];
    self.commentCountLabel.text = [NSString stringWithFormat:@"%d Thảo luận", commentsCount];
    
    // Check bookmarks & votes
    [self checkBookmark];
    [self checkVote];
    
    // Set comment view color
    self.commentView.backgroundColor = NAVBAR_COLOR;
}

#pragma mark - Keyboard delegate
-(void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGRect kKeyboardFrame = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    double kKeyboardTransitionTime = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];;
    _commentView.translatesAutoresizingMaskIntoConstraints = YES;
    [UIView animateWithDuration:kKeyboardTransitionTime
                     animations:^{
                         _commentView.bottom = self.view.height - kKeyboardFrame.size.height;
                     }];
    isKeyboardUp = YES;
}

-(void)keyboardWillHide:(NSNotification *)notification {
    _commentView.bottom = self.view.height;
    isKeyboardUp = NO;
}

- (void)keyboardDidChangeFrame:(NSNotification *)notification {
    if (isKeyboardUp) {
        NSDictionary* info = [notification userInfo];
        CGRect kKeyboardFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        _commentView.translatesAutoresizingMaskIntoConstraints = YES;
        _commentView.bottom = self.view.height - kKeyboardFrame.size.height;
    }
}

#pragma mark - Scrollview delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offset = self.tableView.contentOffset.y;
    // Hide & show bookmark button when scroll
    if (offset > 0 && self.bookmarkButton.alpha > 0) {
        [UIView animateWithDuration:0.2
                         animations:^{
                             self.bookmarkButton.alpha = 0;
                         }];
    } else if (offset <= 1 && self.bookmarkButton.alpha < 1) {
        [UIView animateWithDuration:0.2
                         animations:^{
                             self.bookmarkButton.alpha = 1;
                         }];
    }
    
}

#pragma mark - Text field delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    [self checkLoginStatus:^{
        [_commentTextField becomeFirstResponder];
    }];
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [_commentTextField resignFirstResponder];
    return YES;
}

#pragma mark - Table View Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return comments == nil ? 1 : comments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BMCommentViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CommentViewCell" forIndexPath:indexPath];
    cell.comment = comments[indexPath.row];
    return cell;
}



-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest
 navigationType:(UIWebViewNavigationType)inType
{
    NSLog(@"expected:%d, got:%d", UIWebViewNavigationTypeLinkClicked, inType);
    if ( inType == UIWebViewNavigationTypeLinkClicked )
    {
        NSString* url = inRequest.URL.absoluteString;
        // Open all URLs that are not app store links with Safari
        if ([url rangeOfString:@"itms-apps://"].location == NSNotFound)
        {
            [[UIApplication sharedApplication] openURL:[inRequest URL]];
            return NO;
        }
    }
    
    return YES;
}

-(UITableView *) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    return indexPath;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize maximumLabelSize = CGSizeMake(self.view.width-58-20, CGFLOAT_MAX);
    CGRect frame;
    CGFloat padding = 50;
    NSString * body;

    NSDictionary * comment = comments[indexPath.row];
    body = comment[@"content"];

    frame = [body boundingRectWithSize:maximumLabelSize
                               options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                            attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                               context:nil];
    CGFloat t = frame.size.height + padding;
    return t;
}

#pragma mark - Button did touch

- (IBAction)bookmarkButtonDidTouch:(id)sender {
    [self checkLoginStatus:^{
        self.bookmarkButton.selected = !self.bookmarkButton.selected;
        if (!(questionID > 0)) {
            questionID = _questionid;
        }
        if (self.bookmarkButton.selected)
        {
            [ApplicationDelegate.networkEngine bookmarkQuestion:questionID
                                                completionBlock:^(id responseObject) {
                                                    // Set already bookmarked
                                                    self.question.isBookmark = self.bookmarkButton.selected;
                                                } errorBlock:^(NSError *error) {
                                                    // Revert
                                                    self.bookmarkButton.selected = NO;
                                                }];
        } else {
            [ApplicationDelegate.networkEngine unBookmarkQuestion:questionID
                                                  completionBlock:^(id responseObject) {
                                                      // Set not bookmark yet
                                                      self.question.isBookmark = self.bookmarkButton.selected;
                                                  } errorBlock:^(NSError *error) {
                                                      // Revert
                                                      self.bookmarkButton.selected = YES;
                                                  }];
        }
    }];
}

- (IBAction)commentButtonDidTouch:(id)sender {
    [self checkLoginStatus:^{
        [self.commentTextField becomeFirstResponder];
    }];
}

- (IBAction)shareButtonDidTouch:(id)sender {
    // Open share sheet
    NSString *shareMessage = [NSString stringWithFormat:@"Câu hỏi: %@\nTrả lời:%@\nNguồn: %@\nXem đầy đủ câu trả lời trên ứng dụng babyMe. Download tại đây: http://www.babyme.vn", self.question.question, self.question.sharedAnswer, @"babyme.vn"];
    UIActivityViewController *activityVC =
    [[UIActivityViewController alloc] initWithActivityItems:@[shareMessage]
                                      applicationActivities:nil];

    // Disable invalid services
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeSaveToCameraRoll, UIActivityTypeAssignToContact, UIActivityTypePostToWeibo, UIActivityTypeAirDrop, UIActivityTypePostToTencentWeibo, UIActivityTypePostToFlickr, UIActivityTypePostToVimeo];

    [self presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)backButtonDidTouch:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)commentSendButtonDidTouch:(id)sender {
    // Don't submit comment when text field empty
    if ([self.commentTextField.text isEqualToString:@""]) {
        [SIAlertView showAlertWithTitle:nil message:@"Thảo luận không được để trống." cancelButtonTitle:kDismissText];
        [self.view endEditing:YES];
        return;
    }
    self.commentCountLabel.text = [NSString stringWithFormat:@"%d Thảo luận",++commentsCount];
    id responseObject = [[NSUserDefaults standardUserDefaults] secretObjectForKey:kAPIUserObject];
    NSString * commentName = [responseObject[@"name"] isEqualToString: @""] ? responseObject[@"email"] : responseObject[@"name"];
    [_commentSendButton setTitle:@"Gửi..." forState:UIControlStateNormal];
    _commentSendButton.enabled = NO;
    _commentSendButton.alpha = .5f;
    _commentTextField.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    [ApplicationDelegate.networkEngine commentFor:questionID
                                      withContent:self.commentTextField.text
                                  completionBlock:^(id responseObject) {
                                      NSDictionary * newComment = @{@"author": commentName,
                                                                    @"content":self.commentTextField.text};
                                      // Add new comment to array
                                      [comments addObject:newComment];

                                      // Move down keyboard
                                      self.commentTextField.text = nil;
                                      [self.commentTextField resignFirstResponder];

                                      // Reload table view
                                      [self.tableView reloadData];
                                      [_commentSendButton setTitle:@"Gửi" forState:UIControlStateNormal];
                                      _commentSendButton.enabled = YES;
                                      _commentSendButton.alpha = 1.0f;
                                      _commentTextField.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0];
                                  } errorBlock:^(NSError *error) {
                                      self.commentCountLabel.text = [NSString stringWithFormat:@"%d Thảo luận",--commentsCount];

                                  }];
}

- (IBAction)likeButtonDidTouch:(id)sender {
    [self checkLoginStatus:^{
        self.likeButton.selected = !self.likeButton.selected;
        if (self.likeButton.selected) {
            // Update number of vote label
            self.voteCountLabel.text = [NSString stringWithFormat:@"%d Hữu ích",++votesCount];
            [ApplicationDelegate.networkEngine likeFor:questionID
                                       completionBlock:^(id responseObject) {
                                           // Reload table view
                                           [self.tableView reloadData];
                                           
                                       } errorBlock:^(NSError *error) {
                                           self.voteCountLabel.text = [NSString stringWithFormat:@"%d Hữu ích",--votesCount];
                                           self.likeButton.selected = NO;
                                       }];
        }
        else {
            // Update number of vote label
            self.voteCountLabel.text = [NSString stringWithFormat:@"%d Hữu ích",--votesCount];
            [ApplicationDelegate.networkEngine unLikeFor:questionID
                                       completionBlock:^(id responseObject) {
                                           // Reload table view
                                           [self.tableView reloadData];
                                           
                                       } errorBlock:^(NSError *error) {
                                           self.voteCountLabel.text = [NSString stringWithFormat:@"%d Hữu ích",++votesCount];
                                           self.likeButton.selected = NO;
                                       }];
        }
    }];
}

- (IBAction)relatedQuestionDidTouch:(id)sender {
    relatedButtonTouched = relatedQuestion;
    [self performSegueWithIdentifier:@"questionDetailToRelatedQuestionSegue" sender:nil];
}

- (IBAction)relatedArticleDidTouch:(id)sender {
    relatedButtonTouched = relatedArticle;
    [self performSegueWithIdentifier:@"questionDetailToRelatedQuestionSegue" sender:nil];
}

#pragma mark - Webview delegate

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    // Re-format the html
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.body.style.padding='0px 0px 30px 0px';document.body.style.margin='0px 0px 0px 0px';document.body.style.display='table';document.body.style.background = '#E7E7E7';"]];

    webViewHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight;"] intValue];
    NSLog(@"HALLLLO %f", webViewHeight);
    if(IS_IPHONE4){
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ){
            self.anwerWebViewHeightConstraint.constant = webView.scrollView.contentSize.height;
            self.headerView.height +=  webView.scrollView.contentSize.height + 30;
        }else{
            self.anwerWebViewHeightConstraint.constant = webView.scrollView.contentSize.height;
            self.headerView.height +=  webView.scrollView.contentSize.height + 130;
        }
    }else if(IS_IPHONE6){
        self.anwerWebViewHeightConstraint.constant = webView.scrollView.contentSize.height;
        self.headerView.height +=  webView.scrollView.contentSize.height + 120;
    }
    else{
        self.anwerWebViewHeightConstraint.constant = webView.scrollView.contentSize.height + 30;
        self.headerView.height +=  webView.scrollView.contentSize.height + 30;
    }

    [self.headerView setNeedsLayout];
    [self.headerView layoutIfNeeded];

    self.tableView.tableHeaderView = self.headerView;
}


#pragma mark - Helpers

-(void)getQuestionDetail {
    // Call API for data
    [actInd startAnimating];
    [[BMQuestionManager sharedInstance] getQuestionDetailOf:_questionid
                                           withSuccessBlock:^{
                                               [actInd stopAnimating];
                                               NSArray * questions = [BMQuestionManager sharedInstance].questionList;
                                               self.question = [questions lastObject];
                                               [self populateQuestionAndAnswer];
                                               [SVProgressHUD dismiss];
                                               [self checkBookmark];
                                           }
                                              andErrorBlock:^{
                                                  [actInd stopAnimating];
                                                  [self dismissViewControllerAnimated:YES completion:nil];
                                              }
     ];
}

-(void)checkBookmark {
    // Check bookmark status
    self.bookmarkButton.selected = self.question.isBookmark;
}

-(void)checkVote {
    // Check vote status
    self.likeButton.selected = self.question.isVoted;
}

-(void)checkLoginStatus:(void (^)())process {
    if (LOGGED_IN) process();
    else {
        [SIAlertView showAlertWithTitle:nil
                                message:@"Bạn cần đăng nhập để sử dụng tính năng này"
                      cancelButtonTitle:@"Đóng lại"
                          okButtonTitle:@"ĐĂNG NHẬP"
                                handler:^{
                                    // Go to login screen
                                    BMIntroViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"introViewController"];
                                    [self presentViewController:controller animated:YES completion:nil];
                                }];
        [self.view endEditing:YES];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"questionDetailToRelatedQuestionSegue"]) {
        BMRelatedQuestionViewController *controller = [segue destinationViewController];
        controller.questionid = questionID;
        controller.categoryid = categoryID;
        controller.relatedButtonTouched = relatedButtonTouched;
    }
}

#pragma mark - Clean up

-(void)dealloc {
    self.tableView.delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
