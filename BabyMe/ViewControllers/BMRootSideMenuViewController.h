//
//  BMRootSideMenuViewController.h
//  BabyMe
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface BMRootSideMenuViewController : RESideMenu <RESideMenuDelegate>

@end
