//
//  BMNotificationViewController.m
//  BabyMe
//
//  Created by Hung Pham Duy on 6/2/15.
//  Copyright (c) 2015 Silicon Straits Saigon. All rights reserved.
//

#import "BMNotificationViewController.h"
#import "BMReminderTableViewCell.h"

@interface BMNotificationViewController ()<UITableViewDataSource, UITableViewDelegate>{
    NSInteger selectedIndex;
    BOOL extendedCellFlag;
}

@property (nonatomic, strong) UITableView * tableView;
@end

@implementation BMNotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedIndex = 0;
    // Do any additional setup after loading the view from its nib.
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height - 60 ) style:UITableViewStylePlain];
//    tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.opaque = NO;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.backgroundView = nil;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.bounces = YES;
    tableView.scrollsToTop = YES;
    self.tableView = tableView;

    [self.view addSubview:self.tableView];
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    self.title = @"Thông Báo";
    // Format Navbar
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Sidebar Indicator"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(sideMenuButtonDidTouch:)];
    [menuButton setTintColor:[UIColor whiteColor]];
    self.navigationItem.leftBarButtonItem = menuButton;
   

    
    self.navigationController.navigationBar.barTintColor = NAVBAR_COLOR;
    [self.navigationController.navigationBar setTranslucent:NO];
    UILabel * titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.text = @"Thông Báo";
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    //Load Notifs
    [self loadNotifs];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Load notifs
-(void)loadNotifs{
    [SVProgressHUD show];
    [ApplicationDelegate.networkEngine getNotificationOf:BABY_ID completionBlock:^(id responseObject) {
        NSLog(@"responseObject %@",responseObject);
        self.data = responseObject;
        [self.tableView reloadData];
        [SVProgressHUD dismiss];
    } errorBlock:^(NSError *error) {
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - Table view
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.data.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat padding;
    CGRect frame;
    CGSize maximumLabelSize = CGSizeMake(self.view.width-56-20, CGFLOAT_MAX);
    NSString * body;
    

            if (indexPath.row == selectedIndex) {
                extendedCellFlag = YES;
                padding = 40;
                NSString *body1 = [[self.data objectAtIndex:indexPath.row] objectForKey:@"content"];
                NSString *body2 = [[self.data objectAtIndex:indexPath.row] objectForKey:@"title"];
                body = [NSString stringWithFormat:@"%@\n%@", body1, body2];
                NSLog(@"TESTing %@", body);
            }
            else {
                return 80;
            }
    
    // Calculate content height and cache height
    frame = [body boundingRectWithSize:maximumLabelSize
                               options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                            attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Roboto-Light" size:16]}
                               context:nil];
    CGFloat t = frame.size.height + padding;
    return t;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    // Check condition if cell was extended or not
    selectedIndex = (extendedCellFlag && selectedIndex == indexPath.row) ? -1 : indexPath.row;
    // Reload row at index path
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView.delegate tableView:tableView didSelectRowAtIndexPath:indexPath];
    return NO;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  UITableViewCell *  cell = [tableView dequeueReusableCellWithIdentifier:@"ReminderCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"BMReminderTableViewCell" owner:nil options:nil] lastObject];
    }
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        ((BMReminderTableViewCell *)cell).contentToTitleContraint.constant = -2;
    }
    ((BMReminderTableViewCell *)cell).reminderContent.text = [[self.data objectAtIndex:indexPath.row] objectForKey:@"content"];
    ((BMReminderTableViewCell *)cell).reminderTitle.text = [[self.data objectAtIndex:indexPath.row] objectForKey:@"title"];
//    ((BMReminderTableViewCell *)cell).reminderAvatar.image = (UIImage *)[reminderIconDict objectForKey:[reminderCategory objectAtIndex:indexPath.row]];
    return cell;
}


#pragma mark - Action
- (IBAction)sideMenuButtonDidTouch:(id)sender {
    [self presentLeftMenuViewController:nil];
}

@end
