//
//  BMFillBabyInfoViewController.h
//  BabyMe
//

#import <UIKit/UIKit.h>
#import "BMButton.h"

@interface BMFillBabyInfoViewController : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

// Text field
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *babyNameTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *babyBirthdayTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *babyGenderTextField;

// Register, Submit Button

@property (weak, nonatomic) IBOutlet BMButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@end
