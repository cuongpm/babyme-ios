//
//  BMWalkthroughViewController.h
//  BabyMe
//

#import <UIKit/UIKit.h>
#import "BMRegisterViewController.h"
#import "BMWalkthroughPageContentViewController.h"

@interface BMWalkthroughPageViewController : UIPageViewController <UIPageViewControllerDelegate, UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController  *walkthroughPageViewController;
@property (strong, nonatomic) NSArray               *pageTitles;
@property (strong, nonatomic) NSDictionary          *tempBabyDict;

@end
