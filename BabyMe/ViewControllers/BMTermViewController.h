//
//  BMTermViewController.h
//  BabyMe
//
//  Created by Cuong Pham on 2/10/15.
//  Copyright (c) 2015 Silicon Straits Saigon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BMTermViewController : UIViewController<UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
