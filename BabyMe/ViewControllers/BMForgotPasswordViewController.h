//
//  BMForgotPasswordViewController.h
//  BabyMe
//
//  Created by Cuong Pham on 2/26/15.
//  Copyright (c) 2015 Silicon Straits Saigon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BMForgotPasswordViewController : UIViewController


- (IBAction)exitButtonDidTouch:(id)sender;
- (IBAction)sendPassword:(id)sender;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *email;


@end
