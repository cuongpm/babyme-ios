//
//  BMSearchViewController.h
//  BabyMe
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface BMSearchViewController : GAITrackedViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actInd;

- (IBAction)exitButtonDidTouch:(id)sender;

@end
