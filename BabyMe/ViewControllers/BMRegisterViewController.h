//
//  BMRegisterViewController.h
//  BabyMe
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"
@interface BMRegisterViewController : GAITrackedViewController <UITextFieldDelegate>
{
           IBOutlet UIImageView *divider;
    __weak IBOutlet UIButton *registerFacebook;
    __weak IBOutlet JVFloatLabeledTextField *emailField;
    __weak IBOutlet JVFloatLabeledTextField *passwordField;
    __weak IBOutlet JVFloatLabeledTextField *confirmPasswordField;
    __weak IBOutlet UIButton *exitButton;
}

@property (nonatomic, strong) NSDictionary *babyDict;
@property (weak, nonatomic) IBOutlet UIButton *btnTerm;
- (IBAction)onTermButton:(id)sender;
- (IBAction)moveToLoginSreen:(id)sender;
@end
